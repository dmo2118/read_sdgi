#include "io_result.h"

#include <stdio.h>

#if _WIN32

_TCHAR *optarg;
int optind = 1;

static void _getopt_error(_TCHAR ch)
{
	_ftprintf(stderr, _T("Invalid switch - \"%c\"\n"), ch);
}

/* DOS/Windows-appropriate behavior. */
int getopt(int argc, _TCHAR **argv, const _TCHAR *optstring)
{
	/* TODO: Does this work for switches without ':'? */
	size_t opt = optind;
	_TCHAR *arg0;
	const _TCHAR *c1;
	int result;
	_TCHAR lower;

	optarg = NULL;

	for(;;)
	{
		if(opt == argc)
			return -1;

		arg0 = argv[opt];
		if((arg0[0] == '/' || arg0[0] == '-') && arg0[1] && arg0[1] != ':')
			break;

		++opt;
	}

	arg0 = argv[opt];
	lower = arg0[1];
	if(lower >= 'A' && lower <= 'Z')
		lower += 'a' - 'A';
	c1 = _tcschr(optstring, lower);

	if(!c1)
	{
		_getopt_error(arg0[1]);
		result = '?';
	}
	else
	{
		result = *c1;
		if(c1[1] == ':')
		{
			if(opt + 1 == argc)
			{
				result = '?';
			}
			else
			{
				optarg = argv[opt + 1];
				++opt;
			}
		}
	}

	{
		unsigned end = optind;
		if(optarg)
			++end;
		while(opt != end)
		{
			argv[opt] = argv[opt - (optarg ? 2 : 1)];
			--opt;
		}
	}

	argv[optind] = arg0;
	if(optarg)
	{
		++optind;
		argv[optind] = optarg;
	}
	++optind;

	return result;
}

#endif
