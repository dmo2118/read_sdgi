#ifndef VERSION_H
#define VERSION_H

#include <stddef.h>

extern const char version[];
extern const ptrdiff_t version_size;

#endif
