/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOX_INTG_H
#define BOX_INTG_H

#include "box.h"

static inline double _b2(double t, double x0, double x1)
{
	return t * (x1 - x0) + x0; // a.k.a. degree-2 Bézier
}

const struct box_vtbl *box_line(double lC, double y0, double y1, struct box_mb t, double *v, double (*g)[4]);
const struct box_vtbl *box_c(double lC, double y0, double y1, struct box_mb t, double *v, double (*g)[4]);

#endif
