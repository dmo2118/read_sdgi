sdgi2sf2
========

Converts the ROM of an original Ensoniq Soundscape (or one of its clones) to a
SoundFont.

The result is functional, but a work in progress. Identifying deficiencies is
left as an exercise to the end user.

On macOS, using the binary release: Run the following to bypass the
"unidentified developer" error:

```
xattr -d com.apple.quarantine sdgi2sf2 *.dylib
```

A hardware floating-point unit is strongly recommended to run `sdgi2sf2` on
older CPUs. An Intel i386/20 MHz without a 387 is likely to take around a week
to run the DOS build. A floating-point unit is standard on nearly all x86 chips
introducted 1995 or later, Mac computers since 1997, as well as all Raspberry Pi
models.

ROMs
----

The following hardware is known to contain a compatible ROM:

* Ensoniq Soundscape S-2000 (2 MB)
* Reveal SoundFX WAVE 32 (SC600)
    * Other Reveal SoundFX cards have unrelated architectures, and won't work
      here.
* SPEA V7-Media FX (2 MB) (untested, but likely works)
* Several arcade games from Incredible Technologies:
    * (Strata) BloodStorm
    * (Strata) Driver's Edge
    * (Strata) NFL Hard Yardage
    * (Strata) Pairs
    * (Strata) Pairs Redemption
    * (Tuning Electronic) Hot Memory
    * Golden Tee 3D Golf (v1.8 or earlier, v1.92S)
    * World Class Bowling (v1.0, v1.1)

The ROM chip is a long
[DIP](https://en.wikipedia.org/wiki/Dual_in-line_package) package typically with
an "MX" logo (for Macronix). Only the chip labeled 1350901601 is supported. Once
extracted, the ROM image should start with a four byte `SDGI` signature, and it
should have a CRC32 of `9FDC4825`.

The ROM image can be obtained from one of the supported sound cards using the
software from <http://www.os2museum.com/wp/dumping-ensoniq-soundscape-roms/>.

Most if not all Ensoniq cards released after the original Soundscape — including
a 1 MB variant of the S-2000 — use different ROM formats, and are incompatible
with this tool. These other formats are relatives of the
[Ensoniq Concert Wavetable](https://web.archive.org/web/20110819020545/http://johnnengelmann.com/technology/ecw/index.php)
format, and they have the string `SNDR` at byte offset 8h.

Known incompatible Ensoniq ROMs include:

| Card          | ROM        | CRC32      |
| ------------- | ---------- | ---------- |
| S-2000 (1 MB) | 1351000501 | `46F809AF` |
| OPUS          | 1351000801 | `1B696EFE` |

There are two firmware programs that can drive the original 2 MB Soundscape
S-2000: an older `SNDSCAPE.COD`, and a newer `SNDSCAPE.CO0`, with a slightly
different resulting sound for both. `sdgi2sf2` follows the `SNDSCAPE.COD`
interpretation of the ROM.

SoundFont synthesizers
----------------------

The resulting SoundFont runs about as well as it can with
[FluidSynth](https://www.fluidsynth.org/) 2.0 or later.

[TiMidity++](https://timidity.sourceforge.net/) unfortunately does not work as
well. If you want to use TiMidity++ anyway, you may want to follow the advice at
<https://askubuntu.com/a/1335651> to make things sound better, or at least add
`-Ee` to the TiMidity++ command line.

Building
--------

Install [NLopt](https://github.com/stevengj/nlopt) and the
[GNU Scientific Library](https://www.gnu.org/s/gsl/), then from a POSIX shell
(Linux, macOS, Cygwin, MSYS2):

```
make
```

Modify the usual environment variables (`PATH`, `CC`, `LDLIBS`, etc.) for
cross-compiling.

Usage
-----

```
sdgi2sf2 [-i] [-p] [-V] [rom_file] [sf2_file]
```

`-i` Add extra detail about the operating system to the `ISFT` (software info)
chunk.

`-p` Show progress. `sdgi2sf2` takes a very long time to run.

`-V` Show the version and exit.

`ensoniq.2m` and `ensoniq.sf2` are the defaults for `rom_file` and `sf2_file`.

SoundFont vs. Soundscape ROM
----------------------------

* The synthesis model is fairly similar: envelopes, LFOs, modulators, low-pass
  filtering. And measurements are expressed in similar ways, with volume
  measured in fractional decibels, pitch measured in fractional cents, and
  envelope times measured in log(seconds). But the SoundFont measurements are
  somewhat finer-grained than the ROM.

* SoundFont presets and instruments map reasonably well to objects in the ROM,
  even if the ROM has three hierarchical levels, and SoundFont has two.

* The ROM supports envelopes defined by a small number of arbitrary (time,
  volume) points. SoundFont uses a fixed-shape
  [DAHDSR envelope](https://en.wikipedia.org/wiki/Envelope_%28music%29#Other_envelopes)
  which can map very poorly to the more complex ROM envelopes.

* The ROM uses 8-bit μ-law PCM, though not quite the typical encoding for μ-law,
  so you can't just feed the ROM to `ffplay -f mulaw` and hear correct samples.
  SoundFont 2 uses uncompressed 16-bit PCM, so the 2 MB ROM expands to 4 MB of
  samples.

* The ROM supports bidirectional looping and SoundFont doesn't, so loops are
  placed in the .sf2 file twice: once forwards, and once backwards. This
  accounts for most of the remainder of the final 4.5 MB SoundFont size.

* SoundFont allows for lists of modulators, plus envelopes and LFOs to affect
  things like volume, pitch, and filter cutoff. The ROM format instead has one
  or two open slots that can be filled with modulators/envelopes/LFOs for each
  of volume/pitch/cutoff.

* The ROM supports several different kinds of waveforms for LFOs. SoundFont only
  supports a triangle wave.

* The ROM is big-endian, so that a
  [68000](https://en.wikipedia.org/wiki/Motorola_68000)-based microcontroller
  can handle it efficiently. SoundFont 2 is little-endian, likely so that an
  [x86](https://en.wikipedia.org/wiki/X86)-based PC can handle it efficiently.

License
-------

Copyright (C) 2020-2024  Dave Odell <dmo2118@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Binary releases may also contain the following:

| Library                                          | License |
| ------------------------------------------------ | ------- |
| [CWSDPMI](https://sandmann.dotster.com/cwsdpmi/) | [GPLv2 with exception](https://sandmann.dotster.com/cwsdpmi/cwsdpmi.txt) |
| [GNU Scientific Library](https://gnu.org/s/gsl/) | [GPLv3](https://gnu.org/s/gsl/#licensing) |
| [GCC](https://gcc.gnu.org/) runtime libraries    | [GPLv3 with exception](https://gcc.gnu.org/onlinedocs/libstdc++/manual/license.html) |
| [NLopt](https://github.com/stevengj/nlopt)       | [LGPLv2.1](https://nlopt.readthedocs.io/en/latest/NLopt_License_and_Copyright/) |
| [libwinpthread](https://www.mingw-w64.org/)      | [ZPLv2.1](https://old.zope.dev/Resources/ZPL/) |
