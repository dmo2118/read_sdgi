/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "io_mem.h"

#include <stdlib.h>

void io_mem_delete(struct io_mem *self)
{
	struct io_mem_iterator *node = self->_first;
	while(node)
	{
		struct io_mem_iterator *next = node->_next;
		free(node);
		node = next;
	}
}

void *io_mem_write_begin(struct io_mem *self, size_t size)
{
	if(!self->_last || self->_last->_size + size > self->_last_capacity)
	{
		// TODO: Use `_last_capacity * 2` rounded to `size`.
		size_t new_capacity = self->_last_capacity * 2;
		if(size > new_capacity)
			new_capacity = size;

		struct io_mem_iterator *new_last = malloc(sizeof(struct io_mem_iterator) + new_capacity);
		if(!new_last)
			return NULL;

		if(self->_last)
			self->_last->_next = new_last;
		else
			self->_first = new_last;

		new_last->_next = NULL;
		new_last->_size = 0;
		self->_last = new_last;
		self->_last_capacity = new_capacity;
	}

	return (char *)(self->_last + 1) + self->_last->_size;
}
