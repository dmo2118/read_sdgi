/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_MMAP_H
#define IO_MMAP_H

#include "io_result.h"

#if _WIN32

#include <windows.h>

typedef DWORDLONG io_mmap_size_type;

struct io_mmap
{
	HANDLE file;
	HANDLE mapping;
	void *view;
	ULARGE_INTEGER size;
};

static inline DWORDLONG io_mmap_size(struct io_mmap *self)
{
	return self->size.QuadPart;
}

#else

#include <unistd.h>

typedef off_t io_mmap_size_type;

struct io_mmap
{
	int fd;
	off_t file_size;
	char *base;
	off_t length;
	off_t offset;
};

static inline off_t io_mmap_size(struct io_mmap *self)
{
	return self->file_size;
}

#endif

io_result io_mmap_new(struct io_mmap *self, const _TCHAR *path);
void io_mmap_delete(struct io_mmap *self);

// Old value for addr is bad on failure.
io_result io_mmap_view(struct io_mmap *self, io_mmap_size_type offset, size_t length, void **addr);
io_result io_mmap_read(struct io_mmap *self, io_mmap_size_type offset, size_t length, void *addr);

#endif

