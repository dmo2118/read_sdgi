/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RIFF_H
#define RIFF_H

#include "io_file.h"

#include <inttypes.h>

struct riff
{
	io_file_size offset;
};

static inline uint16_t little_endian_16(uint16_t x)
{
	return x;
}

static inline uint32_t little_endian_32(uint32_t x)
{
	return x;
}

static inline uint32_t riff_tag(const char *tag)
{
	return *(uint32_t *)tag;
}

io_result riff_begin(struct riff *riff, struct io_file *io, uint32_t tag);
io_result riff_commit(struct riff *riff, struct io_file *io);
io_result riff_write(struct io_file *io, uint32_t tag, const void *data, size_t size);

#endif
