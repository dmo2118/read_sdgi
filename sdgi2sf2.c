/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
The code in here is kinda crap in places. Don't look too closely at it.
*/

#define _GNU_SOURCE

#if !defined(_UNICODE) && UNICODE
#	define _UNICODE 1
#endif

#include "io_result.h"

#include <gsl/gsl_version.h>
#include <nlopt.h>

#include <assert.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "box.h"
#include "envelope_fit.h"
#include "io_file.h"
#include "io_mem.h"
#include "io_mmap.h"
#include "riff.h"
#include "sf2.h"
#include "sdgi.h"
#include "version.h"

#if _WIN32
#	if _WIN64
#		define _Z "ll"
#	else
#		define _Z ""
#	endif
#else
#	include <sys/utsname.h>
#	include <unistd.h>
#	define _Z "z"
#endif

#if MSDOS
#	include <fcntl.h>
#	if __DJGPP__
#		include <sys/exceptn.h>
#	endif
#endif

#if __GLIBC__
#	include <gnu/libc-version.h>
#endif

#if DEBUG_ENVELOPES
#	include "plot_bezier.h"
#endif

#if __SSE__
#	include <xmmintrin.h>
#endif

/*
TODO:
Volume (+ Envelopes)
  Fitted release
  Negative attenuation?
  Probably not necessary: Exact fit

Percussion exclusives (probably not possible)
LFOs
Percussion shouldn't hold.
Are $7A and $7B too quiet?
Wave $159 is pretty clicky in the loop. Probably just needs filtering.
Envelope length vs. semitone
Variable attack vs. velocity
*/

#if !__GLIBC__

static void *memrchr(const void *s, int c, size_t n)
{
	const char *sc = s;
	while(n)
	{
		--n;
		if(sc[n] == c)
			return (void *)(sc + n);
	}

	return NULL;
}

static char *strchrnul(const char *s, int c)
{
	while(*s && *s != c)
		++s;
	return (char *)s;
}

#endif

#define arraysize(a) (sizeof(a) / sizeof(*(a)))

#define verify(x) { int _ = !!(x); assert(_); }

enum
{
	_debug_envelopes_x = 192,
	_debug_envelopes_y = 16,
	_debug_envelopes_w = 480,
	_debug_envelopes_h = 256,
	_debug_envelopes_release_w = 192,
	_debug_envelopes_release_w_pad = 64,
};

enum _envelope_type
{
	_envelope_mod,
	_envelope_vol
};

struct _wave
{
	int32_t offset;

	union
	{
		struct sdgi_wave_body body;
		struct sdgi_wave_redirect redirect;
	} wave;

	uint32_t min;
	uint32_t max;
	uint32_t start;
	uint32_t start_loop;
	uint32_t end_loop;
	uint32_t end;
	uint32_t start_smpl;
};

struct _region_envelope
{
	double release_end;
	bool sustain0;
	double release_x;
	double sustain_y;
};

struct _region_envelopes
{
	struct _region_envelope envelopes[2];

	double vol;

	int16_t initial_filter_fc;
	int16_t env_to_filter_fc;
};

struct _region
{
	const struct sdgi_region region;
	// struct _region_envelope envelope1;
	bool is_melodic: 1;
	bool is_drum_kit: 1;
};

struct _gen_mod
{
	struct io_mem io;
	WORD size;
};

typedef struct _gen_mod _gen, _mod;

static void _gen_new(_gen *self)
{
	io_mem_new(&self->io);
	self->size = 0;
}

static inline void _gen_write(_gen *self, struct sfGenList *gen_list, size_t count)
{
	verify(!io_mem_write(&self->io, gen_list, count * sizeof(*gen_list)));
	self->size += count;
}

static inline void _gen_write_range(_gen *self, uint16_t oper, BYTE lo, BYTE hi)
{
	struct sfGenList gen;
	gen.sfGenOper = little_endian_16(oper);
	gen.genAmount.ranges.byLo = lo;
	gen.genAmount.ranges.byHi = hi;
	_gen_write(self, &gen, 1);
}

static inline void _gen_write_value(_gen *self, uint16_t oper, SHORT value)
{
	struct sfGenList gen;
	gen.sfGenOper = little_endian_16(oper);
	gen.genAmount.wAmount = little_endian_16(value);
	_gen_write(self, &gen, 1);
}

static inline void _mod_new(_mod *self)
{
	_gen_new(self);
}

static inline void _mod_write(_mod *self, struct sfModList *mod_list, size_t count)
{
	verify(!io_mem_write(&self->io, mod_list, count * sizeof(*mod_list)));
	self->size += count;
}

static void _mod_write1(_mod *self, SFModulator src_oper, SFGenerator dest_oper, SHORT amount)
{
	struct sfModList mod;
	mod.sfModSrcOper = src_oper; // Note-On Key Number
	mod.sfModDestOper = dest_oper;
	mod.modAmount = amount;
	mod.sfModAmtSrcOper = 0;
	mod.sfModTransOper = 0; // Linear
	_mod_write(self, &mod, 1);
}

static ptrdiff_t _shift_right_round(ptrdiff_t x, unsigned bits)
{
	ptrdiff_t half = 1 << (bits - 1);
	return (x + half - !(x & (half << 1))) >> bits;
}

static ptrdiff_t _sf2_from_sdgi_pitch(int16_t pitch)
{
	return _shift_right_round(1200 * (ptrdiff_t)pitch, 11);
	// return (1200 * pitch) >> 11;
}

static const uint8_t _sdgi_vol_offset = 127;
static const uint8_t _base_key = 64;
static const double _from_keynum_scale = 120.0 / 128.0;

static inline double _sf2_from_sdgi_vol(double vol)
{
	// TODO volume ratio possibilities:
	// - 2 (From MIDI volume)
	// return (10 / 2) * vol;

	// From _2DE, (log(2)/12)/(log(10)/200). By this, envelopes have a 64 dB range.
	return 5.017166594399686 * vol;
}

static inline double _sf2_from_seconds(double seconds, double from_keynum, int dest_semitone)
{
	return (_base_key - dest_semitone) * from_keynum + 1200 * log2(seconds);
}

static SHORT _sf2_from_seconds_clip(double time, double from_keynum, int dest_semitone)
{
	time = _sf2_from_seconds(time, from_keynum, dest_semitone);
	time += 0.5;
	if(time < -0x8000)
		return -0x8000;
	else if(time > 0x7fff)
		return 0x7fff;
	return time;
}

static inline uint32_t div2(uint32_t x)
{
	assert(!(x & 1));
	return x >> 1;
}

static inline const uint32_t _section(const uint32_t *sdgi, unsigned i)
{
	return div2(big_endian_32(sdgi[i]));
}

/*
void vector_ptr_add(const void ***vec, size_t *vec_size, const uint16_t *x)
{
	uint16_t id = big_endian_16(*x);
	if(id >= *vec_size)
	{
		unsigned new_size = id + 1;
		*vec = (const void **)realloc(*vec, new_size * sizeof(void *));
		assert(*vec);
		memset(&(*vec)[*vec_size], 0, (new_size - *vec_size) * sizeof(void *));
		*vec_size = new_size;
	}

	assert(!(*vec)[id]);
	(*vec)[id] = x;
}
*/

#if 0

static void _debug_sdgi_wave(uint16_t id, uint32_t offset, const struct sdgi_wave_body *wave_body)
{
	printf("$%.3x\t", id);

	for(int i = 7; i >= 0; --i)
	{	
		static const char bits[8] = "0C2LBADR";
		putchar(wave_body->flags & (1 << i) ? bits[i] : '.');
	}

	putchar('\t');

	if(wave_body->flags & sdgi_wave_flag_redirect)
	{
		const struct sdgi_wave_redirect *redirect = (const struct sdgi_wave_redirect *)wave_body;

		printf("$%.2x\t", redirect->shift);

		size_t count = sdgi_wave_redirect_count(redirect);
		const uint16_t *subwaves = (const uint16_t *)(redirect + 1);
		for(size_t i = 0; i != count; ++i)
			printf("$%.3x ", big_endian_16(subwaves[i]));
		putchar('\n');
	}
	else
	{
		printf(
			"%g\t$%.8x\t$%.8x\t$%.8x\t$%.8x\t$%.4x\n",
			(double)big_endian_16(wave_body->pitch),
			(offset << 12) + (big_endian_32(wave_body->start) << 1),
			(offset << 12) + (big_endian_32(wave_body->start_loop) << 1),
			(offset << 12) + (big_endian_32(wave_body->end_loop) << 1),
			(offset << 12) + (big_endian_32(wave_body->end) << 1),
			big_endian_16(wave_body->alt_wave));
	}

}

static void _debug_sdgi_instrument(size_t i, const struct sdgi_instrument *range)
{
	printf(
		"%"_Z"u\t$%.3x\t%d\t$%.2x\t$%.2x",
		i,
		big_endian_16(range->region_id) & sdgi_instrument_region_mask,
		(int16_t)big_endian_16(range->pitch),
		range->volume & sdgi_instrument_mask,
		range->_5);
}

static void _debug_sdgi_melodic(sdgi_id id, const struct sdgi_instrument *ranges)
{
	printf("%u\n", id);
	for(size_t i = 0; i != 4; ++i)
	{
		const struct sdgi_instrument *range = &ranges[i];
		if(range->volume & sdgi_instrument_volume_present)
		{
			_debug_sdgi_instrument(i, range);

			printf(
				"\t%c\t$%.2x\t$%.2x\n",
				range->melodic_lo_drum_exclusive & sdgi_melodic_lo_velocity_param ? 'V' : 'S',
				range->melodic_lo_drum_exclusive & sdgi_instrument_mask,
				range->melodic_hi & sdgi_instrument_mask);
		}
	}
}

static void _debug_sdgi_region(sdgi_id id, const struct sdgi_region *region)
{
#if 1
	static const char *params[] =
	{
		"LFO0",
		"LFO1",
		"RAMP0",
		"RAMP1",
		"V.SEMITONE",
		"V.SEMITONE_X2_M127",
		"V.VELOCITY",
		"V.SCALED_VELOCITY",
		"V.{counter}",
		"M.PRESSURE",
		"M.MODULATION_WHEEL",
		"M.BREATH_CONTROLLER",
		"M.FOOT_PEDAL",
		"M.VOLUME",
		"M.PAN_POSITION",
		"M.EXPRESSION",
	};

	char flags[8];
	for(unsigned i = 0; i != 8; ++i)
		flags[i] = region->flags & (1 << i) ? ("01" "NR34" "6" "?")[i] : '.';

	printf(
		"$%.3X\t%d\t%.2x\t%.3x\t%.8s",
		id,
		(int16_t)big_endian_16(region->pitch),
		region->vol,
		big_endian_16(region->wave) & 0xfff,
		flags);

	printf("\t(%s)*%g\t(%s)*%g",
		params[sdgi_param1(region->pitch_params)],
		sdgi_pitch_param_scale(region->pitch_param_scale1) / 256.0,
		params[sdgi_param2(region->pitch_params)],
		sdgi_pitch_param_scale(region->pitch_param_scale2) / 256.0);

	printf("\t(%s)*%d",
		params[region->vol_param],
		region->vol_param_scale);

	printf("\t(%s)*%d\t$%.2x\t$%.2x\t(%s)*%d\t$%.2x\t$%.2x",
		params[sdgi_param1(region->k_params)],
		(uint8_t)region->k1_param_scale,
		(uint8_t)region->k1_semitone_scale,
		(uint8_t)region->k1,
		params[sdgi_param2(region->k_params)],
		(uint8_t)region->k2_param_scale,
		(uint8_t)region->k2_semitone_scale,
		(uint8_t)region->k2);

	for(unsigned i = 0; i != 2; ++i)
	// if(false)
	{
		// unsigned i = 1;
		const struct sdgi_region_ramp *ramp = &region->ramp[i];
		putchar('\t');
		for(unsigned j = 0; j != 6; ++j)
			printf("$%.2x ", ramp->time[j]);
		putchar('\t');
		for(unsigned j = 0; j != 5; ++j)
			printf("$%.2x ", (unsigned)ramp->level[j]);
		printf("\t$%.2x\t$%.2x\t$%.2x", ramp->velocity_scale0, ramp->velocity_attack, ramp->semitone_scale);
	}

	putchar('\n');
#endif

#if 0
	const struct sdgi_region_ramp *ramp = &region->ramp[1];
	size_t t = 0;
	fputs("0\t", stdout);
	for(unsigned i = 0; i != 6; ++i)
	{
		t += ramp->time[i];
		printf("%d\t", t);
	}
	putchar('\n');

	fputs("0\t", stdout);
	for(unsigned i = 0; i != 5; ++i)
		printf("%d\t", ramp->level[i]);
	puts("0\n");
#endif
}

void _debug_sdgi_drum_kit(const struct sdgi_instrument *instruments)
{
	for(size_t i = 0; i != sdgi_drum_kit_instruments; ++i)
	{
		const struct sdgi_instrument *instrument = &instruments[i];
		if(instrument->volume & sdgi_instrument_volume_present)
		{
			_debug_sdgi_instrument(sdgi_drum_kit_base + i, instrument);
			printf("\t$%.1x\n", instrument->melodic_lo_drum_exclusive);
		}
	}
}

#endif

/*
static const char *_split_field(const char *s, char delim)
{
	s = strchrnul(s, delim);
	if(*s)
		++s;
	return s;
}

static void _riff_string(struct io *io, uint32_t tag, const char *text, size_t size)
{
	struct riff riff;
	verify(!riff_begin(&riff, io, tag));
	verify(!io_file_write(io, text, size));
	verify(!io_file_write(io, "", 1));
	verify(!riff_commit(&riff, io));
}
*/

static inline struct riff _riff_begin(struct io_file *io, const char *tag)
{
	struct riff result;
	verify(!riff_begin(&result, io, riff_tag(tag)));
	return result;
}

static const void *_memchr_end(const void *s, const void *end, int c)
{
	const void *result = memchr(s, c, (const char *)end - (const char *)s);
	return result ? result : end;
}

static bool _full_range(const struct sdgi_instrument *range)
{
	return (range->melodic_lo_drum_exclusive & sdgi_instrument_mask) == 0x00 && (range->melodic_hi & sdgi_instrument_mask) == 0x7f;
}

void *_map(struct io_mmap *self, off_t offset, size_t length)
{
	void *addr;
	verify(!io_mmap_view(self, offset, length, &addr));
	return addr;
}

void _map_read(struct io_mmap *self, off_t offset, size_t length, void *addr)
{
	verify(!io_mmap_read(self, offset, length, addr));
}

static void *_map_object(
	struct io_mmap *io_mmap,
	sdgi_id id,
	size_t sdgi_size,
	off_t *sdgi_offset,
	size_t obj_size,
	const void *obj_default,
	size_t *objs_size,
	void **objs)
{
	id = big_endian_16(id);

	if(id >= *objs_size)
	{
		size_t i = *objs_size;

		*objs_size = id + 1;
		*objs = realloc(*objs, *objs_size * obj_size);
		verify(*objs);

		for(; i != *objs_size; ++i)
			memcpy((char *)*objs + i * obj_size, obj_default, obj_size);
	}

	void *result = (char *)*objs + id * obj_size;
	_map_read(io_mmap, *sdgi_offset + sizeof(id), sdgi_size, result);
	*sdgi_offset += sizeof(id) + sdgi_size;
	return result;
}

static void _wave_id(CHAR *dest, uint16_t id)
{
	size_t n = sprintf((char *)dest, "$%.3X", id);
	memset(dest + n, 0, 20 - n);
}

static void _copy_io(struct io_file *dest, const struct io_mem *src)
{
	for(const struct io_mem_iterator *it = io_mem_begin(src); it != io_mem_end(src); it = io_mem_iterator_next(it))
		verify(!io_file_write(dest, io_mem_iterator_data(it), io_mem_iterator_size(it)));
}

static void _write_ibag(struct io_mem *io_ibag, WORD gen, WORD mod, WORD *bag)
{
	struct sfInstBag inst = {little_endian_16(gen), little_endian_16(mod)};
	io_mem_write(io_ibag, &inst, sizeof(inst));
	++*bag;
}

static void _write_igen(_gen *igen, const struct _wave *waves, WORD id)
{
	if(waves[id].wave.body.flags & sdgi_wave_flag_lpe)
		_gen_write_value(igen, sampleModes, 1);

	_gen_write_value(igen, sampleID, id);
}

static void _write_pbag(struct io_mem *io_pbag, WORD gen, WORD *preset_bag)
{
	struct sfPresetBag bag = {little_endian_16(gen), 0};
	verify(!io_mem_write(io_pbag, &bag, sizeof(bag)));
	*preset_bag += 1;
}

static void _write_tune(_gen *gen, ptrdiff_t tune)
{
#if 1
	ptrdiff_t tune_coarse = tune >= 0 ? tune / 100 : -((-tune) / 100);
	if(tune_coarse)
		_gen_write_value(gen, coarseTune, tune_coarse); // TODO: Consider overflow.

	ptrdiff_t tune_fine = tune - tune_coarse * 100;
	assert(tune == tune_coarse * 100 + tune_fine);
#else
	ptrdiff_t tune_fine = tune;
#endif
	if(tune_fine)
		_gen_write_value(gen, fineTune, tune_fine); // TODO: Consider overflow.
}

static void _write_vol(_gen *gen, double vol)
{
	// The SoundFont spec doesn't match reality: Creative's hardware does it wrong, and everybody matches Creative's hardware.
	// https://schristiancollins.wordpress.com/2016/03/02/using-soundfonts-in-2016/#comment-947
	// https://github.com/FluidSynth/fluidsynth/blob/v2.3.5/src/sfloader/fluid_defsfont.c#L35
	assert(vol <= 0);
	if(vol)
		_gen_write_value(gen, initialAttenuation, -2.5 * vol);
}

static double _seconds_from_sdgi_envelope_time(double dt)
{
	// t = dt > 0 ? exp((dt - 1) * log(2) / 10.0) : 0;
	return exp(dt * log(2) / 10.0) * (16.0 / 1000.0);
}

#if DEBUG_ENVELOPES

struct _graph_curve
{
	double lC;
	double y0;
	struct point *pt;
};

static inline double _lerp(double t, double a, double b)
{
	return a + t * (b - a);
}

static double _graph_curve_at(void *self_raw, double x)
{
	const struct _graph_curve *self = self_raw;
	double t = (x - self->pt[0].x) / (self->pt[1].x - self->pt[0].x);

	double y1 = exp(self->lC * self->pt[1].y);
	// double r = log(_lerp(t, self->y0, y1)) / self->lC;
	double r = log(_lerp(t, self->y0, y1) / y1) / self->lC + self->pt[1].y;
	return r < 0 || !isfinite(r) ? 0 : r;
}

static void _graph(const char *class, const struct point *points, size_t n, double lC)
{
	printf("\t\t<path class=\"%s\" d=\"M", class);

	if(isfinite(points[0].y))
		point_graph(points[0]);

	size_t p;
	if(lC == 0 ||
		points[1].x == points[2].x ||
		points[1].y == points[2].y ||
		(points[2].x - points[1].x) / (points[2].x + points[1].x) < 1.0 / (1ull << 48))
	{
		if(n >= 2 && !isfinite(points[1].y))
		{
			struct point pt = {points[1].x, 0};
			point_graph(pt);
			p = 2;
		}
		else
		{
			p = 1;
		}
	}
	else
	{
		double y0 = exp(lC * points[1].y);

		/*
		struct _graph_curve self = {lC, y0, points + 1};
		putchar(' ');
		plot2d(&self, _graph_curve_at, points[1].x, points[2].x, 64);
		*/

		double dx = points[2].x - points[1].x;

		double y1 = exp(lC * points[2].y);
		double dy = y1 - y0;

		double line_p = points[2].x * y0 - points[1].x * y1;

		struct point p0 = points[1];
		if(points[1].y < 0)
		{
			double xint = (dx - line_p) / dy;
			if(points[1].x <= xint)
			{
				p0.x = nextafter(xint, INFINITY);
				p0.y = 0;
			}
		}

		point_graph(p0);
		fputs(" C", stdout);

		plot_bezier_log(1 / lC, dy / dx, line_p / dx, p0.x, points[2].x);

		fputs(" L", stdout);
		p = 3;
	}

	while(p != n)
	{
		point_graph(points[p]);
		++p;
	}
	printf("\" />\n");
}

#endif

static double _envelope_sf2_time_scale(const struct sdgi_region_ramp *envelope_in)
{
	return (envelope_in->semitone_scale * _base_key) / 128.0;
}

double _x_max(const struct envelope_fit *envelope)
{
	return envelope->delay_dx + envelope->attack_dx + envelope->hold_dx + envelope->decay_dx;
}

size_t _points_from_sdgi(const struct sdgi_region_ramp *envelope_in, bool no_sustain, struct point *s)
{
	// bool no_sustain = drum || region->region.flags & sdgi_region_flag_no_sustain;
	// const struct sdgi_region_ramp *envelope_in = &region->region.ramp[1];

	const int8_t *level_bytes = envelope_in->level;
	double time_scale = _envelope_sf2_time_scale(envelope_in);
	size_t n = no_sustain ? 6 : 4;

	{
		double t = 0;
		s[0].x = 0;
		s[0].y = 0;

		unsigned si = 1;

		for(size_t p = 0; p != n; ++p)
		{
			double stop;
			if(p < 4)
				stop = level_bytes[p];
			else if(p == 4)
				stop = (ptrdiff_t)level_bytes[p - 1] + level_bytes[p];
			else
				stop = 0;


			double dt = envelope_in->time[p];
			if(dt || (p && !envelope_in->time[p - 1] && stop != s[si - 1].y))
			{
				if(!p)
					dt -= envelope_in->velocity_attack; // TODO: Modulator for velocity

				if(p < 5)
					dt -= time_scale;

				dt = _seconds_from_sdgi_envelope_time(dt);
			}
			else
			{
				dt = 0;
			}

			if(p == 4 && stop < 0)
			{
				assert(level_bytes[p]);
				dt *= (double)level_bytes[p - 1] / -level_bytes[p];
				stop = 0;
			}

			assert(stop >= 0);

			t += dt;

			assert(si);
			if((t == s[si - 1].x && (si == 1 || s[si - 1].x == s[si - 2].x)) ||
				(si >= 2 && stop == s[si - 1].y && s[si - 1].y == s[si - 2].y))
				--si;

			s[si].x = t;
			s[si].y = stop;
			++si;
		}

		if(si >= 2 && s[si - 1].y == s[si - 2].y)
			--si;

		n = si;
	}

	return n;
}

void _finish_envelope(
	const struct sdgi_region_ramp *envelope_in,
	bool no_sustain,
	double lC,
	double max_release_time,
	const struct point *s,
	size_t n,
	struct _region_envelope *envelope_out,
	const struct envelope_fit *envelope)
{
	assert(n);

	envelope_out->sustain0 = no_sustain || !envelope_in->level[3];

	double slope;
	if(no_sustain)
	{
		slope = (s[n - 1].y - envelope->hold_y) / envelope->decay_dx;
	}
	else
	{
		double t1 = _seconds_from_sdgi_envelope_time(envelope_in->time[4] - _envelope_sf2_time_scale(envelope_in));
		if(t1 >= max_release_time)
		{
			slope = envelope_in->level[4] / t1;
		}
		else
		{
			double t2 = _seconds_from_sdgi_envelope_time(envelope_in->time[5]);

#if 0
			if((t1 + t2) > max_release_time)
				slope = ((t2 + t1 - max_release_time) * (envelope_in->level[4] + 127) - 127 * t2) / (max_release_time * t2);
			else
				slope = -127 / (t1 + t2);
#else
			// BFGS fitted release would be a very mild improvement.
			double t = 1.0 / 3.0;
			double x_min = t1 + t2;
			if(x_min > max_release_time)
			{
				t *= max_release_time / x_min;
				x_min = max_release_time;
			}

			slope = ((1 - t) * envelope_in->level[4] + t * -127) / (t1 + t2 * t);
#endif
		}
	}

	envelope_out->release_x = (envelope_out->release_end - envelope->hold_y) / slope;
	if(envelope_out->release_x < 0)
		envelope_out->release_x = INFINITY;
}

#if DEBUG_ENVELOPES

void _debug_envelopes(
	const struct sdgi_region_ramp *envelope_in,
	size_t region_id,
	size_t *envelope_svg,
	double *error_sum,
	double error_best,
	double split,
	enum envelope_fit_type envelope_type,
	bool no_sustain,
	double lC,
	double max_release_time,
	const struct point *s,
	size_t n,
	const struct _region_envelopes *region_envelopes,
	const struct envelope_fit (*envelopes)[2],
	size_t envelopes_n)
{
	double x_max = 0;
	for(size_t i = 0; i != envelopes_n; ++i)
	{
		double new_max = _x_max(&envelopes[i][envelope_type]);
		if(new_max >= x_max)
			x_max = new_max;
	}

	for(size_t p = 0; p != n; ++p)
	{
		if(s[p].x > x_max)
			x_max = s[p].x;
	}

	assert(error_best > -1e-10);
	if(error_best < 0)
		error_best = 0;
	assert(isfinite(error_best));
	*error_sum += error_best;

	//if(t > 1)
	{
		printf("<g transform=\"translate(0,%g)\">\n", (double)(*envelope_svg * _debug_envelopes_h + _debug_envelopes_y));

		printf("\t<text y=\"16\">$%.3"_Z"X, %s</text>\n", region_id, envelope_type != envelope_fit_mod ? "vol" : "mod");
		printf("\t<text y=\"32\">n = %"_Z"d</text>\n", n);
		printf("\t<text y=\"48\">lC = %g</text>\n", lC);
		printf("\t<text y=\"64\">error = %g</text>\n", error_best);

		double scale_x = _debug_envelopes_w / x_max, scale_y = _debug_envelopes_h / -128.0;

		printf(
			"\t<g transform=\"translate(%g,%g) scale(%g,%g)\">\n",
			(double)_debug_envelopes_x,
			(double)_debug_envelopes_h,
			scale_x,
			scale_y);

		struct point segs[6];
		segs[5].x = x_max;

		for(size_t i = 0; i != envelopes_n; ++i)
		{
			envelope_fit_segments(&envelopes[i][envelope_type], segs);
			segs[5].y = segs[4].y;

			char style[6 + 20 + 1];
			sprintf(style, "params%"_Z"d", i);
			_graph(style, segs, 6, lC);
		}

		if(split >= 0)
			printf("\t\t<path class=\"split\" d=\"M %g,0 %g,127\" />\n", split, split);

		_graph("src", s, n, 0);

		if(box_mean < x_max)
			printf("\t\t<path class=\"mean\" d=\"M %g,0 %g,127\" />\n", box_mean, box_mean);

		puts("\t</g>");

		for(size_t i = 0; i != n; ++i)
		{
			printf(
				"\t<text x=\"%g\" y=\"%g\">%.4g,%.4g</text>\n",
				s[i].x * scale_x + _debug_envelopes_x,
				s[i].y * scale_y + _debug_envelopes_h,
				s[i].x,
				s[i].y);
		}

		if(!no_sustain)
		{
			double release_x = _debug_envelopes_x + _debug_envelopes_w;
			printf("\t<path class=\"release\" d=\"M %g,0 %g,%g\" />\n", release_x, release_x, (double)_debug_envelopes_h);

			double
				w0 = _seconds_from_sdgi_envelope_time(envelope_in->time[4] - _envelope_sf2_time_scale(envelope_in)),
				w1 = _seconds_from_sdgi_envelope_time(envelope_in->time[5]);

			double release_x_max = w0 + w1;
			if(release_x_max > max_release_time)
				release_x_max = max_release_time;

			double release_scale_x = _debug_envelopes_release_w / release_x_max;

			printf(
				"\t<g transform=\"translate(%g,%g) scale(%g,%g)\">\n",
				release_x,
				(double)_debug_envelopes_h,
				release_scale_x,
				scale_y);

			double release_mid_y = 127 + envelope_in->level[4];

			for(size_t i = 0; i != envelopes_n; ++i)
			{
				const struct _region_envelope *region_envelope = &region_envelopes[i].envelopes[envelope_type];
				printf(
					"\t\t<path class=\"params%"_Z"u\" d=\"M 0,127 %g,%g\" />\n",
					i,
					region_envelope->release_x,
					127 - (envelopes[i][envelope_type].hold_y - region_envelope->release_end));
			}

			printf("\t\t<path class=\"src\" d=\"M 0,127 %g,%g %g,0\" />\n", w0, release_mid_y, w0 + w1);

			puts("\t</g>");

			printf(
				"\t<text x=\"%g\" y=\"%g\">%.4g,%.4g</text>\n",
				w0 * release_scale_x + release_x,
				release_mid_y * scale_y + _debug_envelopes_h,
				w0,
				release_mid_y);

			double release_x1 = release_x + _debug_envelopes_release_w;

			printf(
				"\t<text x=\"%g\" y=\"%g\">%.4g,0</text>\n",
				(w0 + w1) * release_scale_x + release_x,
				(double)_debug_envelopes_h,
				w0 + w1);

			printf("\t<path class=\"release\" d=\"M %g,0 %g,%g\" />\n", release_x1, release_x1, (double)_debug_envelopes_h);
		}

		puts("</g>\n");
		fflush(stdout);

		++*envelope_svg;
	}
}

#endif

void _envelope_partial_time_mod(double u, double v, double lC, double N, double *m, double *b)
{
	const double t = 1.0 / 3.0;

	// u: envelope step 1 time
	// v: envelope step 2 time
	// C: curvature; 2^(1/1200) for SoundFont units, 2^(1/10) for ROM units
	// N: max extent for a linear bipolar
	double CN = exp(lC * N);
	double lCuv = log(CN * u + v);
	double lCvu = log(CN * v + u);

	*m = (lCuv - lCvu) / (2 * lC * N) + 0.5;
	*b = t * (lCuv + lCvu - 2 * log(v + u) - lC * N) / (2 * lC);
}

static void _write_pgen(_gen *pgen, const struct sdgi_instrument *inst, struct _region *regions, bool drum)
{
	// struct _region *region = &regions[big_endian_16(inst->region_id) & sdgi_instrument_region_mask];

	_write_tune(pgen, _sf2_from_sdgi_pitch(big_endian_16(inst->pitch)));

	ptrdiff_t vol = (ptrdiff_t)(inst->volume & 0x7f) - (_sdgi_vol_offset + sdgi_instrument_volume_boost);
	if(inst->melodic_hi & sdgi_melodic_hi_volume_boost)
		vol += sdgi_instrument_volume_boost;
	_write_vol(pgen, _sf2_from_sdgi_vol(vol));

	_gen_write_value(pgen, instrument, big_endian_16(inst->region_id) & sdgi_instrument_region_mask);
}

static bool _use_alt_wave(const struct _wave *waves, uint16_t id)
{
	const struct _wave *wave = &waves[id];
	return
		(wave->wave.body.flags & sdgi_wave_flag_alt_wave) &&
		!(wave->wave.body.flags & sdgi_wave_flag_bpe) &&
		!(waves[big_endian_16(wave->wave.body.alt_wave)].wave.body.flags & sdgi_wave_flag_bpe);
}

size_t _write_samples_forward(struct io_file *io, const uint8_t *begin, const uint8_t *end, const int16_t *ulaw)
{
	size_t size = end - begin;
	if(size)
	{
		int16_t *smpls = io_file_write_begin(io, size * 2);
		verify(smpls);
		for(size_t i = 0; i != size; ++i)
			smpls[i] = ulaw[begin[i]];
		verify(!io_file_write_commit(io, size * 2));
	}
	return size;
}

size_t _write_samples_backward(struct io_file *io, const uint8_t *begin, const uint8_t *end, const int16_t *ulaw)
{
	size_t size = end - begin;
	if(size)
	{
		int16_t *smpls = io_file_write_begin(io, size * 2);
		verify(smpls);
		for(size_t i = 0; i != size; ++i)
			smpls[i] = ulaw[begin[size - 1 - i]];
		verify(!io_file_write_commit(io, size * 2));
	}
	return size;
}

struct _wave *_wave_linked(struct _wave *waves, uint16_t id)
{
	struct _wave *base = &waves[id];
	return _use_alt_wave(waves, id) ? &waves[big_endian_16(base->wave.body.alt_wave)] : base;
}

static int _error(_TCHAR **argv, const _TCHAR *path, io_result error)
{
	const _TCHAR *error_str = io_result_new_str(error);
	_ftprintf(stderr, _TEXT("%s: %s: %s\n"), *argv, path, error_str);
	io_result_delete(error_str);
	return EXIT_FAILURE;
}

static inline uint16_t _make_modulator(uint8_t type, bool polarity, bool direction, bool cc, uint8_t index)
{
	return little_endian_16(
		index | ((uint16_t)cc << 7) | ((uint16_t)direction << 8) | ((uint16_t)polarity << 9) | ((uint16_t)type << 10));
}

static inline uint16_t _make_modulator_simple(bool direction, bool cc, uint8_t index)
{
	return _make_modulator(0, 1, direction, cc, index);
}

static void _write_envelope(
	const struct sdgi_region_ramp *envelope_in,
	bool no_sustain,
	_gen *igen,
	_mod *imod,
	SHORT delay_gen,
	const struct _region_envelope *region_envelope,
	const struct envelope_fit *envelope)
{
	double from_keynum = envelope_in->semitone_scale * _from_keynum_scale;
	SHORT gen_offset = delay_gen - delayModEnv;

	SHORT from_keynum_i = from_keynum + 0.5;
	SHORT from_keynum_i64 = little_endian_16(from_keynum * 64 + 0.5);

	SHORT time_w;

	time_w = _sf2_from_seconds_clip(envelope->delay_dx, from_keynum, 64);
	if(time_w > -0x8000)
	{
		_gen_write_value(igen, delayModEnv + gen_offset, time_w);

		if(from_keynum_i64)
		{
			_mod_write1(
				imod,
				_make_modulator_simple(1, 0, 3), // Note-On Key Number
				little_endian_16(delayModEnv + gen_offset),
				from_keynum_i64);
		}
	}

	time_w = _sf2_from_seconds_clip(envelope->attack_dx, from_keynum, 64);
	if(time_w > -0x8000)
	{
		_gen_write_value(igen, attackModEnv + gen_offset, time_w);

		if(from_keynum_i64)
		{
			_mod_write1(
				imod,
				_make_modulator_simple(1, 0, 3), // Note-On Key Number
				little_endian_16(attackModEnv + gen_offset),
				from_keynum_i64);
		}
	}

	time_w = _sf2_from_seconds_clip(envelope->hold_dx, from_keynum, 60);
	if(time_w > -0x8000)
	{
		_gen_write_value(igen, holdModEnv + gen_offset, time_w);

		if(from_keynum_i)
			_gen_write_value(igen, little_endian_16(keynumToModEnvHold + gen_offset), from_keynum_i);
	}

	time_w = _sf2_from_seconds_clip(envelope->decay_dx, from_keynum, 60);
	if(time_w > -0x8000)
	{
		_gen_write_value(igen, decayModEnv + gen_offset, time_w);

		if(from_keynum_i)
			_gen_write_value(igen, little_endian_16(keynumToModEnvDecay + gen_offset), from_keynum_i);
	}

	int16_t sustain = region_envelope->sustain_y + 0.5;
	if(sustain)
		_gen_write_value(igen, sustainModEnv + gen_offset, sustain);

	double release_mod = from_keynum * 64;

	const double lC = log(2) / 1200.0;
	double release_x = region_envelope->release_x;

	if(!no_sustain)
	{
		double m, b;
		_envelope_partial_time_mod(
			_seconds_from_sdgi_envelope_time(envelope_in->time[4] - _envelope_sf2_time_scale(envelope_in)),
			_seconds_from_sdgi_envelope_time(envelope_in->time[5]),
			lC,
			release_mod,
			&m,
			&b);

		release_x *= exp(lC * b);
		release_mod *= m;
	}

	time_w = _sf2_from_seconds_clip(release_x, from_keynum, 64);
	if(time_w > -0x8000)
	{
		_gen_write_value(igen, releaseModEnv + gen_offset, time_w);

		int16_t release_mod_i = release_mod + 0.5;
		if(release_mod_i)
		{
			_mod_write1(
				imod,
				_make_modulator_simple(1, 0, 3), // Note-On Key Number
				little_endian_16(releaseModEnv + gen_offset),
				release_mod_i);
		}
	}
}

void _write_envelopes(
	const struct sdgi_region_ramp *envelopes_in,
	bool no_sustain,
	bool uses_ramp_mod,
	_gen *igen,
	_mod *imod,
	const struct _region_envelopes *region_envelopes,
	const struct envelope_fit (*envelopes_fit)[2])
{
	if(uses_ramp_mod)
	{
		_write_envelope(
			&envelopes_in[_envelope_mod],
			no_sustain,
			igen,
			imod,
			delayModEnv,
			&region_envelopes->envelopes[envelope_fit_mod],
			&(*envelopes_fit)[envelope_fit_mod]);
	}

	_write_envelope(
		&envelopes_in[_envelope_vol],
		no_sustain,
		igen,
		imod,
		delayVolEnv,
		&region_envelopes->envelopes[envelope_fit_vol],
		&(*envelopes_fit)[envelope_fit_vol]);

	_write_vol(igen, region_envelopes->vol);

	_gen_write_value(igen, initialFilterFc, region_envelopes->initial_filter_fc);
	if(region_envelopes->env_to_filter_fc)
		_gen_write_value(igen, modEnvToFilterFc, region_envelopes->env_to_filter_fc);
}

bool _uses_ramp0(uint8_t param, uint8_t scale)
{
	return param == 2 && scale;
}

struct io_mem _isft(bool id_detail)
{
	struct io_mem io;
	io_mem_new(&io);

#define WRITE_ISFT(s) verify(!io_mem_write(&io, s, sizeof(s) - 1))

	WRITE_ISFT("sdgi2sf2/");
	verify(!io_mem_write(&io, version, version_size));

	if(id_detail)
	{
		// Enough for a reproducible build.

#if _WIN32
		WRITE_ISFT(" (Windows ");

		// GetVersion returns the most recent version from the application manifest for Windows 10 and later.
		DWORD windows_version = GetVersion();
		if(!(windows_version & 0x80000000))
			verify(!io_mem_write(&io, "NT ", 3));

		// Windows 95/98/Me has NTDLL, but doesn't have RtlGetVersion. Don't load it on those platforms.
		NTSTATUS (NTAPI *rtl_get_version)(RTL_OSVERSIONINFOW *) = NULL;
		HMODULE ntdll = GetModuleHandle(_TEXT("NTDLL"));
		if(ntdll)
			rtl_get_version = (NTSTATUS (NTAPI *)(RTL_OSVERSIONINFOW *))GetProcAddress(ntdll, "RtlGetVersion");

		WORD version_word = LOWORD(windows_version);
		RTL_OSVERSIONINFOW os_version_info = {sizeof(os_version_info), LOBYTE(version_word), HIBYTE(version_word)};
		if(rtl_get_version)
			rtl_get_version(&os_version_info);

		char version_str[10 + 1 + 10 + 1];
		io_mem_write(
			&io,
			version_str,
			sprintf(version_str, "%lu.%lu", os_version_info.dwMajorVersion, os_version_info.dwMinorVersion));

#else
		WRITE_ISFT(" (");

		struct utsname uname_buf;
		uname(&uname_buf);

		verify(!io_mem_write(&io, uname_buf.sysname, strlen(uname_buf.sysname)));
#if !__GLIBC__
		WRITE_ISFT(" ");
		verify(!io_mem_write(&io, uname_buf.release, strlen(uname_buf.release)));
#endif
		WRITE_ISFT(" ");
		verify(!io_mem_write(&io, uname_buf.machine, strlen(uname_buf.machine)));
#endif

		WRITE_ISFT(
#	if _M_AMD64 || __x86_64__
			"; AMD64"
#	elif _M_IA64 || __ia64__
			"; IA64"
#	elif _M_IX86 || __i386__
			"; x86"
#	elif _M_ARM64 || __aarch64__
			"; ARM64"
#	elif _M_ARM || __arm__
			"; ARM"
#	elif _M_ALPHA || __alpha__
			"; Alpha"
#	elif _M_MIPS || __mips__
			"; MIPS"
#	elif _M_PPC || __ppc__
			"; PPC"
#	endif

#	if defined _UCRT
			"; UCRT"
#	elif __MSVCRT__
			"; MSVCRT"
#	endif
			")");

#if __GLIBC__
		const char *glibc_version = gnu_get_libc_version();
		WRITE_ISFT(" glibc/");
		verify(!io_mem_write(&io, glibc_version, strlen(glibc_version)));
#endif

		char nlopt_version_text[11 + 1 + 11 + 1 + 11 + 1];
		int nlopt_major, nlopt_minor, nlopt_bugfix;
		nlopt_version(&nlopt_major, &nlopt_minor, &nlopt_bugfix);
		WRITE_ISFT(" NLopt/");
		verify(
			!io_mem_write(
				&io,
				nlopt_version_text,
				sprintf(nlopt_version_text, "%d.%d.%d", nlopt_major, nlopt_minor, nlopt_bugfix)));

		static const char gsl[] = " GSL/";
		verify(!io_mem_write(&io, gsl, sizeof(gsl) - 1));
		verify(!io_mem_write(&io, gsl_version, strlen(gsl_version)));
	}

	return io;
}

int _tmain(int argc, _TCHAR **argv)
{
#if __SSE__
	_mm_setcsr(_mm_getcsr() | _MM_FLUSH_ZERO_ON); // Flush-to-zero. Optimization, but it also helps envelope_fit() not crash.
#endif

#if MSDOS
	_fmode = O_BINARY;
#	if __DJGPP__
	__djgpp_set_ctrl_c(true);
#	endif
#endif

	// Device fingerprinting is impolite.
	bool id_detail = false;
	bool progress = false;

	for(;;)
	{
		int c = getopt(argc, argv, _TEXT("ipVv"));
		if(c < 0)
			break;

		switch(c)
		{
		case 'i':
			id_detail = true;
			break;
		case 'p':
			progress = true;
			break;
		case 'V':
		case 'v':
			{
				struct io_mem isft = _isft(true);
				for(const struct io_mem_iterator *it = io_mem_begin(&isft);
					it != io_mem_end(&isft);
					it = io_mem_iterator_next(it))
				{
					fwrite(io_mem_iterator_data(it), 1, io_mem_iterator_size(it), stdout);
				}

				io_mem_delete(&isft);
			}
			putchar('\n');
			return EXIT_SUCCESS;
		default:
			_tprintf(_TEXT("Usage: %s [-i] [-p] [-V] [rom_file] [sf2_file]\n"), argv[0]);
			return EXIT_FAILURE;
		}
	}

	const _TCHAR *infile = _TEXT("ensoniq.2m");
	const _TCHAR *outfile = _TEXT("ensoniq.sf2");
	if(argv[optind])
	{
		infile = argv[optind];
		if(argv[optind + 1])
			outfile = argv[optind + 1];
	}

	struct io_mmap in_mmap;

	{
		int error = io_mmap_new(&in_mmap, infile);
		if(error)
			return _error(argv, infile, error);
	}

	off_t in_size = io_mmap_size(&in_mmap);

	size_t waves_size = 0;
	size_t melodic_instruments_size = 0;
	size_t regions_size = 0;

	/*
	Melodic: 128 * (2 + 8 * 4) == 4352
	Regions: 0xb2 * 0x48 == 12816
	Drum kit: 2 + 8 * 88 == 706
	Waves: 0x15c * (6 + 21) == 9396
	*/

	struct _wave *waves = NULL;
	struct sdgi_instrument (*melodic_instruments)[4] = NULL;
	struct _region *regions = NULL;

	bool has_drum_kit = false;
	struct sdgi_instrument drum_kit[sdgi_drum_kit_instruments];

	{
		bool has_sdgi = false;
		size_t base = 0;
		while(base < in_size)
		{
			uint32_t sdgi[8];
			_map_read(&in_mmap, base, sizeof(sdgi), sdgi);
			if(sdgi_block_ok(sdgi))
			{
				has_sdgi = true;

				if(sdgi[4])
				{
					off_t wave_head = base + _section(sdgi, 4);
					for(;;)
					{
						sdgi_id sdgi_obj = *(const sdgi_id *)_map(&in_mmap, wave_head, sizeof(sdgi_id));
						if(sdgi_obj == sdgi_eof)
							break;

						static const struct _wave default_wave = {-1, {}, 0xFFFFFFFF, 0};
						struct _wave *wave = (struct _wave *)_map_object(
							&in_mmap,
							sdgi_obj,
							sizeof(sdgi_wave_head),
							&wave_head,
							sizeof(struct _wave),
							&default_wave,
							&waves_size,
							(void **)&waves);

						wave->offset = base + div2(big_endian_32(*(const sdgi_wave_head *)&wave->offset));

						/*
						size_t offset = div2(big_endian_32(wave_head->offset));
						printf("%lx\t", offset);
						_debug_sdgi_wave(id, offset, wave_body);
						*/
					}
				}

				if(sdgi[5])
				{
					off_t region = base + _section(sdgi, 5);
					for(;;)
					{
						sdgi_id sdgi_obj = *(const sdgi_id *)_map(&in_mmap, region, sizeof(sdgi_id));
						if(sdgi_obj == sdgi_eof)
							break;

						static const struct _region default_region = {};
						_map_object(
							&in_mmap,
							sdgi_obj,
							sizeof(struct sdgi_region),
							&region,
							sizeof(struct _region),
							&default_region,
							&regions_size,
							(void **)&regions);

						// _debug_sdgi_region(region);
					}
				}

				if(sdgi[6])
				{
					off_t melodic = base + _section(sdgi, 6);
					for(;;)
					{
						const sdgi_id sdgi_obj = *(const sdgi_id *)_map(&in_mmap, melodic, sizeof(sdgi_id));

						if(sdgi_obj == sdgi_eof)
							break;

						static const struct sdgi_instrument default_melodic[sdgi_melodic_instruments] = {};
						_map_object(
							&in_mmap,
							sdgi_obj,
							sizeof(struct sdgi_instrument) * sdgi_melodic_instruments,
							&melodic,
							sizeof(struct sdgi_instrument) * sdgi_melodic_instruments,
							default_melodic,
							&melodic_instruments_size,
							(void **)&melodic_instruments);

						// _debug_sdgi_melodic(instrument);
					}
				}

				if(sdgi[7])
				{
					off_t drum_kit_offset = base + _section(sdgi, 7);

					if(*(const sdgi_id *)_map(&in_mmap, drum_kit_offset, sizeof(sdgi_id)) != sdgi_eof)
					{
						_map_read(
							&in_mmap,
							drum_kit_offset + sizeof(sdgi_id),
							sizeof(struct sdgi_instrument) * sdgi_drum_kit_instruments,
							drum_kit);
						has_drum_kit = true;
						// _debug_sdgi_drum_kit(drum_kit);
					}
				}
			}

			base += sdgi_block_size;
		}

		if(!has_sdgi)
		{
			_ftprintf(stderr, _TEXT("%s: error: missing SDGI file signature\n"), infile);
			return EXIT_FAILURE;
		}

		if(!waves_size || !regions_size || (!melodic_instruments_size && !has_drum_kit))
		{
			_ftprintf(stderr, _TEXT("%s: error: missing sound data\n"), infile);
			return EXIT_FAILURE;
		}
	}

	for(size_t i = 0; i != melodic_instruments_size; ++i)
	{
		const struct sdgi_instrument *melodic = melodic_instruments[i];
		for(size_t j = 0; j != sdgi_melodic_instruments; ++j)
		{
			const struct sdgi_instrument *instrument = &melodic[j];
			if(instrument->volume & sdgi_instrument_volume_present)
			{
				size_t region_id = big_endian_16(instrument->region_id) & sdgi_instrument_region_mask;
				assert(region_id < regions_size);
				regions[region_id].is_melodic = true;
			}
		}
	}

	if(has_drum_kit)
	{
		for(size_t i = 0; i != arraysize(drum_kit); ++i)
		{
			const struct sdgi_instrument *instrument = &drum_kit[i];
			if(instrument->volume & sdgi_instrument_volume_present)
			{
				size_t region_id = big_endian_16(instrument->region_id) & sdgi_instrument_region_mask;
				assert(region_id < regions_size);
				regions[region_id].is_drum_kit = true;
			}
		}
	}

	for(size_t id = 0; id != waves_size; ++id)
	{
		struct _wave *wave = &waves[id];
		if(wave->offset != -1)
		{
			const struct sdgi_wave_redirect *redirect = _map(&in_mmap, wave->offset, sizeof(struct sdgi_wave_redirect));
			if(redirect->flags & sdgi_wave_flag_redirect)
				wave->wave.redirect = *redirect;
			else
				_map_read(&in_mmap, wave->offset, sizeof(struct sdgi_wave_body), &wave->wave.body);
		}
	}

	// Waves are tricky:
	// 1. Collate waves by ID
	// 2. Get min and max for each wave that doesn't use alt_wave data. (Which needs step 1 done.)
	// 3. Write out waves, record positions in `smpl` for each. (Which needs step 2 done.)
	// 4. Write out wave headers. (Which needs step 3 done.)

	ptrdiff_t waves_no_redirect_max = -1;

	for(size_t id = 0; id != waves_size; ++id)
	{
		struct _wave *wave = &waves[id];
		if(wave->offset != -1 && !(wave->wave.redirect.flags & sdgi_wave_flag_redirect))
		{
			if((ptrdiff_t)id > waves_no_redirect_max)
				waves_no_redirect_max = id;

			wave = _wave_linked(waves, id);

			uint32_t start = big_endian_32(wave->wave.body.start), end = big_endian_32(wave->wave.body.end);
			if(start < wave->min)
				wave->min = start;
			if(end > wave->max)
				wave->max = end;
		}
	}

	waves_no_redirect_max += 1;

/*
	for(unsigned i = 0; i != waves_size; ++i)
	{
		struct sdgi_wave_head *w = waves[i];

		// printf("%d\t", i);
		if(w)
		{
			assert(big_endian_16(w->id) == i);
			// printf("$%.6x",div2(big_endian_32(w->offset)));
		}
		// putchar('\n');
	}
*/

//	printf("%lu\n", region_size);
//	printf("%lu\n", melodic_instrument_size);

	// --------

	struct io_file io;
	int error = io_file_new(&io, outfile);
	if(error)
		return _error(argv, outfile, error);

#define WRITE_S(s) verify(!io_file_write(&io, s, sizeof(s) - 1))

	struct riff RIFF = _riff_begin(&io, "RIFF");
	WRITE_S("sfbk");

	{
		struct riff LIST = _riff_begin(&io, "LIST");
		WRITE_S("INFO");

		{
			struct sfVersionTag version = {little_endian_16(2), little_endian_16(1)}; // 2.01
			verify(!riff_write(&io, riff_tag("ifil"), &version, sizeof(version)));
		}

		{
			static const char sound_engine[] = "EMU8000";
			verify(!riff_write(&io, riff_tag("isng"), sound_engine, sizeof(sound_engine)));
		}

	/*
		{
			struct riff ICOP = _riff_begin(&io, "ICOP");
			// Zero-filled space at the end of partially filled pages in memory-mapped files + the page size being known
			// means that this is OK even if strlen goes a byte past EOF.
			const char *text = data + 0x20;
			io_file_write(&io, text, strnlen(text, 255));
			io_file_write(&io, "", 1);
			verify(!riff_commit(&ICOP, &io));
		}
	*/

		{
			size_t text_buf_end = ~0;
			for(size_t i = 0; i != waves_size; ++i)
			{
				if(waves[i].offset != -1 && waves[i].offset < text_buf_end)
					text_buf_end = waves[i].offset;
			}

			size_t text_buf_size = text_buf_end - 0x20;
			char *text_buf = NULL;
			const char *text = _map(&in_mmap, 0x20, text_buf_size);
			if(!memrchr(text, text_buf_size, 0))
			{
				text_buf = malloc(text_buf_size + 1);
				verify(text_buf);
				memcpy(text_buf, text, text_buf_size);
				text_buf[text_buf_size] = 0;
				text = text_buf;
			}

			{
				const char *end = strchrnul(text, '\n');

				const char *date_end = text;
				for(unsigned i = 2; i; --i)
				{
					date_end = _memchr_end(date_end, end, ' ');
					if(date_end == end)
						break;
					++date_end;
				}

				size_t size = date_end - text;
				if(size)
				{
					struct riff ICRD = _riff_begin(&io, "ICRD");
					if(text[size - 1] == ' ')
						--size;
					verify(!io_file_write(&io, text, size));
					if(size && text[size - 1] != ',')
						WRITE_S(", ");

					text = date_end;
					date_end = _memchr_end(text, end, ' ');

					verify(!io_file_write(&io, text, date_end - text));
					verify(!io_file_write(&io, "", 1));

					verify(!riff_commit(&ICRD, &io));
					text = date_end;
					if(text != end)
						++text;
				}

				// The ICMT string is provided for any non-scatological uses.
				uint32_t tags[] = {riff_tag("ICMT"), riff_tag("INAM"), riff_tag("ICOP"), riff_tag("IENG")};
				unsigned mask = 0;
				for(unsigned i = 0; i != arraysize(tags); ++i)
				{
					end = strchrnul(text, '\n');
					if(text != end)
					{
						struct riff riff = _riff_begin(&io, (const char *)&tags[i]);
						mask |= 1 << i;
						verify(!io_file_write(&io, text, end - text));
						verify(!io_file_write(&io, "", 1));
						verify(!riff_commit(&riff, &io));
					}
					text = end;
					if(*text)
						++text;
				}

				if(!(mask & (1 << 1)))
				{
					static const char name[] = "General MIDI";
					verify(!riff_write(&io, riff_tag("INAM"), name, sizeof(name)));
				}

				{
					struct riff riff = _riff_begin(&io, "ISFT");

					struct io_mem isft = _isft(id_detail);
					_copy_io(&io, &isft);
					io_mem_delete(&isft);

					verify(!io_file_write(&io, "", 1));

					verify(!riff_commit(&riff, &io));
				}
			}

			free(text_buf);
		}

		// TODO: Always use Bank 0.

		verify(!riff_commit(&LIST, &io));
	}

	{
		struct riff LIST = _riff_begin(&io, "LIST");
		WRITE_S("sdta");

		// ENSONIQ OTTO Specification Rev 2.3
		// https://rbelmont.mameworld.info/wp-content/uploads/2018/12/es5506.pdf
		int16_t ulaw[256];
		for(size_t i = 0; i != 256; ++i)
		{
			unsigned mantissa = ((i & 0x1f) << 10) | (1 << 9);
			unsigned exp = i >> 5;
			ulaw[i] = (int16_t)(exp ? (mantissa + 0x4000) ^ 0xc000 : mantissa << 1) >> (7 - exp);
		}

		struct riff riff_smpl = _riff_begin(&io, "smpl");

		size_t smpl_size = 0;

		for(size_t id = 0; id != waves_no_redirect_max; ++id)
		{
			struct _wave *wave = &waves[id];

			if(wave->offset != -1)
			{
				// _debug_sdgi_wave(id, 0, wave->wave.body);

				wave->min = _shift_right_round(wave->min, 11);
				wave->max = _shift_right_round(wave->max, 11);

				wave->start = _shift_right_round(big_endian_32(wave->wave.body.start), 11);
				wave->start_loop = _shift_right_round(big_endian_32(wave->wave.body.start_loop), 11);

				if(wave->wave.body.flags & (sdgi_wave_flag_redirect | sdgi_wave_flag_alt_wave))
				{
					wave->end_loop = _shift_right_round(big_endian_32(wave->wave.body.end_loop), 11);
					wave->end = _shift_right_round(big_endian_32(wave->wave.body.end), 11);
				}
				else
				{
					assert(!(wave->wave.body.flags & sdgi_wave_flag_redirect));
					assert(!(wave->wave.body.flags & sdgi_wave_flag_dir));
					assert(!_use_alt_wave(waves, id));

					wave->start_smpl = smpl_size;
					if(wave->wave.body.flags & sdgi_wave_flag_cmpd)
					{
						size_t wave_size = wave->max - wave->min;
						const uint8_t *min = (const uint8_t *)_map(&in_mmap, wave->offset + wave->min, wave_size);
						const uint8_t *max = min + wave_size;

						if(wave->wave.body.flags & sdgi_wave_flag_bpe)
						{
							// TODO: Fine-tune the loop end
							const uint8_t *start_loop = min + (wave->start_loop - wave->min);
							const uint8_t *end_loop = min +
								(_shift_right_round(big_endian_32(wave->wave.body.end_loop), 11) - wave->min);

							size_t size = _write_samples_forward(&io, min, end_loop, ulaw);
							size += _write_samples_backward(&io, start_loop + 1, end_loop + 1, ulaw);
							wave->end_loop = wave->min + size;
							smpl_size += size;

							size = _write_samples_forward(&io, start_loop, start_loop + 8, ulaw);
							wave->end = wave->end_loop + size;
							smpl_size += size;
						}
						else
						{
							smpl_size += _write_samples_forward(&io, min, max, ulaw);
							wave->end_loop = _shift_right_round(big_endian_32(wave->wave.body.end_loop), 11);
							wave->end = _shift_right_round(big_endian_32(wave->wave.body.end), 11);
						}
					}
					else
					{
						abort(); // TODO: 16-bit samples
					}
				}
			}

			// TODO: Constraints:
			// dwStart-[8]-dwStartLoop-[32]-dwEndLoop-[8]-dwEnd

			if(progress)
			{
				printf("1/2 smpl %"_Z"u/%"_Z"u\r", id + 1, (size_t)waves_no_redirect_max);
				fflush(stdout);
			}
		}

		if(progress)
			putchar('\n');


		verify(!riff_commit(&riff_smpl, &io));

		verify(!riff_commit(&LIST, &io));
	}

	{
		struct riff LIST = _riff_begin(&io, "LIST");
		WRITE_S("pdta");

		struct io_mem io_pbag;
		io_mem_new(&io_pbag);
		_gen pgen;
		_gen_new(&pgen);

		{
			struct riff riff_phdr = _riff_begin(&io, "phdr");

			WORD preset_bag = 0;

			for(size_t i = 0; i != melodic_instruments_size; ++i)
			{
				static const char preset_names[][20] =
				{
					"Acou Grand Piano", "Bright Acou Piano", "Elctric Grnd Piano", "Honky-tonk Piano",
					"Elec Piano 2", "Elec Piano 1", "Harpsichord", "Clavi",

					"Celesta", "Glockenspiel", "Music Box", "Vibraphone",
					"Marimba", "Xylophone", "Tubular Bells", "Dulcimer",

					"Drawbar Organ", "Percussive Organ", "Rock Organ", "Church Organ",
					"Reed Organ", "Accordion", "Harmonica", "Tango Accordion",

					"Acou Guit (nylon)", "Acou Guit (steel)", "Elec Guit (jazz)", "Elec Guit (clean)",
					"Elec Guit (muted)", "Overdriven Guitar", "Distortion Guitar", "Guit Harmonics",

					"Acoustic Bass", "Elec Bass (finger)", "Elec Bass (pick)", "Fretless Bass",
					"Slap Bass 1", "Slap Bass 2", "Synth Bass 1", "Synth Bass 2",

					"Violin", "Viola", "Cello", "Contrabass",
					"Tremolo Strings", "Pizzicato Strings", "Orchestral Harp", "Timpani",

					"String Ensemble 1", "String Ensemble 2", "SynthStrings 1", "Synth Strings 2",
					"Choir Aahs", "Voice Oohs", "Synth Voice", "Orchestral Hit",

					"Trumpet", "Trombone", "Tuba", "Muted Trumpet",
					"French Horn", "Brass Section", "SynthBrass 1", "SynthBrass 2",

					"Soprano Sax", "Alto Sax", "Tenor Sax", "Baritone Sax",
					"Oboe","English Horn", "Bassoon", "Clarinet",

					"Piccolo", "Flute", "Recorder", "Pan Flute",
					"Blown Bottle", "Shakuhachi", "Whistle", "Ocarina",

					"Lead 1 (Square)","Lead 2 (sawtooth)", "Lead 3 (calliope)", "Lead 4 (chiff)",
					"Lead 5 (charang)", "Lead 6 (voice)", "Lead 7 (fifths)", "Lead 8 (bass+lead)",

					"Pad 1 (new age)", "Pad 2 (warm)", "Pad 3 (polysynth)", "Pad 4 (choir)",
					"Pad 5 (bowed)", "Pad 6 (metallic)", "Pad 7 (halo)", "Pad 8 (sweep)",

					"FX 1 (rain)", "FX 2 (Soundtrack)", "FX 3 (crystal)", "FX 4 (atmosph)",
					"FX 5 (brightness)", "FX 6 (goblins)", "FX 7 (echoes)", "FX 8 (sci-fi)",

					"Sitar", "Banjo", "Shamisen", "Koto",
					"Kalimba", "Bag Pipe", "Fiddle", "Shanai",

					"Tinkle Bell", "Agogo", "Steel Drums", "Woodblock",
					"Taiko Drum", "Melodic Tom", "Synth Drum", "Reverse Cymbal",

					"Guitar Fret Noise", "Breath Noise", "123 Seashore", "Bird Tweet",
					"Telephone Ring", "126 Helicopter", "Applause", "Gunshot",
				};

				struct sfPresetHeader header;
				memcpy(&header.achPresetName, preset_names[i], sizeof(preset_names[i]));
				header.wPreset = little_endian_16(i);
				header.wBank = 0;
				header.wPresetBagNdx = little_endian_16(preset_bag);
				header.dwLibrary = 0;
				header.dwGenre = 0;
				header.dwMorphology = 0;
				io_file_write(&io, &header, sizeof(header));

				for(size_t j = 0; j != 4; ++j)
				{
					const struct sdgi_instrument *range = &melodic_instruments[i][j];
					if(range->volume & sdgi_instrument_volume_present)
					{
						_write_pbag(&io_pbag, pgen.size, &preset_bag);

						if(!_full_range(range))
						{
							_gen_write_range(
								&pgen,
								range->melodic_lo_drum_exclusive & sdgi_melodic_lo_velocity_param ? velRange : keyRange,
								range->melodic_lo_drum_exclusive & sdgi_instrument_mask,
								range->melodic_hi & sdgi_instrument_mask);
						}

						_write_pgen(&pgen, range, regions, false);
					}
				}
			}

			if(has_drum_kit)
			{
				struct sfPresetHeader header;
				char drum_name[20] = "Percussion";
				memcpy(&header.achPresetName, drum_name, sizeof(drum_name));
				header.wPreset = 0;
				header.wBank = little_endian_16(128);
				header.wPresetBagNdx = little_endian_16(preset_bag);
				header.dwLibrary = 0;
				header.dwGenre = 0;
				header.dwMorphology = 0;
				io_file_write(&io, &header, sizeof(header));

				for(size_t i = 0; i != arraysize(drum_kit); ++i)
				{
					const struct sdgi_instrument *range = &drum_kit[i];
					if(range->volume & sdgi_instrument_volume_present)
					{
						_write_pbag(&io_pbag, pgen.size, &preset_bag);
						_gen_write_range(&pgen, keyRange, i + sdgi_drum_kit_base, i + sdgi_drum_kit_base);
						_write_pgen(&pgen, range, regions, true);
					}
				}
			}

			struct sfPresetHeader header = {};
			memcpy(header.achPresetName, "EOP", 4);
			header.wPresetBagNdx = little_endian_16(preset_bag);
			verify(!io_file_write(&io, &header, sizeof(header)));

			struct sfPresetBag bag = {little_endian_16(pgen.size), 0};
			verify(!io_mem_write(&io_pbag, &bag, sizeof(bag)));

			struct sfGenList gen_list = {};
			verify(!io_mem_write(&pgen.io, &gen_list, sizeof(gen_list)));

			verify(!riff_commit(&riff_phdr, &io));
		}

		{
			struct riff riff_pbag = _riff_begin(&io, "pbag");
			_copy_io(&io, &io_pbag);
			verify(!riff_commit(&riff_pbag, &io));
		}

		{
			struct riff riff_pmod = _riff_begin(&io, "pmod");

			struct sfModList mod = {};
			io_file_write(&io, &mod, sizeof(mod));

			verify(!riff_commit(&riff_pmod, &io));
		}

		{
			struct riff riff_pgen = _riff_begin(&io, "pgen");
			_copy_io(&io, &pgen.io);
			verify(!riff_commit(&riff_pgen, &io));
		}

		io_mem_delete(&io_pbag);
		io_mem_delete(&pgen.io);

		struct io_mem io_ibag;
		io_mem_new(&io_ibag);

		_gen igen;
		_gen_new(&igen);

		_mod imod;
		_mod_new(&imod);

		{
			struct riff riff_inst = _riff_begin(&io, "inst");

			WORD bag = 0;

#if DEBUG_ENVELOPES
			double error_sum = 0;
			size_t envelope_svg = 0;

			printf(
				"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
				"\n"
				"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"%g\" height=\"%g\">\n"
				"\n"
				"<defs>\n"
				"	<style type=\"text/css\">\n"
				"		text {\n"
				"			font-family: Liberation Sans, sans-serif;\n"
				"			font-size: 10pt;\n"
				"		}\n"
				"\n"
				"		path {\n"
				"			fill: none;\n"
				"			vector-effect: non-scaling-stroke;\n"
				"		}\n"
				"\n"
				"		.src {\n"
				"			stroke: #004586;\n"
				"			stroke-width: .03in;\n"
				"			stroke-dasharray: 6, 2\n"
				"		}\n"
				"\n"
				"		.params0 {\n"
				"			stroke: #ff420e;\n"
				"			stroke-width: .03in;\n"
				"		}\n"
				"\n"
				"		.params1 {\n"
				"			stroke: #ffd320;\n"
				"			stroke-width: .03in;\n"
				"		}\n"
				"\n"
				"		.split {\n"
				"			stroke: #ff420e;\n"
				"			stroke-width: 1pt;\n"
				"			stroke-dasharray: 8, 8\n"
				"		}\n"
				"\n"
				"		.release {\n"
				"			stroke: #b3b3b3;\n"
				"			stroke-width: 1px;\n"
				"		}\n"
				"\n"
				"		.mean {\n"
				"			stroke: #b3b3b3;\n"
				"			stroke-width: 1pt;\n"
				"			stroke-dasharray: 8, 8\n"
				"		}\n"
				"	</style>\n"
				"</defs>\n"
				"\n",
				(double)(_debug_envelopes_x + _debug_envelopes_w + _debug_envelopes_release_w + _debug_envelopes_release_w_pad),
				(double)(_debug_envelopes_y + _debug_envelopes_h * regions_size * 2));
#endif

			for(size_t region_id = 0; region_id != regions_size; ++region_id)
			{
				struct _region *region = &regions[region_id];
				size_t wave_id = little_endian_16(big_endian_16(region->region.wave) & 0xfff);

				// Melodic and drum regions don't overlap, but there are waves that are used by both.
				assert(region->is_melodic ^ region->is_drum_kit);

				struct sfInst inst;
				_wave_id(inst.achInstName, region_id);
				inst.wInstBagNdx = little_endian_16(bag);
				io_file_write(&io, &inst, sizeof(inst));

				_write_ibag(&io_ibag, igen.size, imod.size, &bag);
				_write_tune(&igen, _sf2_from_sdgi_pitch(big_endian_16(region->region.pitch)));

				const struct sdgi_region_ramp *envelope_vol = &region->region.ramp[_envelope_vol];
				double max_release_time =
					_seconds_from_sdgi_envelope_time(envelope_vol->time[4] - _envelope_sf2_time_scale(envelope_vol)) +
					_seconds_from_sdgi_envelope_time(envelope_vol->time[5]);
				bool no_sustain = (region->region.flags & sdgi_region_flag_no_sustain) || region->is_drum_kit;

				// Modulator envelope

				size_t mod_n = 0;
				struct point mod_s_data[7]; // (Seconds, DB2)
				double mod_lC = 0;

				assert(!_uses_ramp0(region->region.vol_param, region->region.vol_param_scale));

				bool uses_ramp_mod =
					_uses_ramp0(sdgi_param1(region->region.pitch_params), region->region.pitch_param_scale1) ||
					_uses_ramp0(sdgi_param2(region->region.pitch_params), region->region.pitch_param_scale2) ||
					_uses_ramp0(region->region.vol_param, region->region.vol_param_scale) ||
					_uses_ramp0(sdgi_param1(region->region.k_params), region->region.k1_param_scale) ||
					_uses_ramp0(sdgi_param2(region->region.k_params), region->region.k2_param_scale);

				if(uses_ramp_mod)
				{
					double sum = 0;
					unsigned exp = 0;

					if(_uses_ramp0(sdgi_param1(region->region.pitch_params), region->region.pitch_param_scale1))
					{
						sum += sdgi_pitch_param_scale(region->region.pitch_param_scale1) / (256.0 * 2048.0);
						++exp;
					}

					if(_uses_ramp0(sdgi_param2(region->region.pitch_params), region->region.pitch_param_scale2))
					{
						sum += sdgi_pitch_param_scale(region->region.pitch_param_scale2) / (256.0 * 2048.0);
						++exp;
					}

					if(_uses_ramp0(region->region.vol_param, region->region.vol_param_scale))
					{
						sum += 2 * (1.0 / 12.0);
						exp += 2;
					}

					if(_uses_ramp0(sdgi_param1(region->region.k_params), region->region.k1_param_scale))
					{
						sum += region->region.k1_param_scale / 1536.0;
						++exp;
					}

					if(_uses_ramp0(sdgi_param2(region->region.k_params), region->region.k2_param_scale))
					{
						sum += region->region.k2_param_scale / 1536.0;
						++exp;
					}

					mod_n = _points_from_sdgi(&region->region.ramp[_envelope_mod], no_sustain, mod_s_data);
					assert(mod_n <= 7);
					mod_lC = log(2) * sum / exp;
				}

				// Volume envelope

				struct point vol_s_data[8]; // (Seconds, DB2)
				struct point *vol_s = vol_s_data + 1;
				size_t vol_n = _points_from_sdgi(&region->region.ramp[_envelope_vol], no_sustain, vol_s);
				assert(vol_n <= 8);

				const double vol_lC = log(2) / 12;

				bool vol_no_delay = false;

				{
					// 1. envelope_fit() works on log(time).
					//   1a. log(time) makes the input and output envelopes stretch backwards to -INFINITY.
					//   1b. Both envelopes must have a matching asymptote at t = -INFINITY, or the error is infinitely large.
					// 2. envelope_fit() fixes its output to == s[0].y.
					// 3. Input envelopes starts as a finite number in [0,127].
					// 4. Output envelope attack starts at -INFINITY decibels.

					// If the source has a delay, then feed it in as s[0].y == -INFINITY, which is kind of true.
					if(vol_n >= 2 && vol_s[0].y == 0 && vol_s[1].y == 0)
					{
						double x1 = vol_s[1].x;

						if(vol_s[1].x != vol_s[2].x)
						{
							--vol_s;
							++vol_n;
						}

						vol_s[0].x = 0;
						vol_s[0].y = -INFINITY;
						vol_s[1].x = x1;
						vol_s[1].y = -INFINITY;
					}
					else
					{
						vol_no_delay = true;
					}

					// Otherwise, if s[0].y == 0, pass that through to envelope_fit(), which is a lie, because actual attack in the output
					// envelope starts at -INFINITY.
				}

#if DEBUG_ENVELOPES
				double mod_split_x, mod_error, vol_error;
#endif
				struct _region_envelopes region_envelopes[envelope_fit_max_src_n];
				struct envelope_fit envelopes[envelope_fit_max_src_n][2];

				assert(mod_n <= envelope_fit_max_src_n);
				assert(vol_n <= envelope_fit_max_src_n);

				size_t envelopes_n = envelope_fit(
					mod_n,
					mod_s_data,
					mod_lC,
					vol_n,
					vol_s,
					vol_lC,
					vol_no_delay,
					127,
#if DEBUG_ENVELOPES
					&mod_split_x,
					&mod_error,
					&vol_error,
#endif
					envelopes);

				if(uses_ramp_mod)
				{
					const struct sdgi_region_ramp *envelope_in = &region->region.ramp[_envelope_mod];

					for(size_t i = 0; i != envelopes_n; ++i)
					{
						struct _region_envelope *region_envelope = &region_envelopes[i].envelopes[envelope_fit_mod];
						const struct envelope_fit *envelope = &envelopes[i][envelope_fit_mod];

						region_envelope->release_end = envelope->delay_y;

						_finish_envelope(
							envelope_in,
							no_sustain,
							mod_lC,
							max_release_time,
							mod_s_data,
							mod_n,
							region_envelope,
							envelope);

						region_envelope->sustain_y =
							1000 * (1 - (envelope->sustain_y - envelope->delay_y) / (envelope->hold_y - envelope->delay_y));
					}

#if DEBUG_ENVELOPES
					_debug_envelopes(
						envelope_in,
						region_id,
						&envelope_svg,
						&error_sum,
						mod_error,
						mod_split_x,
						envelope_fit_mod,
						no_sustain,
						mod_lC,
						max_release_time,
						mod_s_data,
						mod_n,
						region_envelopes,
						envelopes,
						envelopes_n);
#endif
				}

				{
					const struct sdgi_region_ramp *envelope_in = &region->region.ramp[_envelope_vol];

					// Trace the sustain/release slope when going down to 0.
					const double _sf2_release_attenuation = 100 * 10;
					const double time_fac = (_sf2_release_attenuation * log(10) / 200) / (127 * log(2) / 12);

					for(size_t i = 0; i != envelopes_n; ++i)
					{
						struct _region_envelope *region_envelope = &region_envelopes[i].envelopes[envelope_fit_vol];
						struct envelope_fit *envelope = &envelopes[i][envelope_fit_vol];

						region_envelope->release_end = 0;

						_finish_envelope(
							envelope_in,
							no_sustain,
							vol_lC,
							max_release_time,
							vol_s,
							vol_n,
							region_envelope,
							envelope);
					}

#if DEBUG_ENVELOPES
					_debug_envelopes(
						envelope_in,
						region_id,
						&envelope_svg,
						&error_sum,
						vol_error,
						mod_split_x,
						envelope_fit_vol,
						no_sustain,
						vol_lC,
						max_release_time,
						vol_s,
						vol_n,
						region_envelopes,
						envelopes,
						envelopes_n);
#endif

					for(size_t i = 0; i != envelopes_n; ++i)
					{
						struct _region_envelope *region_envelope = &region_envelopes[i].envelopes[envelope_fit_vol];
						struct envelope_fit *envelope = &envelopes[i][envelope_fit_vol];

						if(region_envelope->sustain0)
						{
							envelope->decay_dx *= time_fac;
							region_envelope->sustain_y = _sf2_release_attenuation;
						}
						else
						{
							region_envelope->sustain_y = _sf2_from_sdgi_vol(envelope->hold_y - envelope->sustain_y);
						}

						assert(envelope->hold_y >= envelope->delay_y);

						region_envelope->release_x *= time_fac;

						region_envelopes[i].vol =
							_sf2_from_sdgi_vol((region->region.vol & 0x7f) - _sdgi_vol_offset + envelope->hold_y - 0x7F);
					}
				}

				{
					static const int sf2_from_sdgi_fc_scale = 100;

					uint8_t flags = region->region.flags & (sdgi_region_flag_lp3 | sdgi_region_flag_lp4);
					double k1_fac, k2_fac;
					switch(flags)
					{
					case 0:
					case sdgi_region_flag_lp3:
						k1_fac = 1;
						k2_fac = 0;
						break;
					case sdgi_region_flag_lp4: // 12 dB rolloff, matches SF2.
						k1_fac = 0.5;
						k2_fac = 0.5;
						break;
					default:
						assert(flags == (sdgi_region_flag_lp3 | sdgi_region_flag_lp4));
						k1_fac = 0.75;
						k2_fac = 0.25;
						break;
					}

					for(size_t e = 0; e != envelopes_n; ++e)
					{
						const struct envelope_fit *envelope = &envelopes[e][envelope_fit_mod];
						double k = region->region.k1 * k1_fac + region->region.k2 * k2_fac;

						double env_to_filter_fc = 0;
						if(_uses_ramp0(sdgi_param1(region->region.k_params), region->region.k1_param_scale))
						{
							double f = region->region.k1_param_scale * k1_fac;
							env_to_filter_fc += (envelope->hold_y - envelope->delay_y) * f;
							k += envelope->delay_y * f / 128.0;
						}
						if(_uses_ramp0(sdgi_param2(region->region.k_params), region->region.k2_param_scale))
						{
							double f = region->region.k2_param_scale * k2_fac;
							env_to_filter_fc += (envelope->hold_y - envelope->delay_y) * f;
							k += envelope->delay_y * f / 128.0;
						}

						struct _region_envelopes *region_envelope = &region_envelopes[e];
						region_envelope->initial_filter_fc = k * sf2_from_sdgi_fc_scale + 0.5;
						region_envelope->env_to_filter_fc = env_to_filter_fc * (sf2_from_sdgi_fc_scale / 128.0) + 0.5;
					}

					double k_semitone_scale =
						region->region.k1_semitone_scale * k1_fac +
						region->region.k2_semitone_scale * k2_fac;

					// TODO: Not if k_semitone_scale is 0.
					_mod_write1(
						&imod,
						_make_modulator_simple(0, 0, 3), // Note-On Key Number
						little_endian_16(initialFilterFc),
						little_endian_16(k_semitone_scale * sf2_from_sdgi_fc_scale + 0.5));
				}

				if(envelopes_n <= 1)
				{
					_write_envelopes(
						region->region.ramp,
						no_sustain,
						uses_ramp_mod,
						&igen,
						&imod,
						region_envelopes,
						envelopes);
				}

				const struct _wave *wave = &waves[wave_id];
				if(wave->offset != -1)
				{
					const struct sdgi_wave_body *wave_body = &wave->wave.body;

					if(wave_body->flags & sdgi_wave_flag_redirect)
					{
						size_t count = sdgi_wave_redirect_count(&wave->wave.redirect);
						const struct sdgi_wave_redirect *redirect = &wave->wave.redirect;
						const uint16_t *subwaves = _map(&in_mmap, wave->offset + sizeof(struct sdgi_wave_redirect), 2 * count);
						struct sfGenList gen_list;
						WORD wave_id = 0xffff; // Silence a compiler warning.

						gen_list.sfGenOper = little_endian_16(keyRange);
						assert(count);

						for(unsigned i = 0; i <= count; ++i)
						{
							if(i == count || (i && subwaves[i - 1] != subwaves[i]))
							{
								for(size_t e = 0; e != envelopes_n; ++e)
								{
									_write_ibag(&io_ibag, igen.size, imod.size, &bag);

									gen_list.genAmount.ranges.byHi = (i << redirect->shift) - 1;
									_gen_write(&igen, &gen_list, 1);

									if(envelopes_n > 1)
									{
										_write_envelopes(
											region->region.ramp,
											no_sustain,
											uses_ramp_mod,
											&igen,
											&imod,
											&region_envelopes[e],
											&envelopes[e]);
									}

									_write_igen(&igen, waves, wave_id);
								}
							}

							if(!i || (i != count && subwaves[i - 1] != subwaves[i]))
							{
								gen_list.genAmount.ranges.byLo = i << redirect->shift;
								wave_id = little_endian_16(big_endian_16(subwaves[i]));
							}
						}
					}
					else
					{
						for(size_t e = 0; e != envelopes_n; ++e)
						{
							_write_ibag(&io_ibag, igen.size, imod.size, &bag);

							if(envelopes_n > 1)
							{
								_write_envelopes(
									region->region.ramp,
									no_sustain,
									uses_ramp_mod,
									&igen,
									&imod,
									&region_envelopes[e],
									&envelopes[e]);
							}

							_write_igen(&igen, waves, wave_id);
						}
					}
				}

				if(progress)
				{
					printf("2/2 inst %"_Z"u/%"_Z"u\r", region_id + 1, regions_size);
					fflush(stdout);
				}
			}

			if(progress)
				putchar('\n');

#if DEBUG_ENVELOPES
			printf(
				"<text y=\"16\">error_sum = %g</text>\n"
				"\n"
				"</svg>\n",
				error_sum);
#endif

			{
				struct sfInst inst = {"EOI"};
				inst.wInstBagNdx = little_endian_16(bag);
				verify(!io_file_write(&io, &inst, sizeof(inst)));
			}

			{
				struct sfInstBag inst = {little_endian_16(igen.size), little_endian_16(imod.size)};
				io_mem_write(&io_ibag, &inst, sizeof(inst));
			}

			verify(!riff_commit(&riff_inst, &io));
		}

		{
			struct riff riff_ibag = _riff_begin(&io, "ibag");
			_copy_io(&io, &io_ibag);
			verify(!riff_commit(&riff_ibag, &io));
		}

		{
			struct riff riff_imod = _riff_begin(&io, "imod");

			_copy_io(&io, &imod.io);
			struct sfModList mod = {};
			io_file_write(&io, &mod, sizeof(mod));

			verify(!riff_commit(&riff_imod, &io));
		}

		{
			struct riff riff_igen = _riff_begin(&io, "igen");
			_copy_io(&io, &igen.io);
			verify(!riff_commit(&riff_igen, &io));
		}

		io_mem_delete(&io_ibag);
		io_mem_delete(&igen.io);
		io_mem_delete(&imod.io);

		{
			struct riff riff_shdr = _riff_begin(&io, "shdr");

			for(size_t id = 0; id != waves_no_redirect_max; ++id)
			{
				const struct _wave *wave = &waves[id];

				struct sfSample sample =
				{
					"",
					0,
					0,
					0,
					0,
					little_endian_32(44100),
					0,
					0,
					0,
					little_endian_16(monoSample)
				};

				_wave_id(sample.achSampleName, id);

				if(wave->offset != -1 && !(wave->wave.body.flags & sdgi_wave_flag_redirect))
				{
					const struct _wave *wave_linked = _wave_linked(waves, id);
					size_t start_smpl = wave_linked->start_smpl - wave_linked->min;

					assert(!(big_endian_32(wave->wave.body.start) & ((1 << 11) - 1)));
					assert(!(big_endian_32(wave->wave.body.end) & ((1 << 11) - 1)));

					// TODO: Maybe OK-ish!?
					sample.dwStart = start_smpl + wave->start;
					sample.dwEnd = start_smpl + wave->end;
					sample.dwStartloop = start_smpl + wave->start_loop;
					sample.dwEndloop = start_smpl + wave->end_loop;

					assert(sample.dwStart <= sample.dwStartloop);
					assert(sample.dwStartloop <= sample.dwEndloop);
					assert(sample.dwEndloop <= sample.dwEnd);

					uint16_t pitch = _sf2_from_sdgi_pitch(big_endian_16(wave->wave.body.pitch));
					sample.byOriginalPitch = pitch / 100;
					sample.chPitchCorrection = -(pitch % 100);
				}

				io_file_write(&io, &sample, sizeof(sample));
			}

			{
				struct sfSample sample = {"EOS"};
				io_file_write(&io, &sample, sizeof(sample));
			}

			verify(!riff_commit(&riff_shdr, &io));
		}

		verify(!riff_commit(&LIST, &io));
	}

	verify(!riff_commit(&RIFF, &io));
	io_file_delete(&io);

	io_mmap_delete(&in_mmap);

	free(melodic_instruments);
	free(regions);
	free(waves);

	return EXIT_SUCCESS;
}
