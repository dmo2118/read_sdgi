/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
A: 30.45;
B: 40.5;
lambert_w(B*exp(A))-A;
lambert_w(exp(log(B)+A))-A;
log(%+A)+%+A;
log(B)+A;

f(w) := log(w+a)+w+a - (a+log(b));
df(w) := ''(diff(f(w),w));
df2(w) := ''(diff(f(w),w,2));

w - f(w)/df(w);
expand(logcontract(%));
ratsimp(%,log(b/(w+a)));

[
    w-2*f(w)*df(w)/(2*df(w)^2 - f(w)*df2(w)),
    subst(
        [
            lw=log(wa/b)+w,
            wa1=wa+1,
            wa=w+a
        ],
        w-2*wa*wa1*lw/(lw+2*wa1*wa1))
];
radcan(%[2] - %[1]);
*/

// Protip: log(a+b) == log(a)+log1p(b/a)

/*
See also: Robert M., Corless; David J., Jeffrey; Donald E., Knuth (1997). A sequence of series for the Lambert W function.
Proceedings of the 1997 International Symposium on Symbolic and Algebraic Computation. pp. 197–204.
https://doi.org/10.1145%2F258726.258783

|W(z)| < 1
b*exp(-$)-a
a*expm1(log(b/a)-$)

|W(z)| > 1
log(b/($+a))
log(b/a)-log1p($/a)
log(b/$)-log1p(a/$)
*/

/*
Newton's method:

(($+a)*log(b/($+a))+$)/($+a+1)
(($+a)*(log(b/a)-log1p($/a))+$)/($+a+1)
*/

/*
See also: Mező, István (2020). "An integral representation for the Lambert W function". https://arxiv.org/abs/2012.02480

B: 202.5;
A: 130.5;

lambert_w(B*exp(A))-A;

quad_qag(log(exp(-A)+exp(log(B*sin(t)/t)+t/tan(t))),t,0,%pi,1);
float((1/%pi)*%[1]);

log_cosh(x) := if x < -100 then -x-log(2) elseif x > 100 then x-log(2) else log(cosh(x));
quad_qag(log_cosh((log(B*sin(t)/t) + t/tan(t) + A)/2) + (log(sin(t)/t) + t/tan(t))/2,t,0,%pi,1);
float((1/%pi)*%[1] + (log(B)-A)/2 + log(2));
*/

#include "lambert_w_2.h"

#include <assert.h>
#include <math.h>
#include <float.h>

#if 0
static unsigned _iter = 0;
#define ITER 1
#endif

#if 1

static double _lambert_w(double w, double b, double a, double ba)
{
	// w = lambert_w(b*exp(a)) - a
	// solve((w+a)*exp(w+a)=b*exp(a),w)

	double d0 = INFINITY;
	for(;;)
	{
		assert(isfinite(w));

		double wa = w + a;

#if ITER
		++_iter;
#endif

		double wa1 = 1 + wa;

		// Switching the math from w-=a/b to w=(b*w-a)/b makes this less accurate.

#if 0
		// Newton's method. Probably not as fast. (Edge cases not handled.)
		double d1 = (fabs(w) <= 1 ? (w - ba - b * expm1(-w)) : wa * (log1p((w - ba) / b) + w)) / wa1;
#endif

#if 1
		// Halley's method.
		double wa1_2 = 2 * wa1 * wa1;

		// Assumption: log(x) is not just log1p(x - 1), and exp(x) is not just expm1(x) + 1.
		double d1;
		if(fabs(w) <= 1 || wa == 0)
		{
			// (w+a)*%e^w-b=0
			// w > log(2) is slightly better.
			double ew = w > 1 ? w + a - b * exp(-w) : w - ba - b * expm1(-w);
			d1 = ew / (wa1_2 - (wa + 2) * ew);
		}
		else
		{
			// log((w+a)/b)+w=0
			double lw = (w - ba) / b;

			if(lw <= -0.5)
			{
				// Avoid rounding error from (w - ba) / b.
				lw = wa / b;
				if(lw < 0) // Fell off the edge. It's probably done. Probably never happens on SSE, but it does on x87.
					break;

				lw = log(lw);
			}
			else if(
#if FLT_EVAL_METHOD >= 2 || __STDC_VERSION__ < 199901L
				// Builtin intrinsic isfinite() won't catch intermediate long doubles that exceed the finite range of a double.
				lw < HUGE_VAL
#else
				isfinite(lw)
#endif
				)
			{
				// Poor convergence for log((w+a)/b) for b near a. log1p() boosts accuracy when used.
				lw = log1p(lw);
			}
			else
			{
				assert((wa > 0 && b > 0) || (wa < 0 && b < 0));
				lw = log(fabs(wa)) - log(fabs(b));
			}

			lw += w;
			d1 = wa * lw / (wa1_2 + lw);
		}

		d1 *= 2 * wa1;
#endif

		double ad1 = fabs(d1);

		if(ad1
#if FLT_EVAL_METHOD >= 2 || __STDC_VERSION__ < 199901L
			 * (1 + DBL_EPSILON)
#endif
			>= d0) // Convergence has stopped due to precision loss, probably from catastrophic cancellation.
			break;

		double w0 = w;
		w -= d1;
		if(w == w0)
			break;

#if FLT_EVAL_METHOD >= 2 || __STDC_VERSION__ < 199901L
		if(fabs(d1) < fabs(w0) * DBL_EPSILON)
		{
			w = w0;
			break;
		}
#endif

		// If the initial guess is way off, then w might be cancelled out to 0. If that happens, disregard the size of the first
		// step away from 0, because that might be a short step length followed by a long step length, which would trigger
		// premature termination.
		d0 = w0 == 0 ? INFINITY : ad1;
	}

	return w;
}

/*
ez1(z) := z*%e+1;
a: -1/%e;
Q: -1/(generalized_lambert_w(-1,a*exp(a))-a);
M: 4;
u(z) := -log(-z)-1;
plot2d(
    [
        [parametric,z,lambert_w(z),[z,-1/%e,M]],
        [parametric,z,ez1(z)*((2-%e)/%e)+sqrt(ez1(z))*(2*(%e-1)/%e)-1,[z,-1/%e,0]],
        [parametric,z,Q*log(z/Q+1),[z,0,%e]],
        [parametric,z,((tanh((log(z)-1)/%e)+1)/2)*log(log(z))/log(z)-log(log(z))+log(z),[z,%e,M]],

        [parametric,z,generalized_lambert_w(-1,z),[z,-1/%e,0]],
        [parametric,z,-1-sqrt(2*u(z))-(5/6)*u(z),[z,-1/%e,0]]
    ],
    [y,-M,lambert_w(M)]);
*/

double lambert_w0_2(double b, double a, double ba)
{
	if(b == 0)
		return -a;
	if(b == a && b >= -1)
		return 0;

	double abs_b = fabs(b);
	double lb = log(abs_b);

#if 1
	// Handle asymptote. Improves accuracy.
	const double asymp_gap = log(2) * -53; // ~= -36.7368
	if(a < asymp_gap - lb) // Actual limit (eyeballed) is -33-ish to -30-ish. -35 is probably the minimum.
	{
		// The cross-section crosses 0, so small b and a may need the full _lambert_w.
		double bound = abs_b * (1ll << 54);
		if(abs_b >= (1.0 / (1ll << 49)) || !(-bound <= a && a <= bound))
			return -a;
	}
#endif

	double w;

	if(b < 0)
	{
		double ez1 = b * exp(a) * M_E + 1;
		w = ez1 * ((2 - M_E) / M_E) + sqrt(ez1) * (2 * (M_E - 1) / M_E) - 1;
	}
	else
	{
		double lz = lb + a;
		if(lz < 1)
		{
			const double Q = 0.57117179782422378; // == a = -1/M_E, -1/(Wm1(a*exp(a))-a)
			w = Q * log1p(b * exp(a) / Q);
		}
		else
		{
			// "On the Lambert W Function" (4.19), https://cs.uwaterloo.ca/research/tr/1993/03/W.pdf
			// (The tanh blob is my own.)
			double llz = log(lz);
			w = (tanh((lz - 1) * (1 / M_E)) + 1) * 0.5 * llz / lz - llz + lz;
		}
	}

	w = _lambert_w(w - a, b, a, ba);
	assert(w + a + 1 >= -(fabs(w) + fabs(a)) / (1ll << 40));

	return w;
}

double lambert_wm1_2(double b, double a, double ba)
{
	if(b == a && b <= -1)
		return 0;

	double u = -log(-b) - a - 1;
	assert(u > 0);
	// See also: Chatzigeorgiou, I. (2013). "Bounds on the Lambert function and their Application to the Outage Analysis of User
	// Cooperation". https://arxiv.org/abs/1601.04895
	double w = _lambert_w(-1 - sqrt(2 * u) - (5.0 / 6.0) * u - a, b, a, ba);
	assert(w + a <= -1);
	return w;
}

#else

#include <gsl/gsl_sf_lambert.h>

double lambert_w0_2(double b, double a, double ba)
{
	double ea = exp(a);
	if(!isfinite(ea))
	{
		// Iacono, Roberto; Boyd, John P. (2017-12-01).
		// "New approximations to the principal real-valued branch of the Lambert W-function".
		// Advances in Computational Mathematics.
		// https://doi.org/10.1007/s10444-017-9530-3

		double lb = log(b);
		double x = lb + a;
		double xlx = x - log(x);
		//return log((xlx + 1) / (xlx * (1 + x - log(xlx)))) + lb;

		double lba = lb + a;
		double llba = log(lba);
		//double u = lba - llba;
		double lv = log1p(-llba / lba);
		//return lb - llba - lv - log1p(-lv / (lba - llba + 1));
		double q = log1p((ba - lb) / (lb + a));
		return q - lv - log1p(-lv / (q + a + 1));
	}

	gsl_sf_result result;
	if(gsl_sf_lambert_W0_e(b * ea, &result))
		result.val = NAN;
	return result.val - a;
}

double lambert_wm1_2(double b, double a, double ba)
{
	double ea = exp(a);
	if(ea == 0)
	{
		// Chatzigeorgiou, I. (2013).
		// https://arxiv.org/abs/1601.04895
		double lb = log(-b);
		return lb - sqrt(-2 * (lb + a + 1));
	}

	gsl_sf_result result;
	if(gsl_sf_lambert_Wm1_e(b * ea, &result))
		result.val = NAN;
	return result.val - a;
}

#endif

#if 0

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <gsl/gsl_sf_lambert.h>

double _double_time()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + tv.tv_usec * 1e-6;
}

static inline int64_t _as_int(double x)
{
	union
	{
		double d;
		int64_t i;
	} u = {x};
	return u.i;
}

void _print_w(double expected, double w)
{
	if(expected == 0 || w == 0)
		fputs(expected == w ? "  == " : "  != ", stdout);
	else
		printf("%4"PRId64" ", _as_int(expected) - _as_int(w));
	printf("%25.17e", w);
}

void _try0(double e0, double em1, double b, double a, double ba, int ba_prec)
{
	unsigned branches = lambert_w_branches(b, a);
	assert(branches);

	printf("%23.*g %23.*g ", ba_prec, b, ba_prec, a);

	assert(fabs(b - a - ba) <= fabs(b - a + ba) / (1ull << 4));

#if ITER
	_iter = 0;
#endif

	double w = lambert_w0_2(b, a, ba);
	// double w = gsl_sf_lambert_W0(b * exp(a)) - a;

#if ITER
	printf("%2u ", _iter);
#endif

	_print_w(e0, w);

	if(b < 0)
	{
		assert(branches == 2);

		double w = lambert_wm1_2(b, a, ba);
		putchar(' ');
		_print_w(em1, w);
	}
	putchar('\n');
}

void _try(double e0, double em1, double b, double a, double ba)
{
	_try0(e0, em1, b, a, ba, 15);
}

void _try2(double e0, double em1, double b, double a)
{
	_try0(e0, em1, b, a, b - a, 17);
}

float _double_rand()
{
	union
	{
		unsigned int i;
		float f;
	} u;

	u.i = rand() << 1;
	return u.f;
}

#include <xmmintrin.h>

void fldcw(const uint16_t *m)
{
	asm("fldcw %0": : "m"(*m));
}

void fstcw(uint16_t *m)
{
	asm("fstcw %0": "=m"(*m));
}

int main()
{
/*
	uint16_t fcw = 2;
	fstcw(&fcw);
	printf("%x\n", fcw);
	printf("%x\n", _mm_getcsr());
	return 0;
*/

	printf("%.17g\n", log(1e-308));

	setvbuf(stdout, NULL, _IONBF, 0);

	printf("%.17g\n", -1 / lambert_wm1_2(-1 / M_E, -1 / M_E, 0));

/*
	{
		const int i = -300;
		double x = -pow(10, i);
		printf("%d\t%.17g\n", i, _lambert_w(x, x, 0, x));
	}
*/

	_try(-3.0007632392895281797961681995099e-1,                                  NAN,     2,    3,   -1);
	_try(  1.0000045397868749215429570148043e1,                                  NAN,     1,  -10,   11);
	_try(   2.514655216949275265614711863732e0,  1.2089668208104248941405193706693e0,    -6,   -3,   -3);
	_try( -1.1042102272033903328121236302516e0, -1.6441432689943884020659590351608e0, -0.25, 0.35, -0.6);
	_try(                                    0,                                  NAN,     0,    0,    0);

	putchar('\n');

	_try(                                 1000, NAN,  1,  -1000,  1001);
	_try(                                  500, NAN,  1,   -500,   501);
	_try(                                  100, NAN,  1,   -100,   101);
	_try(  1.0000045397868749215429570148043e1, NAN,  1,    -10,    11);

	_try(  1.5936242600400400923230418758752e0,   0, -2,     -2,     0);
	_try(  2.1200282389876412294846879752719e0, NAN,  1,     -2,     3);
	_try(                    1.278464542761074, NAN,  1,     -1,     2);
	_try(     0.567143290409783872999968662210, NAN,  1,      0,     1);
	_try( 5.0000625005208300779134082710109e-5, NAN,  1, 0.9999,  1e-4);
	_try(                                    0,   0, -1,     -1,     0);
	_try(                                    0, NAN,  1,      1,     0);
	_try(-4.9999375005208365883300813131843e-5, NAN,  1, 1.0001, -1e-4);
	_try(-4.4285440100238858314132799999934e-1, NAN,  1,      2,    -1);
	_try( -2.0705799049803026514382717338279e0, NAN,  1,     10,    -9);
	_try( -4.5585133544241681598279647538193e0, NAN,  1,    100,   -99);
	_try( -6.9008305276108956117608041127511e0, NAN,  1,   1000,  -999);

	_try(  1.5936545283391268434760207266684e0, -9.9998000051665091719856414201252e-6, -2.00001, -2.00002,    1e-5);
	_try( 4.4854829212716975839861732732966e-3, -4.4588163183082614910447391042655e-3, -1.00001, -1.00002,    1e-5);
	_try( 4.4555163628065011417472704497242e-3, -4.4888499598469762070712137424499e-3, -0.99998, -0.99999,    1e-5);
	_try( 5.0000625005208300779134082710109e-5,                                   NAN,        1,   0.9999,    1e-4);
	_try(-4.9999687501614577376306266441488e-6,                                   NAN,  1.00001,  1.00002,   -1e-5);
	_try( 1.9999400014666413334240112517618e-5,                                     1,    50001,    50000,       1);

	_try(  2.2177324239206123946861308263804e0,                                   NAN,  2.00001, -2.00002, 4.00003);

	_try(       -1e-200, -4.6666262516534690504306811971824e2, -1e-200,              0, -1e-200);
	_try2(log(10) * 200, -6.1456065665377682394698287813708e0,      -1, log(10) * -200);
	_try(       -1e-308, -7.1576956692342123433509525651669e2, -1e-308,              0, -1e-308);
	_try2(log(10) * 308, -6.5733582812551636575538884739074e0,      -1, log(10) * -308);

	_try(        1e-200,                                  NAN,  1e-200,              0,  1e-200);
	_try2(log(10) * 200,                                  NAN,       1, log(10) * -200);
	_try(        1e-308,                                  NAN,  1e-308,              0,  1e-308);
	_try2(log(10) * 308,                                  NAN,       1, log(10) * -308);

	_try(  4.5439804503371401612565206459903e2, NAN, 1e200,             0, 1e200);
	_try2(-6.1189735650951206779462263378395e0, NAN,     1, log(10) * 200);
	_try(  7.0264136203410681208112467575223e2, NAN, 1e308,             0, 1e308);
	_try2( -6.554846608059258596416692290551e0, NAN,     1, log(10) * 308);

	putchar('\n');

	_try2(2980.9006027528962, -2.8890824032014667211550419963464e-1, -2233.1522342853127, -2980.9006027528962);
	_try2(2.3411784071376925969585554130714e1, -2.0258992947141659536655600171718e0, -3.3545994588631296, -23.411784071604977);
	_try2(-1.2699367515813415983799302868918e1, NAN, 1, 327553.3714179078);
	_try2(3.3286731595264345497487311403737e1, NAN, 1, -33.286731595264342);
	_try2(727.638794, NAN, 1, -727.638794);
	_try2(1.9165461708320743035812685163996e-5, -4.3582171339959284773701079394266e1, -5.1500628897021026e-18, -1.9165461708325893e-05);
	_try2(-6.4553870446146443364001230717452e1, -7.3590698807266621063419103280438e1, -9.9090788350588469e-32,       64.55279541015625);
	_try2(4.8023445129461956502168404236768e1, NAN, 48433004544, -48.023445129394531);
	_try2(-2.9763997250486042498293710305111e-17, -4.1785788243374966572657581783397e1, -2.9764003230132167e-17, -5.979646125387602e-24);
	_try2(8.3937846065213900023556452561922e1, 8.3667377708249325670309323845896e1, -2.4756484154235688e+36, -84.808700561523438);
	_try2(-2.9664845399994279197382973300143e0, NAN, 0.029355306178331375,      3.5366678237915039);
	_try2(804.983642578125, 5.3237438464825736766987820512937e1, -9.9265194934332144e+25, -804.983642578125);
	_try(2.5797624617316863113837163254324e-3, 4.5257301530955572672694039602907e-5, -1.0013131035933589, -1.0013130451912746, -5.84020843e-8);
	_try2(-5.7649671755436563331314829482027e1, -5.7717020440957000530047276003174e1, -8.8788969614131644e-26, 56.682968139648438);
	_try2(5.6236731726411611785129580955056e-1, NAN, 0.99523365497589111, 0.0047749709337949753);
	_try2(727.6387939453125, 4.8957273698040732368927236254137e0, -96643.203125, -727.6387939453125);
	_try2(3.632890526207112938154730533685e-3, NAN, 1.8182316072136613e-15, -0.0036328905262053013);
	_try2(596180120.55057919, -2.8881132571774772856729022527493e-1, -446630446.85706258, -596180120.55057919);
	_try2(744.8922119140625, -7.04636858956452125074167334066e0, -0.65461111068725586, -744.8922119140625);
	_try2(3.691377639771309619760303074784e-1, NAN, 1.1598217503593078e-13, -0.36913776397705078);
	_try2(-1.0082880505193604215331415958495e-1, -3.70478531803443274771202863155e0, -0.091158092021942139, -4.0753509217256214e-38);
	_try2(-1.7604877425796412988323166053958e-4, -1.1047092824137670919921152909063e1, -0.00017601778381504118, -2.6511438372873535e-33);
	_try2(3.7731064416275766175263054881374e1, 3.7410908493761930431932250343723e1, -20654699589926912, -38.579513549804688);
	_try2(7.7108780812207964220438349599083e1, NAN, 2.3714930130500916e+35, -4.2975894721614645e-19);
	_try2(8.4479705284140243e+25, 1.282158641840436865725448143782e1, -3.126774927687506e+31, -8.4479705284140243e+25);
	_try2(3699783076151296, NAN, 0.24544727802276611, -3699783076151296);
	_try2(17651014211469312, -2.5704924857590364949680883695986e1, -121133.046875, -17651014211469312);
	_try2(19809991191953408, -3.0866942206390420517743718753503e1, -779.0074462890625, -19809991191953408);
	_try2(13961514897637376, -3.262120025693501790242391414225e1, -95.0003662109375, -13961514897637376);
	_try2(17756938842406912, NAN, 0.99989211559295654, -17756938842406912);
	_try2(25330170923581440, NAN, 0.99997031688690186, -25330170923581440);

	_try2(1.6329282681507985234452417847428e1, NAN, 201690048, -1.0270137266120936e-14);
	_try2(-6.9294713548131163209277615660245e-1, NAN, 2.4643563460357417e+20, 4.9277268259655542e+20);
	_try2(3.27194246743721726923870643663e0, NAN, 86.256576538085938, -7.2181797248582844e-16);
	_try2(3.9211290903588699560458276866234e0, NAN, 197.84991455078125, -7.6335192033655066e-23);
	_try2(-2.0367495166314821601430858036584e0, NAN, 2.2153491973876953, 19.018844604492188);
	_try2(5.5925820104632944778990702252497e-1, NAN, 0.97835195064544678, 4.1156784647000677e-09);

	_try2(3.876554107666015625e1, NAN, 1.6738997743459549e-08, -38.765541076660156);
	_try2(4.6360954284667969e1, NAN, 1.6738997743459549e-08, -46.360954284667969);
	_try2(4.6360954284667969e1, NAN, 2.6520996954104703e-08, -46.360954284667969);

	_try(6.0819221496582031543822424755101e1, NAN, 140899483648, -60.819221496582031, 1.40899483708819221496582031e11);
	_try(7.2670288085937499392234955531175e1, 3.3977901939015073425579727283163e1, -22082123380817920, -72.6702880859375, -2.20821233808178473297119140625e16);
	_try(1.0956078415027837801934455536549e-3, -6.1861346189932791525942843477226e-6, -1.0005448086265329, -1.000544812015937, 3.3894040621022243e-09);

	_try2(6.0675171462230306452465926254316e-7, NAN, 6.0675208277005945e-07, 1.5012402706280458e-21);

	puts("-");

	for(;;)
	{
		for(size_t i = 1000000; i; --i)
		{
			double b = _double_rand(), a = _double_rand();
			if(isfinite(a) && isfinite(b) && lambert_w_branches(b, a))
			{
				// _try(b, a);
				lambert_w0_2(b, a, b - a);
				if(b < 0)
					lambert_wm1_2(b, a, b - a);
			}
		}

		putchar('.');
	}

	return 0;
}

#endif
