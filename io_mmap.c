#include "io_mmap.h"

#if _WIN32

io_result io_mmap_new(struct io_mmap *self, const _TCHAR *path)
{
	self->file = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if(self->file == INVALID_HANDLE_VALUE)
		return GetLastError();

	self->size.LowPart = GetFileSize(self->file, &self->size.HighPart);
	if(self->size.LowPart == INVALID_FILE_SIZE)
	{
		DWORD last_error = GetLastError();
		if(last_error)
		{
			CloseHandle(self->file);
			return last_error;
		}
	}

	self->mapping = CreateFileMapping(self->file, NULL, PAGE_READONLY, 0, 0, NULL);
	if(!self->mapping)
	{
		DWORD last_error = GetLastError();
		CloseHandle(self->file);
		return last_error;
	}

	self->view = MapViewOfFile(self->mapping, FILE_MAP_READ, 0, 0, 0);
	if(!self->view)
	{
		DWORD last_error = GetLastError();
		CloseHandle(self->mapping);
		CloseHandle(self->file);
		return last_error;
	}

	return ERROR_SUCCESS;
}

void io_mmap_delete(struct io_mmap *self)
{
	UnmapViewOfFile(self->view);
	CloseHandle(self->mapping);
	CloseHandle(self->file);
}

io_result io_mmap_view(struct io_mmap *self, io_mmap_size_type offset, size_t length, void **addr)
{
	*addr = (char *)self->view + offset;
	return 0;
}

io_result io_mmap_read(struct io_mmap *self, io_mmap_size_type offset, size_t length, void *addr)
{
	memcpy(addr, (char *)self->view + offset, length);
	return 0;
}

#else

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#if !__DJGPP__
#	define HAS_MMAP 1
#else

static ssize_t pread(int fd, void *buf, size_t length, size_t offset)
{
	int result = lseek(fd, offset, SEEK_SET);
	if(result < 0)
		return result;

	return read(fd, buf, length);
}

#endif

// int page_size = sysconf(_SC_PAGE_SIZE);

// For fake mmap only.
const size_t page_size = 4096;
const size_t page_mask = ~(size_t)(page_size - 1);

static const off_t _mapped = ~((off_t)1 << (CHAR_BIT * sizeof(off_t) - 1));

static io_result _fail(struct io_mmap *self, int error)
{
	close(self->fd);
	return error;
}

io_result io_mmap_new(struct io_mmap *self, const char *path)
{
	self->fd = open(path, O_RDONLY);
	if(self->fd < 0)
		return errno;

	struct stat st;
	if(fstat(self->fd, &st))
		return _fail(self, errno);

	self->file_size = st.st_size;

#if HAS_MMAP
	self->base = mmap(NULL, self->file_size, PROT_READ, MAP_SHARED, self->fd, 0);
	if(self->base == MAP_FAILED)
	{
		int error = errno;
		if(error != ENODEV)
			return _fail(self, error);
#endif

		// Rarely. vboxsf once did this, now it doesn't.
		self->base = NULL;
		self->length = 0;
		self->offset = 0;
#if HAS_MMAP
	}
	else
	{
		self->offset = 0;
		self->length = _mapped;
	}
#endif

	return 0;
}

void io_mmap_delete(struct io_mmap *self)
{
#if HAS_MMAP
	if(self->length == _mapped)
		munmap(self->base, self->file_size);
	else
#endif
		free(self->base);
	close(self->fd);
}

static io_result _fail_view(struct io_mmap *self, io_result result)
{
	free(self->base);
	self->base = NULL;
	self->length = 0;
	return result;
}

static void _copy_cached(const struct io_mmap *self, void *dest, size_t offset0, size_t offset1, size_t *src0, size_t *src1)
{
	*src0 = self->offset;
	*src1 = self->offset + self->length;

	if(*src0 < offset0)
		*src0 = offset0;
	if(*src1 > offset1)
		*src1 = offset1;
	assert(*src1 >= *src0);
	memcpy(dest + (*src0 - offset0), self->base + (*src0 - self->offset), *src1 - *src0);
}

static io_result _pread_edges(int fd, char *dest, size_t offset0, size_t offset1, size_t src0, size_t src1)
{
	if(src0 != offset0)
	{
		if(pread(fd, dest, src0 - offset0, offset0) < 0)
			return errno;
	}

	if(src1 != offset1)
	{
		if(pread(fd, dest + src1 - offset0, offset1 - src1, src1) < 0)
			return errno;
	}

	return 0;
}

io_result io_mmap_view(struct io_mmap *self, off_t offset, size_t length, void **addr)
{
	// No buffer change if the requested region is already fully loaded.
	if(offset < self->offset || offset + length > self->offset + self->length)
	{
		assert(self->length != _mapped);

		size_t offset0 = offset & page_mask, offset1 = (offset + length + page_size - 1) & page_mask;
		size_t length0 = offset1 - offset0;

		if(offset1 <= self->offset || offset0 >= self->offset + self->length)
		{
			// Replace the whole buffer if there's no overlap.
			if(self->length > length0 + 2 * page_size || self->length < length0)
			{
				free(self->base);
				self->base = malloc(length0);
				if(!self->base)
				{
					self->length = 0;
					return io_result_no_memory;
				}
			}

			self->length = length0;
			self->offset = offset0;

			if(pread(self->fd, self->base, self->length, self->offset) < 0)
				return _fail_view(self, errno);
		}
		else
		{
			if(offset0 < self->offset)
			{
				if(offset1 < self->offset + self->length)
				{
					length0 *= 2;
					if(length0 > self->length)
						length0 = self->length;
					offset1 = offset0 + length0;
				}
			}
			else if(offset1 > self->offset + self->length)
			{
				if(offset0 > self->offset)
				{
					length0 *= 2;
					if(length0 > self->length)
						length0 = self->length;
					offset0 = offset1 - length0;
				}
			}

			size_t src0, src1;

			if(self->offset == offset0)
			{
				// Slight optimization, not strictly necessary.
				if(length0 > self->length || length0 + 2 * page_size <= self->length)
				{
					static unsigned n;
					void *new_base = realloc(self->base, length0 + n);
					n += 1;
					if(!new_base)
						return _fail_view(self, io_result_no_memory);
					self->base = new_base;
				}

				src0 = self->offset;
				src1 = self->offset + self->length;
			}
			else
			{
				void *new_base = malloc(length0);
				if(!new_base)
					return _fail_view(self, io_result_no_memory);
				_copy_cached(self, new_base, offset0, offset1, &src0, &src1);
				free(self->base);
				self->base = new_base;
			}

			io_result result = _pread_edges(self->fd, self->base, offset0, offset1, src0, src1);
			if(result)
				return _fail_view(self, result);

			self->offset = offset0;
			self->length = length0;
		}
	}

	*addr = self->base + offset - self->offset;
	return 0;
}

io_result io_mmap_read(struct io_mmap *self, off_t offset, size_t length, void *addr)
{
	size_t src0 = self->offset, src1 = self->offset + self->length;
	size_t offset1 = offset + length;
	_copy_cached(self, addr, offset, offset1, &src0, &src1);
	return _pread_edges(self->fd, addr, offset, offset1, src0, src1);
}

#endif
