/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "box_intg.h"

#include "lambert_w_2.h"

#include <gsl/gsl_sf_dilog.h>

#include <assert.h>

static size_t _line_u(double lC, const struct box_mb s, const double *v, double *u, double (*g)[4])
{
	double det = v[1] - s.m;
	if(det == 0)
		return 0;

	det = 1 / det;

	if(g)
	{
		double d = s.b * det * det;
		g[0][0] = d;
		g[0][1] = det;
		g[0][2] = 0;
		g[0][3] = -d;
	}

	*u = s.b * det;
	return 1;
}

static double _line_i(double lC, double u, const double *v)
{
	// integrate(v1*u,u);
	return 0.5 * v[1] * u * u;
}

static double _line_idiv(double lC, double u, const double *v, double *grad)
{
	// integrate(v1,u);
	if(grad)
	{
		grad[0] = v[1];
		grad[1] = 0;
		grad[2] = u;
	}

	return v[1] * u;
}

const struct box_vtbl *box_line(double lC, double y0, double y1, struct box_mb t, double *v, double (*g)[4])
{
	double dy = y1 - y0;
	v[0] = dy * t.b + y0;
	v[1] = dy * t.m;

	if(g)
	{
		g[0][0] = 1 - t.b;
		g[0][1] = t.b;
		g[0][2] = 0;
		g[0][3] = dy;

		g[1][0] = -t.m;
		g[1][1] = t.m;
		g[1][2] = dy;
		g[1][3] = 0;
	}

	static const struct box_vtbl vtbl = {_line_u, _line_i, _line_idiv};
	return &vtbl;
}

static void _lambert_w_solve_grad_y(double x, double y, double m0, double m1, double *grad)
{
	double g1 = 1 / (m0 - m1 * y);
	double g0 = g1 * x;

	// Protip: exp(wa) == 1 / y

	// d/dm0, db0, dm1, db1
	grad[0] = -g0;
	grad[1] = -g1;
	grad[2] = y * g0;
	grad[3] = y * g1;
}

static double _lambert_w_solve_grad(double wa, double m0, double b0, double m1, double b1, double *grad)
{
	double x = -(b1 + wa) / m1;
	if(grad)
		_lambert_w_solve_grad_y(x, m0 * x + b0, m0, m1, grad);
	return x;
}

static inline size_t _lambert_w_solve(double m0, double b0, double m1, double b1, double *x, double (*grad2)[4])
{
	// solve(m0*x+b0=exp(m1*x+b1),x)
	// x=-(m0*lambert_w(-(m1*%e^((b1*m0-b0*m1)/m0))/m0)+b0*m1)/(m0*m1)

	if(m0 == 0)
	{
		if(m1 == 0 || b0 <= 0)
			return 0;

		x[0] = _lambert_w_solve_grad(-log(b0), m0, b0, m1, b1, grad2 ? grad2[0] : NULL);
		return 1;
	}

	if(m1 == 0)
	{
		assert(m0 != 0);

		double y = exp(b1);
		x[0] = (y - b0) / m0;
		if(grad2)
			_lambert_w_solve_grad_y(x[0], y, m0, m1, grad2[0]);
		return 1;
	}

	double b = -m1 / m0;
	double a = b1 + b0 * b;

	if(!lambert_w_branches(b, a))
		return 0;

	double ba = (1 - b0) * b - b1;

	x[0] = _lambert_w_solve_grad(lambert_w0_2(b, a, ba), m0, b0, m1, b1, grad2 ? grad2[0] : NULL);
	assert(isfinite(x[0]));

	if(b >= 0)
		return 1;

	x[1] = _lambert_w_solve_grad(lambert_wm1_2(b, a, ba), m0, b0, m1, b1, grad2 ? grad2[1] : NULL);
	assert(isfinite(x[1]));
	return 2;
}

static size_t _c0_u(double lC, const struct box_mb s, const double *v, double *u, double (*g)[4])
{
	double g_x[2][4];
	size_t n = _lambert_w_solve(1, v[1], lC * s.m, lC * s.b, u, g ? g_x : NULL);

	if(g)
	{
		for(size_t i = 0; i != n; ++i)
		{
			double *gi = g_x[i];
			g[i][0] = lC * gi[2];
			g[i][1] = lC * gi[3];
			g[i][2] = 0;
			g[i][3] = gi[1];
		}
	}

	return n;
}

static double _c0_i(double lC, double u, const double *v)
{
	// TODO: Test.
	// Only called when pos.x0 == pos.x1 and y0 == -INFINITY.
	double v1u = v[1] + u;
	assert(v1u >= -1.0 / (1ull << 56));

	if(v1u <= 0)
		return 0;
	return v1u * (log(v1u) - 1) / lC;
}

static double _c0_idiv(double lC, double u, const double *v, double *grad)
{
	double log_u = log(u);

	if(grad)
	{
		double log_vu = log(v[1] + u);
		grad[0] = log_vu / (lC * u);
		grad[1] = 0;
		grad[2] = (log_u - log_vu) / (lC * v[1]);
	}

	return (gsl_sf_dilog(-v[1] / u) + log_u * log_u * 0.5) / lC;
}

static size_t _c_u(double lC, const struct box_mb s, const double *v, double *u, double (*g)[4])
{
	double g_x[2][4];
	size_t n = _lambert_w_solve(v[1], 1, lC * s.m, lC * s.b, u, g ? g_x : NULL);

	if(g)
	{
		for(size_t i = 0; i != n; ++i)
		{
			double *gi = g_x[i];
			g[i][0] = lC * gi[2];
			g[i][1] = lC * gi[3];
			g[i][2] = 0;
			g[i][3] = gi[0];
		}
	}

	return n;
}

static double _c_i(double lC, double u, const double *v)
{
	// integrate(log1p(v1*u)/lC,u)

	// lC will never actually == 0.
	// If it ever does, it turns into (y1-y0)*t+y0.

	if(v[1] == 0)
		return 0;

	double v1u = v[1] * u;
	double result = v1u == -1 ? 0 : (v1u + 1) * (log1p(v1u) - 1);
	assert(v1u >= -1);
	return result / (lC * v[1]);
}

static double _c_idiv(double lC, double u, const double *v, double *grad)
{
	double r = u * v[1];

	if(grad)
	{
		double log_r = (r == 0 ? 1 / lC : log1p(r) / (r * lC));
		grad[0] = v[1] * log_r;
		grad[1] = 0;
		grad[2] = u * log_r;
	}

	return -gsl_sf_dilog(-r) / lC;
}

const struct box_vtbl *box_c(double lC, double y0, double y1, struct box_mb t, double *v, double (*g)[4])
{
	static const struct box_vtbl c0_vtbl = {_c0_u, _c0_i, _c0_idiv};

	if(y0 == -INFINITY)
	{
		assert(lC > 0);

		assert(t.m > 0);

		// TODO: Test me!
		v[0] = log(t.m) / lC + y1;
		v[1] = t.b / t.m;

		if(g)
		{
			double tm1 = 1 / t.m;
			g[0][0] = 0;
			g[0][1] = 1;
			g[0][2] = tm1 / lC;
			g[0][3] = 0;

			g[1][0] = 0;
			g[1][1] = 0;
			g[1][2] = -t.b * tm1 * tm1;
			g[1][3] = tm1;
		}

		return &c0_vtbl;
	}

	double em = expm1(lC * (y1 - y0));
	double emb = em * t.b;

#if 0
	{
		double em_tm = em * t.m;
		v[0] = log(-em_tm) / lC + y0;
		v[1] = (emb + 1) / -em_tm;

		if(g)
		{
			g[0][0] = -1 / em;
			g[0][1] = 1 / em + 1;
			g[0][2] = 1 / (lC * t.m);
			g[0][3] = 0;

			g[1][0] = -((em+1)*lC)/(em*em*t.m);
			g[1][1] = ((em+1)*lC)/(em*em*t.m);
			g[1][2] = (em*t.b+1)/(em*t.m*t.m);
			g[1][3] = -1 / t.m;
		}

		static const struct box_vtbl c1_vtbl = {_c1_u, _c1_i_plain, _c1_i};
		return &c1_vtbl;
	}
#endif

	if(emb <= -1)
	{
		v[0] = log(em * t.m) / lC + y0;
		v[1] = (emb + 1) / (em * t.m);

		if(g)
		{
			double em1 = 1 / em, tm1 = 1 / t.m;
			double em1_em1 = (em + 1) * em1;
			g[0][0] = -em1;
			g[0][1] = em1_em1;
			g[0][2] = tm1 / lC;
			g[0][3] = 0;

			double em1_tm1 = em1 * tm1;
			double f = em1_em1 * lC * em1_tm1;
			g[1][0] = f;
			g[1][1] = -f;
			g[1][2] = -(emb + 1) * em1_tm1 * tm1;
			g[1][3] = tm1;
		}

		return &c0_vtbl;
	}

	// x0 = -t.b/t.m
	// x1 = (1-t.b)/t.m
	double em_tm = em * t.m;
	v[0] = log1p(emb) / lC + y0;
	v[1] = em_tm / (emb + 1);

	if(g)
	{
		double f = 1 / (emb + 1);
		double fem = f * em;
		double fem1 = fem + f;
		g[0][0] = f * (1 - t.b);
		g[0][1] = fem1 * t.b;
		g[0][2] = 0;
		g[0][3] = fem / lC;

		double f0 = f * t.m;
		double f1 = f0 * fem1 * lC;
		g[1][0] = -f1;
		g[1][1] = f1;
		g[1][2] = fem;
		g[1][3] = -f0 * fem * em;
	}

	static const struct box_vtbl c_vtbl = {_c_u, _c_i, _c_idiv};
	return &c_vtbl;
}

#if 0

#include "point.h"

static const double _lC = log(2) / 12;

#include <stdio.h>

static struct box_mb _mb0(const struct point *seg)
{
	struct box_mb mb;

	if(seg[0].y == -INFINITY && seg[1].y == -INFINITY)
	{
		mb.m = 0;
		mb.b = -INFINITY;
	}
	else
	{
		double dx = seg[1].x - seg[0].x;
		mb.m = (seg[1].y - seg[0].y) / dx;
		mb.b = (seg[1].x * seg[0].y - seg[0].x * seg[1].y) / dx;
	}

	return mb;
}

double _f(const struct box_vtbl *vtbl, double lC, double x, const double *v)
{
	double g[3];
	vtbl->idiv(lC, x, v, g);
	return g[0] * x;
}

int main()
{
	const double _d = 1e-4;
	const box_v box_v = box_c;

	const struct point src[] = {{0.15, 110.0}, {0.92, 34.0}};
	struct box_mb mb_s = _mb0(src);

	struct point dst[] = {{0.25, 97.0}, {0.8, 12.5}};
	//struct point dst[] = {{0.25, 12.5}, {0.8, 97.0}};

	double dx = dst[1].x - dst[0].x;
	struct box_mb mb_t = {1 / dx, -dst[0].x / dx};

	const size_t v_diffs = 4;
	double v[2];
	double v_g[2][v_diffs];
	const struct box_vtbl *vtbl = box_v(_lC, dst[0].y, dst[1].y, mb_t, v, v_g);

#if 1
	printf("%.17g\t%.17g\n", dst[0].y, dst[1].y);
	printf("%.17g\t%.17g\n", _f(vtbl, _lC, dst[0].x, v) + v[0], _f(vtbl, _lC, dst[1].x, v) + v[0]);
#endif

#if 0
	{
		const size_t i = 1;
		//dst[1].y += _d;
		mb_t.b += _d;
		double v0[2];
		box_v(_lC, dst[0].y, dst[1].y, mb_t, v0, NULL);
		for(size_t k = 0; k != 2; ++k)
		{
			for(size_t j = 0; j != 4; ++j)
				printf("%.17g\t", v_g[k][j]);
			putchar('\n');
		}
		printf("%.17g\n", (v0[i] - v[i]) / _d);
	}
#endif

#if 0
	double u[2];
	double g_u[2][4];
	mb_s.b -= v[0];
	size_t n = vtbl->u(_lC, mb_s, v, u, g_u);
	const size_t j = 1;
	printf("%.17g\n", _f(vtbl, _lC, u[j], v) + v[0]);
	printf("%.17g\n", mb_s.m * u[j] + mb_s.b + v[0]);

	//mb_s.b += _d;
	v[1] += _d;
	double u1[2];
	vtbl->u(_lC, mb_s, v, u1, NULL);
	for(size_t i = 0; i != 4; ++i)
		printf("%.17g\t", g_u[j][i]);
	putchar('\n');
	printf("%.17g\n", (u1[j] - u[j]) / _d);
#endif

/*
	double id[3];
	double i = vtbl->idiv(_lC, dst[0].x, v, id);

	//v[1] += _d;
	dst[0].x += _d;
	printf("%.17g\n", (vtbl->idiv(_lC, dst[0].x, v, NULL) - i) / _d);

	printf("%.17g\t%.17g\t%.17g\n", id[0], id[1], id[2]);
*/
	return 0;
}

#endif
