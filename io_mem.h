/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_MEM_H
#define IO_MEM_H

#include <stddef.h>
#include <string.h>

struct io_mem_iterator
{
	struct io_mem_iterator *_next;
	size_t _size;
};

struct io_mem
{
	struct io_mem_iterator *_first;
	struct io_mem_iterator *_last;
	size_t _last_capacity;
};

static inline void io_mem_new(struct io_mem *self)
{
	self->_first = NULL;
	self->_last = NULL;
	self->_last_capacity = 0;
}

void io_mem_delete(struct io_mem *self);

void *io_mem_write_begin(struct io_mem *self, size_t size);

static inline void io_mem_write_commit(struct io_mem *self, size_t size)
{
	self->_last->_size += size;
}

static inline void io_mem_write_rollback(struct io_mem *self)
{
}

static inline int io_mem_write(struct io_mem *self, const void *data, size_t size)
{
	void *dest = io_mem_write_begin(self, size);
	if(!dest)
		return 1;
	memcpy(dest, data, size);
	io_mem_write_commit(self, size);
	return 0;
}

static inline struct io_mem_iterator *io_mem_begin(const struct io_mem *self)
{
	return self->_first;
}

static inline struct io_mem_iterator *io_mem_end(const struct io_mem *self)
{
	return NULL;
}

static inline const struct io_mem_iterator *io_mem_iterator_next(const struct io_mem_iterator *self)
{
	return self->_next;
}

static inline size_t io_mem_iterator_size(const struct io_mem_iterator *self)
{
	return self->_size;
}

static inline const void *io_mem_iterator_data(const struct io_mem_iterator *self)
{
	return self + 1;
}

#endif
