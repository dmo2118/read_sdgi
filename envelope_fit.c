/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "envelope_fit.h"

#include "box_intg.h"

#include <nlopt.h>

#include <assert.h>
#include <float.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define arraysize(a) (sizeof(a) / sizeof(*(a)))
#define arrayend(a) ((a + arraysize(a)))

// TODO: This is probably unnecessary.
static const double _error_scale = 1.0 / (1 << 20); // $B0,mod needs at least 1 << 7, $8B,vol needs 1 << 20.

enum _fit_segment
{
	_FIT_DELAY_Y,
	_FIT_DELAY_DX,
	_FIT_ATTACK_DX,
	_FIT_HOLD_Y,
	_FIT_HOLD_DX,
	_FIT_DECAY_DX,
	_FIT_SUSTAIN_Y,
	_FIT_SUSTAIN_DX,

	_FIT_COUNT_BASE, // TODO: Change back to _FIT_COUNT.

	_FIT_SUSTAIN_DY_DELAY = _FIT_COUNT_BASE,
	_FIT_HOLD_DY_SUSTAIN,
	_FIT_ATTACK_DX_X0,
	_FIT_DELAY_X_T,

	_FIT_COUNT_FULL
};

enum
{
	_FIT_XY_ATTACK_Y,
	_FIT_XY_ATTACK_X,
	_FIT_XY_HOLD_X,
	_FIT_XY_HOLD_Y,
	_FIT_XY_DECAY_X,
	_FIT_XY_SUSTAIN_X,
	_FIT_XY_SUSTAIN_Y,
	_FIT_XY_END_X,

	_FIT_XY_COUNT
};


typedef uint16_t enums_flags;

struct _flex_fit
{
	struct box_xmb src_begin[envelope_fit_max_src_n];
	const struct box_xmb *src_end;

	/*
	Up to two envelopes, where the second envelope is muted while the first one plays. Only the delay and attack phases are
	allowed for the second envelope while it's muted by the first, because if the second envelope is totally muted by the first
	envelope, then the gradient won't point in a direction that would get the second envelope out from behind the first.
	*/
	size_t envelope_count;
	enums_flags enums[envelope_fit_max_envelopes];

	double (*params)[_FIT_COUNT_FULL];

	double y_max;
	double lC;

	bool enforce_gain;
};

double *_read_x0(enums_flags enums, const double *params, double *p, size_t n)
{
	for(size_t i = 0; i != n; ++i)
	{
		if(enums & (1 << i))
		{
			*p = params[i];
			++p;
		}
	}

	return p;
}

static void _read_x_funcs(double x0, double *params)
{
	params[_FIT_SUSTAIN_DY_DELAY] = params[_FIT_SUSTAIN_Y] - params[_FIT_DELAY_Y];
	params[_FIT_HOLD_DY_SUSTAIN] = params[_FIT_HOLD_Y] - params[_FIT_SUSTAIN_Y];

	double hold_x = params[_FIT_DELAY_DX] + params[_FIT_ATTACK_DX];
	params[_FIT_ATTACK_DX_X0] = hold_x - x0;
	if(params[_FIT_ATTACK_DX_X0] < 0)
	{
		assert(params[_FIT_ATTACK_DX_X0] > -9e-16);
		params[_FIT_ATTACK_DX_X0] = 0;
	}
	params[_FIT_DELAY_X_T] = hold_x == 0 ? 0 : params[_FIT_DELAY_DX] / hold_x;
}

double _params_dx(const double *params)
{
	return
		params[_FIT_DELAY_DX] +
		params[_FIT_ATTACK_DX] +
		params[_FIT_HOLD_DX] +
		params[_FIT_DECAY_DX] +
		params[_FIT_SUSTAIN_DX];
}

void _write_x(struct _flex_fit *self, const double *p)
{
	double x0 = 0;

	for(size_t e = 0; e != self->envelope_count; ++e)
	{
		size_t enums = self->enums[e];
		double *params = self->params[e];

		for(size_t i = 0; i != _FIT_COUNT_FULL; ++i)
		{
			if(enums & (1 << i))
			{
				params[i] = *p;
				++p;
			}
		}

		/*
		if(enums & (1 << _FIT_SUSTAIN_Y))
			params[_FIT_SUSTAIN_DY_DELAY] = params[_FIT_SUSTAIN_Y] - params[_FIT_DELAY_Y];
		if(enums & (1 << _FIT_HOLD_Y))
			params[_FIT_HOLD_DY_SUSTAIN] = params[_FIT_HOLD_Y] - params[_FIT_SUSTAIN_Y];

		if(enums & ((1 << _FIT_DELAY_DX) | (1 << _FIT_ATTACK_DX)) == ((1 << _FIT_DELAY_DX) | (1 << _FIT_ATTACK_DX)))
		{
			double hold_x = params[_FIT_DELAY_DX] + params[_FIT_ATTACK_DX];
			params[_FIT_ATTACK_DX_X0] = hold_x - x0;
			params[_FIT_DELAY_X_T] = params[_FIT_DELAY_DX] / hold_x;
		}
		else
		{
			assert(!(enums & ((1 << _FIT_DELAY_DX) | (1 << _FIT_ATTACK_DX))));
		}
		*/

		if(enums & (1 << _FIT_SUSTAIN_DY_DELAY))
			params[_FIT_SUSTAIN_Y] = params[_FIT_DELAY_Y] + params[_FIT_SUSTAIN_DY_DELAY];

		if(enums & (1 << _FIT_HOLD_DY_SUSTAIN))
			params[_FIT_HOLD_Y] = params[_FIT_SUSTAIN_Y] + params[_FIT_HOLD_DY_SUSTAIN];

		if(enums & (1 << _FIT_ATTACK_DX_X0))
		{
			double hold_x = params[_FIT_ATTACK_DX_X0] + x0;
			if(enums & (1 << _FIT_DELAY_X_T))
				params[_FIT_DELAY_DX] = hold_x * params[_FIT_DELAY_X_T];
			params[_FIT_ATTACK_DX] = hold_x - params[_FIT_DELAY_DX];
		}
		else
		{
			assert(!(enums & ((1 << _FIT_ATTACK_DX_X0) | (1 << _FIT_DELAY_X_T))));
		}

		x0 = _params_dx(params);
	}
}

static struct box_xmb *_mb(const struct point *lines, struct box_xmb *xmb, size_t n)
{
	for(unsigned i = 0; i != n; ++i)
	{
		const struct point *seg = lines + i;

		xmb->x = box_warp(seg->x);

		struct box_mb *mb = &xmb->mb;

		// seg[0].x == seg[1].x is normally filtered out in sdgi2sf2.c. Still needs to be removed here, though.
		if(seg[0].x != seg[1].x)
		{
			if(seg[0].y == -INFINITY && seg[1].y == -INFINITY)
			{
				mb->m = 0;
				mb->b = seg[0].y;
			}
			else
			{
				double dx = seg[1].x - seg[0].x;
				mb->m = (seg[1].y - seg[0].y) / dx;
				mb->b = seg[0].x == 0 ? seg[0].y : (seg[1].x * seg[0].y - seg[0].x * seg[1].y) / dx; // Be exact for box_sqr().
			}

			++xmb;
		}
	}

	xmb->x = box_warp(lines[n].x);
	xmb->mb.m = 0;
	xmb->mb.b = lines[n].y;

	return xmb;
}

void line_range_new(struct box_line_range *self, const struct box_xmb *src, const struct point *dst)
{
	self->seg_src = src;
	self->seg_dst = dst;
	self->x0 = 0;
	self->has_x1_0 = true; // Edge case fails if false.
}

bool line_range_more(struct box_line_range *self, const struct box_xmb *src_end, const struct point *dst_end)
{
	while(self->seg_src != src_end && self->seg_src[1].x <= self->seg_dst[0].x)
		++self->seg_src;

	if(self->seg_src == src_end && self->seg_dst == dst_end)
		return false;

	if(self->seg_src == src_end)
		self->has_x1_1 = 1;
	else if(self->seg_dst == dst_end)
		self->has_x1_1 = 0;
	else
		self->has_x1_1 = self->seg_dst[1].x <= self->seg_src[1].x;

	self->x1 = self->has_x1_1 ? self->seg_dst[1].x : self->seg_src[1].x;
	return true;
}

void line_range_next(struct box_line_range *self)
{
	self->x0 = self->x1;
	self->has_x1_0 = self->has_x1_1;
	if(self->has_x1_1)
		++self->seg_dst;
	else
		++self->seg_src;
}

static double _write_envelope_x(double params_dx, double *x_warp)
{
	double x_warp1 = *x_warp + params_dx;
	double result = box_warp_inv_sub(x_warp1, *x_warp);
	*x_warp = x_warp1;
	return result;
}

static void _write_envelope(const double *params, struct envelope_fit *envelope_out)
{
	for(size_t i = 0; i != _FIT_COUNT_BASE; ++i)
	{
		double p = params[i];
		assert((i == _FIT_DELAY_Y && p == -INFINITY) || isfinite(p));
	}

	double x_warp = 0;

	envelope_out->delay_y = params[_FIT_DELAY_Y];
	envelope_out->delay_dx = _write_envelope_x(params[_FIT_DELAY_DX], &x_warp);
	envelope_out->attack_dx = _write_envelope_x(params[_FIT_ATTACK_DX], &x_warp);
	envelope_out->hold_y = params[_FIT_HOLD_Y];
	envelope_out->hold_dx = _write_envelope_x(params[_FIT_HOLD_DX], &x_warp);
	envelope_out->decay_dx = _write_envelope_x(params[_FIT_DECAY_DX], &x_warp);
	envelope_out->sustain_y = params[_FIT_SUSTAIN_Y];

	// assert(0 <= envelope_out->attack_x);
	// assert(envelope_out->attack_x <= envelope_out->hold_x);
	// assert(envelope_out->hold_x <= envelope_out->decay_x);
	// assert(envelope_out->decay_x <= envelope_out->sustain_x);
}

double _param_y(double x0, double dx, double y0, double y1)
{
	assert(dx);
	return _b2(x0 / dx, y0, y1);
}

static double _split_attack_y0(double lC, double dx0, double dx, double y0, double y1)
{
	double result = log1p(expm1(lC * (y1 - y0)) * dx0 / dx) / lC + y0;
	assert(isfinite(result));
	return result;
}

static bool _needs_clip(double attack_x, const double (*dst)[_FIT_XY_COUNT])
{
	return (*dst)[_FIT_XY_HOLD_X] - attack_x != 0 && attack_x < dst[-1][_FIT_XY_END_X];
}

static void _segments_xy_clip_attack(double lC, size_t n, double (*dst)[_FIT_XY_COUNT])
{
	for(size_t i = 1; i < n; ++i)
	{
		++dst;

		if(_needs_clip((*dst)[_FIT_XY_ATTACK_X], dst))
		{
			(*dst)[_FIT_XY_ATTACK_Y] = _split_attack_y0(
				lC,
				box_warp_inv_sub(dst[-1][_FIT_XY_END_X], (*dst)[_FIT_XY_ATTACK_X]),
				box_warp_inv_sub((*dst)[_FIT_XY_HOLD_X], (*dst)[_FIT_XY_ATTACK_X]),
				(*dst)[_FIT_XY_ATTACK_Y],
				(*dst)[_FIT_XY_HOLD_Y]);
			(*dst)[_FIT_XY_ATTACK_X] = dst[-1][_FIT_XY_END_X];
		}
	}
}

static void _segments_xy_clip_attack_grad(
	double lC,
	size_t n,
	double prev_attack_x, // TODO: Arrays
	double prev_attack_y,
	double (*dst)[_FIT_XY_COUNT],
	double (*grad)[_FIT_XY_COUNT])
{
	for(size_t i = 1; i < n; ++i)
	{
		++dst;
		++grad;

		if(_needs_clip(prev_attack_x, dst))
		{
			double v1 = expm1(lC * (dst[0][_FIT_XY_HOLD_Y] - prev_attack_y));
			double attack_dx = box_warp_inv_sub(dst[0][_FIT_XY_HOLD_X], prev_attack_x);
			double end_hold_dx = box_warp_inv_sub(dst[0][_FIT_XY_HOLD_X], dst[-1][_FIT_XY_END_X]);
			double end_attack_dx = box_warp_inv_sub(dst[-1][_FIT_XY_END_X], prev_attack_x);

			double f0 = grad[0][_FIT_XY_ATTACK_Y] / (end_attack_dx * v1 + attack_dx);
			double f1 = f0 * v1 / lC;
			double f2 = -f1 / attack_dx;

			grad[-1][_FIT_XY_END_X] += f1 * box_warp_inv_diff(dst[-1][_FIT_XY_END_X]) + grad[0][_FIT_XY_ATTACK_X];

			grad[0][_FIT_XY_ATTACK_Y] = f0 * end_hold_dx;
			grad[0][_FIT_XY_ATTACK_X] = f2 * end_hold_dx * box_warp_inv_diff(prev_attack_x);
			grad[0][_FIT_XY_HOLD_X] += f2 * end_attack_dx * box_warp_inv_diff(dst[0][_FIT_XY_HOLD_X]);
			grad[0][_FIT_XY_HOLD_Y] += f0 * end_attack_dx * (v1 + 1);
		}
	}
}

static void _segments_xy(size_t n, const double (*params)[_FIT_COUNT_FULL], double (*dst)[_FIT_XY_COUNT])
{
	assert(n);

	for(size_t i = 0; i != n; ++i)
	{
		(*dst)[_FIT_XY_ATTACK_Y] = (*params)[_FIT_DELAY_Y];
		(*dst)[_FIT_XY_ATTACK_X] = (*params)[_FIT_DELAY_DX];
		(*dst)[_FIT_XY_HOLD_X] = (*params)[_FIT_DELAY_DX] + (*params)[_FIT_ATTACK_DX];
		(*dst)[_FIT_XY_HOLD_Y] = (*params)[_FIT_HOLD_Y];
		(*dst)[_FIT_XY_DECAY_X] = (*dst)[_FIT_XY_HOLD_X] + (*params)[_FIT_HOLD_DX];
		(*dst)[_FIT_XY_SUSTAIN_X] = (*dst)[_FIT_XY_DECAY_X] + (*params)[_FIT_DECAY_DX];
		(*dst)[_FIT_XY_SUSTAIN_Y] = (*params)[_FIT_SUSTAIN_Y];
		(*dst)[_FIT_XY_END_X] = (*dst)[_FIT_XY_SUSTAIN_X] + (*params)[_FIT_SUSTAIN_DX];

		++params;
		++dst;
	}
}

static void _segments_point(size_t n, const double (*params)[_FIT_XY_COUNT], struct point *dst)
{
	assert(n);

	size_t i = 0;

	double x0 = 0;

	for(;;)
	{
		dst[0].x = x0;
		dst[0].y = (*params)[_FIT_XY_ATTACK_Y];
		dst[1].x = (*params)[_FIT_XY_ATTACK_X];
		dst[1].y = (*params)[_FIT_XY_ATTACK_Y];
		dst[2].x = (*params)[_FIT_XY_HOLD_X];
		dst[2].y = (*params)[_FIT_XY_HOLD_Y];
		dst[3].x = (*params)[_FIT_XY_DECAY_X];
		dst[3].y = (*params)[_FIT_XY_HOLD_Y];
		dst[4].x = (*params)[_FIT_XY_SUSTAIN_X];
		dst[4].y = (*params)[_FIT_XY_SUSTAIN_Y];

		++i;
		if(i == n)
			break;

		dst[5].x = (*params)[_FIT_XY_END_X];
		dst[5].y = (*params)[_FIT_XY_SUSTAIN_Y];

		++params;
		dst += 6;
		x0 = dst[-1].x;
	}
}

void envelope_fit_segments(const struct envelope_fit *envelope, struct point *dst)
{
	double params[_FIT_COUNT_FULL];
	params[_FIT_DELAY_Y] = envelope->delay_y;
	params[_FIT_DELAY_DX] = envelope->delay_dx;
	params[_FIT_ATTACK_DX] = envelope->attack_dx;
	params[_FIT_HOLD_Y] = envelope->hold_y;
	params[_FIT_HOLD_DX] = envelope->hold_dx;
	params[_FIT_DECAY_DX] = envelope->decay_dx;
	params[_FIT_SUSTAIN_Y] = envelope->sustain_y;

	double params_xy[_FIT_XY_COUNT];
	_segments_xy(1, &params, &params_xy);
	_segments_point(1, &params_xy, dst);
}

static double _lines_f0(
	double lC,
	size_t envelope_count,
	const struct box_xmb *src_begin,
	const struct box_xmb *src_end,
	const double (*params_xy)[_FIT_XY_COUNT],
	double (*diff_xy)[_FIT_XY_COUNT])
{
	struct point dst_begin[6 * envelope_fit_max_envelopes];
	_segments_point(envelope_count, params_xy, dst_begin);

	struct point *dst_end = dst_begin + 6 * envelope_count - 2;

	// Hacky, but it should work. This isn't used unless src is longer than dst. If it is, dst_begin[5].x can be anything but
	// dst_begin[4].x, because pos->has_x1_1 is false for that segment. (pos->has_x1_0 and pos->has_x1_1 must be true for
	// zero-length dst segments.) If self->src.end->x == dst_begin[4].x, then dst_begin[5] will never be used.
	// TODO: Sus! Retest.
	dst_end[1].x = src_end->x;
	dst_end[1].y = dst_end[0].y;

/*
	for(size_t i = 0; i != 5; ++i)
		printf("%g\t", dst[i][0]);
	putchar('\n');
	for(size_t i = 0; i != 5; ++i)
		printf("%g\t", dst[i][1]);
	putchar('\n');
*/

	struct box_line_range pos;
	line_range_new(&pos, src_begin, dst_begin);

	struct point dst_diff[envelope_fit_max_envelopes * 6] = {};

/*
	for(size_t i = 0; i != _FIT_COUNT; ++i)
		printf("%g\t", self->params[i]);
	putchar(grad ? '*' : ' ');
	fflush(stdout);
*/

	double result = 0;
	while(line_range_more(&pos, src_end, dst_end))
	{
		//printf("%21.16g|%ld\t%ld\t%d\t%d\t%.16g\t%.16g\n", result, pos.seg_src - self->src.begin, pos.seg_dst - dst_begin, pos.has_x1_0, pos.has_x1_1, pos.x0, pos.x1);
		//fflush(stdout);

		//if(pos.x1 < pos.x0)
		//	return HUGE_VAL;
		assert(pos.x0 <= pos.x1);

		double g[4] = {};

		if(
			pos.seg_src[0].mb.b == -INFINITY && /* Check y first because this's almost always false. */
			pos.seg_dst[0].y == -INFINITY &&
			pos.seg_src[1].mb.m != INFINITY &&
			pos.seg_dst[1].y == -INFINITY &&
			pos.seg_src[1].x == pos.seg_dst[1].x &&
			pos.seg_src[0].x == pos.seg_dst[0].x)
		{
			// result += 0;
			if(diff_xy)
			{
				g[0] = INFINITY;
				g[1] = INFINITY;
				g[2] = INFINITY;
				g[3] = INFINITY;
			}
		}
		else
		{
			result += box_sqr((pos.seg_dst - dst_begin) % 6 == 1 ? box_c : box_line, lC, &pos, diff_xy ? g : NULL);

			//if(d < 0)
			//	printf("d = %g, dpos = %g\n", d, pos.x1 - pos.x0);
		}

		if(diff_xy)
		{
			size_t i = pos.seg_dst - dst_begin;

			dst_diff[i].x += g[0];
			dst_diff[i].y += g[1];
			dst_diff[i + 1].x += g[2];
			dst_diff[i + 1].y += g[3];
		}

		line_range_next(&pos);
	}

	//printf("%g", result);

	if(diff_xy)
	{
		// The last d[5].x is always 0. If dst is shorter than src, then because has_x1_1 is false, changing d[5].x
		// doesn't affect the result sum, because that final placeholder dst line has a slope of 0, so it follows that the
		// derivative of d[5].x is 0. The math cancels itself out accordingly, though not in a way where it would be
		// practical to make the real-world floating point values turn out to be exactly 0.

		const struct point *d = dst_diff;

		for(size_t e = 0; e != envelope_count; ++e)
		{
			if(e)
				diff_xy[e - 1][_FIT_XY_END_X] += d[0].x;

			double *diff0_xy = diff_xy[e];

			diff0_xy[_FIT_XY_ATTACK_Y] = d[0].y + d[1].y;
			diff0_xy[_FIT_XY_ATTACK_X] = d[1].x;
			diff0_xy[_FIT_XY_HOLD_X] = d[2].x;
			diff0_xy[_FIT_XY_HOLD_Y] = d[2].y + d[3].y;
			diff0_xy[_FIT_XY_DECAY_X] = d[3].x;
			diff0_xy[_FIT_XY_SUSTAIN_X] = d[4].x;
			diff0_xy[_FIT_XY_SUSTAIN_Y] = d[4].y + d[5].y;
			diff0_xy[_FIT_XY_END_X] = d[5].x;

			d += 6;
		}
	}

	return result;
}

void _nudge_x(double x0, double *x1)
{
	if(x0 > *x1)
	{
		assert(*x1 - x0 <= 1.0 / (1ll << 58));
		*x1 = x0;
	}
}

static double _lines_f(unsigned n, const double *p, double *grad, void *self_raw)
{
	struct _flex_fit *self = self_raw;

	{
		size_t n0 = 0;
		for(size_t j = 0; j != self->envelope_count; ++j)
		{
			for(size_t i = 0; i != _FIT_COUNT_FULL; ++i)
			{
				if(self->enums[j] & (1 << i))
					++n0;
			}
		}
		assert(n == n0);
	}

	_write_x(self, p);

	double params_xy[envelope_fit_max_envelopes][_FIT_XY_COUNT];
	_segments_xy(self->envelope_count, self->params, params_xy);
	double prev_attack_x = params_xy[1][_FIT_XY_ATTACK_X], prev_attack_y = params_xy[1][_FIT_XY_ATTACK_Y];
	_segments_xy_clip_attack(self->lC, self->envelope_count, params_xy);

	{
		double x0 = 0;
		for(size_t e = 0; e != self->envelope_count; ++e)
		{
			double *params = params_xy[e];
			_nudge_x(x0, &params[_FIT_XY_ATTACK_X]);
			_nudge_x(params[_FIT_XY_ATTACK_X], &params[_FIT_XY_HOLD_X]);
			_nudge_x(params[_FIT_XY_HOLD_X], &params[_FIT_XY_DECAY_X]);
			_nudge_x(params[_FIT_XY_DECAY_X], &params[_FIT_XY_SUSTAIN_X]);
			//if(e != self->envelope_count - 1)
			{
				_nudge_x(params[_FIT_XY_SUSTAIN_X], &params[_FIT_XY_END_X]);
				x0 = params[_FIT_XY_END_X];
			}
		}
	}

	double diff_xy[envelope_fit_max_envelopes][_FIT_XY_COUNT];

	double result = _lines_f0(
		self->lC,
		self->envelope_count,
		self->src_begin,
		self->src_end,
		params_xy,
		grad ? diff_xy : NULL);

	if(grad)
	{
		_segments_xy_clip_attack_grad(self->lC, self->envelope_count, prev_attack_x, prev_attack_y, params_xy, diff_xy);

		double diff[envelope_fit_max_envelopes][_FIT_COUNT_FULL] = {};

#if __GNUC__ || __clang__
		if(!self->envelope_count)
			__builtin_unreachable();
#endif

		for(size_t e = 0; e != self->envelope_count; ++e)
		{
			const double *diff0_xy = diff_xy[e];
			double *diff0 = diff[e];

			diff0[_FIT_SUSTAIN_DX] = diff0_xy[_FIT_XY_END_X];
			diff0[_FIT_DECAY_DX] = diff0_xy[_FIT_XY_SUSTAIN_X] + diff0[_FIT_SUSTAIN_DX];
			diff0[_FIT_HOLD_DX] = diff0_xy[_FIT_XY_DECAY_X] + diff0[_FIT_DECAY_DX];
			diff0[_FIT_ATTACK_DX] = diff0_xy[_FIT_XY_HOLD_X] + diff0[_FIT_HOLD_DX];
			diff0[_FIT_DELAY_DX] = diff0_xy[_FIT_XY_ATTACK_X] + diff0[_FIT_ATTACK_DX];

			diff0[_FIT_DELAY_Y] = diff0_xy[_FIT_XY_ATTACK_Y];
			diff0[_FIT_HOLD_Y] = diff0_xy[_FIT_XY_HOLD_Y];
			diff0[_FIT_SUSTAIN_Y] = diff0_xy[_FIT_XY_SUSTAIN_Y];
		}

		for(size_t e = self->envelope_count - 1; e; --e) // TODO: Direction OK for envelope_count > 2?
		{
			if(self->enums[e] & (1 << _FIT_ATTACK_DX_X0))
			{
				double *diff0 = diff[e - 1];
				double attack_dx0 = diff[e][_FIT_ATTACK_DX];
				diff0[_FIT_DELAY_DX] += attack_dx0;
				diff0[_FIT_ATTACK_DX] += attack_dx0;
				diff0[_FIT_HOLD_DX] += attack_dx0;
				diff0[_FIT_DECAY_DX] += attack_dx0;
				diff0[_FIT_SUSTAIN_DX] += attack_dx0;
				diff[e][_FIT_DELAY_DX] -= diff[e][_FIT_ATTACK_DX];
				diff[e][_FIT_ATTACK_DX_X0] = diff[e][_FIT_ATTACK_DX];

				if(self->enums[e] & (1 << _FIT_DELAY_X_T))
				{
					double d = diff[e][_FIT_DELAY_DX] * self->params[e][_FIT_DELAY_X_T];
					diff0[_FIT_DELAY_DX] += d;
					diff0[_FIT_ATTACK_DX] += d;
					diff0[_FIT_HOLD_DX] += d;
					diff0[_FIT_DECAY_DX] += d;
					diff0[_FIT_SUSTAIN_DX] += d;
					diff[e][_FIT_ATTACK_DX_X0] += d;

					diff[e][_FIT_DELAY_X_T] = (_params_dx(self->params[e - 1]) + self->params[e][_FIT_ATTACK_DX_X0]) * diff[e][_FIT_DELAY_DX];
				}
			}
		}

		{
			for(size_t e = 0; e != self->envelope_count; ++e)
			{
				double *diff0 = diff[e];

				// Fun fact: These next two can go anywhere.
				if(self->enums[e] & (1 << _FIT_HOLD_DY_SUSTAIN))
				{
					diff0[_FIT_SUSTAIN_Y] += diff0[_FIT_HOLD_Y];
					diff0[_FIT_HOLD_DY_SUSTAIN] = diff0[_FIT_HOLD_Y];
				}

				if(self->enums[e] & (1 << _FIT_SUSTAIN_DY_DELAY))
				{
					diff0[_FIT_DELAY_Y] += diff0[_FIT_SUSTAIN_Y];
					diff0[_FIT_SUSTAIN_DY_DELAY] = diff0[_FIT_SUSTAIN_Y];
				}
			}
		}

		{
			double *g0 = grad;
			for(size_t e = 0; e != self->envelope_count; ++e)
				g0 = _read_x0(self->enums[e], diff[e], g0, _FIT_COUNT_FULL);
		}

		//for(size_t i = 0; i != n; ++i)
		//	printf("\t%g", grad[i]);

		for(size_t i = 0; i != n; ++i)
		{
			assert(isfinite(grad[i]));
		}
	}

	//putchar('\n');
	//fflush(stdout);

	result *= _error_scale;
	if(grad)
	{
		for(size_t i = 0; i != n; ++i)
			grad[i] *= _error_scale;
	}

	assert(isfinite(result));

	return result;
}

void nlopt_check(nlopt_opt opt, nlopt_result result)
{
	if(result < 0)
	{
		fprintf(stderr, "%d %s\n", result, nlopt_get_errmsg(opt));
		abort();
	}
}

double _fit(struct _flex_fit *self, const double *step_params, double t0, const double *min0, const double *max0)
{
	double x0[_FIT_COUNT_FULL];
	size_t enums_n = _read_x0(self->enums[0], self->params[0], x0, _FIT_COUNT_BASE) - x0;

	double grad[11];
	assert(isfinite(_lines_f(enums_n, x0, grad, self)));
	for(size_t i = 0; i != enums_n; ++i)
	{
		assert(isfinite(grad[i]));
	}

	nlopt_opt opt = nlopt_create(NLOPT_LD_SLSQP, enums_n);
	if(!opt)
		abort();

	{
		double step[_FIT_COUNT_BASE];
		_read_x0(self->enums[0], step_params, step, _FIT_COUNT_BASE);
		nlopt_result nlopt_result = nlopt_set_initial_step(opt, step);
		if(!(self->src_begin == self->src_end && nlopt_result == NLOPT_INVALID_ARGS))
			nlopt_check(opt, nlopt_result);
	}

	// nlopt_check(opt, nlopt_set_ftol_rel(opt, t0)); // TODO: Still necessary?
	// nlopt_check(opt, nlopt_set_ftol_abs(opt, 1e-5));
	nlopt_check(opt, nlopt_set_xtol_rel(opt, t0));

	nlopt_check(opt, nlopt_set_maxeval(opt, 32768));

	{
		double min[_FIT_COUNT_BASE];
		_read_x0(self->enums[0], min0, min, _FIT_COUNT_BASE);
		nlopt_check(opt, nlopt_set_lower_bounds(opt, min));
	}

	{
		double max[_FIT_COUNT_BASE];
		_read_x0(self->enums[0], max0, max, _FIT_COUNT_BASE);
		nlopt_check(opt, nlopt_set_upper_bounds(opt, max));
	}

	nlopt_check(opt, nlopt_set_min_objective(opt, _lines_f, self));

	double result;
	nlopt_result nlopt_result = nlopt_optimize(opt, x0, &result);
	if(nlopt_result == NLOPT_FAILURE) // Edge case when SLSQP can go nowhere useful.
		return INFINITY;
	if(nlopt_result != NLOPT_ROUNDOFF_LIMITED)
		nlopt_check(opt, nlopt_result);

	nlopt_destroy(opt);

	result /= _error_scale;

	assert(isfinite(_lines_f(enums_n, x0, NULL, self)));

	_write_x(self, x0);
	assert(isfinite(result));
	return result;
}

void _normalize(double sustain_y, double *params, size_t enums_mask)
{
	const double limit_y = 1.0 / 32.0; // TODO: Need a better choice.
	if(fabs(params[_FIT_HOLD_Y] - params[_FIT_DELAY_Y]) < limit_y)
	{
		if(params[_FIT_HOLD_Y] >= sustain_y)
		{
			params[_FIT_DELAY_DX] = 0;
			params[_FIT_ATTACK_DX] = 0;
		}
	}

	if(fabs(sustain_y - params[_FIT_HOLD_Y]) < limit_y)
	{
		if(params[_FIT_HOLD_Y] >= params[_FIT_DELAY_Y])
		{
			params[_FIT_HOLD_DX] = 0;
			params[_FIT_DECAY_DX] = 0;
		}
	}

	const double limit = 1e-5; // TODO: Need a better choice.

	if(enums_mask & (1 << _FIT_DELAY_DX) && params[_FIT_DELAY_DX] < limit)
	{
		params[_FIT_DELAY_DX] = 0;

		if(enums_mask & (1 << _FIT_ATTACK_DX) && params[_FIT_ATTACK_DX] < limit)
		{
			params[_FIT_ATTACK_DX] = 0;
			if(enums_mask & (1 << _FIT_DELAY_Y))
				params[_FIT_DELAY_Y] = sustain_y;
		}
	}
}

static bool _right_side_up(const double *params)
{
	return params[_FIT_DELAY_Y] <= params[_FIT_SUSTAIN_Y] && params[_FIT_SUSTAIN_Y] <= params[_FIT_HOLD_Y];
	//return params[_FIT_SUSTAIN_Y] < params[_FIT_HOLD_Y] || params[_FIT_SUSTAIN_Y] == params[_FIT_HOLD_Y] && params[_FIT_DELAY_Y] <= params[_FIT_SUSTAIN_Y]
}

static double _refine(
	const struct point *src,
	size_t src_n,
	double (*params)[_FIT_COUNT_FULL],
	size_t enums_mask,
	double y_max,
	double lC,
	bool enforce_gain)
{
	// Do this so that the optimizer can't find an all-zero envelope to be better than the first guess.
	enums_flags first_try = ((1 << _FIT_HOLD_DX) | (1 << _FIT_DECAY_DX));

	struct _flex_fit self =
	{
		{},
		NULL,
		1,
		{enums_mask & first_try},
		params,
		y_max,
		lC,
		enforce_gain
	};

	self.src_end = _mb(src, self.src_begin, src_n - 1);
	double sustain_y = self.src_end->mb.b;

	// TODO: Bubble up y_max.
	const double x_step = src[src_n - 1].x / 4;
	const double y_step = y_max / 4;

	// Maybe repeat the following.

	_normalize(sustain_y, *params, enums_mask);

	for(size_t i = 0; i != _FIT_COUNT_BASE; ++i)
		assert((i == _FIT_DELAY_Y && params[0][i] == -INFINITY) || isfinite(params[0][i]));

	double step_params[_FIT_COUNT_BASE];

	step_params[_FIT_DELAY_Y] = y_step;
	step_params[_FIT_DELAY_DX] = x_step;
	step_params[_FIT_ATTACK_DX] = x_step;
	step_params[_FIT_HOLD_DX] = x_step;
	step_params[_FIT_DECAY_DX] = x_step;
	step_params[_FIT_HOLD_Y] = y_step;

	double min0[_FIT_COUNT_BASE];
	double max0[_FIT_COUNT_BASE];
	for(size_t i = 0; i != _FIT_COUNT_BASE; ++i)
	{
		min0[i] = 0;
		max0[i] = HUGE_VAL;
	}

	// Don't let the delay and attack both reduce to 0, in the sleaziest way possible.
/*
	if(enums_mask & (1 << _FIT_DELAY_DX))
		min0[_FIT_ATTACK_DX] = DBL_EPSILON;
	else
		min0[_FIT_DECAY_DX] = DBL_EPSILON;
*/

	if((enums_mask & ((1 << _FIT_ATTACK_DX) | (1 << _FIT_DECAY_DX))) == ((1 << _FIT_ATTACK_DX) | (1 << _FIT_DECAY_DX)))
		min0[(*params)[_FIT_ATTACK_DX] > 0 ? _FIT_ATTACK_DX : _FIT_DECAY_DX] = DBL_EPSILON;
	else if(enums_mask & (1 << _FIT_ATTACK_DX))
		min0[_FIT_ATTACK_DX] = DBL_EPSILON;
	else
		min0[_FIT_DECAY_DX] = DBL_EPSILON;

	if(enforce_gain)
	{
		if(_right_side_up(params[0]))
		{
			min0[_FIT_DELAY_Y] = -HUGE_VAL;
			max0[_FIT_DELAY_Y] = sustain_y;
			min0[_FIT_HOLD_Y] = sustain_y;
			max0[_FIT_HOLD_Y] = y_max;
		}
		else
		{
			min0[_FIT_DELAY_Y] = sustain_y;
			max0[_FIT_DELAY_Y] = y_max;
			min0[_FIT_HOLD_Y] = 0;
			max0[_FIT_HOLD_Y] = sustain_y;
		}
	}
	else
	{
		min0[_FIT_DELAY_Y] = 0;
		max0[_FIT_DELAY_Y] = y_max;
		min0[_FIT_HOLD_Y] = 0;
		max0[_FIT_HOLD_Y] = y_max;
	}

	double error = 0;

	assert(enums_mask); // Otherwise, error isn't calculated.

	if(self.enums[0])
		error = _fit(&self, step_params, 1.0 / (1 << 20), min0, max0);

	if(enums_mask & ~first_try)
	{
		self.enums[0] = enums_mask;
		error = _fit(&self, step_params, 1.0 / (1 << 20), min0, max0); // $0AF,vol needs 1 / (1 << 20).
	}

	_normalize(sustain_y, params[0], enums_mask);

	return error;
}

static void _peak(
	const struct point *s,
	size_t n,
	bool no_delay,
	double y_max,
	double lC,
	double *params_best,
	double *error_best,
	size_t first_max,
	size_t last_max)
{
	// (first|last)_max is the location for the hold.

	assert(first_max < n);
	assert(last_max < n);

	double params[_FIT_COUNT_FULL];

	size_t enums_mask = (1 << _FIT_HOLD_DX) | (1 << _FIT_DECAY_DX);
	double peak = s[first_max].y;
	double not_peak = s[first_max > 0 ? first_max - 1 : (last_max < n - 1 ? last_max + 1 : first_max)].y;

	double sustain_y = s[n - 1].y;

	if(s[0].y == sustain_y && peak == sustain_y)
	{
		if(peak >= not_peak && peak == y_max)
			return;
		if(peak < not_peak && peak == 0)
			return;
	}

	if(first_max == 0)
	{
		// Sample beginnings are more important than endings.

		// The envelope is fit weighted by an approximation (via erf(x*√π/2) ≃ tanh(x)) of a (reversed) log-normal cumulative
		// distribution for MIDI note lengths.

		params[_FIT_DELAY_Y] = peak > not_peak ? 0 : sustain_y;
		params[_FIT_DELAY_DX] = 0;
		params[_FIT_ATTACK_DX] = 0;
		params[_FIT_HOLD_Y] = s[0].y;
	}
	else
	{
		params[_FIT_DELAY_Y] = s[0].y;

		if(s[0].y == -INFINITY)
		{
			if(n >= 2 && s[1].y == -INFINITY)
			{
				if(!first_max) // TODO, maybe?
					return;
				params[_FIT_DELAY_DX] = box_warp(s[1].x);
			}
			else
			{
				params[_FIT_DELAY_DX] = 0;
			}
		}
		else if(no_delay)
		{
			params[_FIT_DELAY_DX] = 0;
		}
		else
		{
			params[_FIT_DELAY_DX] = box_warp(s[first_max].x * 0.5);
			enums_mask |= 1 << _FIT_DELAY_DX;
		}

		enums_mask |= (1 << _FIT_ATTACK_DX) | (1 << _FIT_HOLD_Y);

		double peak0 = s[last_max].y;
		double y0 = s[0].y, y1 = s[n - 1].y;
		if(y0 < y1)
		{
			if(peak0 < sustain_y)
				peak0 = sustain_y;
		}
		else if(y0 > y1)
		{
			if(peak0 > sustain_y)
				peak0 = sustain_y;
		}
		// else peak0 can be anywhere! What fun!

		params[_FIT_ATTACK_DX] = box_warp(s[first_max].x) - params[_FIT_DELAY_DX];
		assert(params[_FIT_ATTACK_DX] >= 0);

		params[_FIT_HOLD_Y] = peak0;
	}

	double last_x = box_warp(s[last_max].x);
	params[_FIT_HOLD_DX] = last_x - params[_FIT_ATTACK_DX] - params[_FIT_DELAY_DX];
	params[_FIT_DECAY_DX] = box_warp(s[n - 1].x) - last_x;
	params[_FIT_SUSTAIN_Y] = sustain_y;
	params[_FIT_SUSTAIN_DX] = 0;

	double error = _refine(
		s,
		n,
		&params,
		enums_mask,
		y_max,
		lC,
		true);

#if 0
	double sustain = s[n - 1].y;
	if(
		(params[_FIT_ATTACK_Y] < params[_FIT_HOLD_Y] && params[_FIT_HOLD_Y] < sustain) ||
		(params[_FIT_ATTACK_Y] > params[_FIT_HOLD_Y] && params[_FIT_HOLD_Y] > sustain) ||
		(sustain < params[_FIT_ATTACK_Y] && params[_FIT_ATTACK_Y] < params[_FIT_HOLD_Y]) ||
		(sustain > params[_FIT_ATTACK_Y] && params[_FIT_ATTACK_Y] > params[_FIT_HOLD_Y]))
	{
		double ig = fabs(params[_FIT_ATTACK_Y] - params[_FIT_HOLD_Y]);
		double gs = fabs(params[_FIT_HOLD_Y] - sustain);
		double is = fabs(params[_FIT_ATTACK_Y] - sustain);
		if(ig < gs && ig < is)
		{
			if(!lock_initial)
				params[_FIT_ATTACK_Y] = sustain;
			else
				params[_FIT_HOLD_Y] = sustain;
		}
		else if(gs < is)
		{
			if(!lock_initial)
				params[_FIT_ATTACK_Y] = sustain;
			else
				params[_FIT_HOLD_Y] = sustain;
		}
		else
		{
			params[_FIT_ATTACK_Y] = sustain;
		}

		error = _refine(
			s,
			n,
#if DEBUG_ENVELOPES
			initial,
#endif
			params,
			enums_mask,
			y_max,
			lC,
			lock_initial,
			true);
	}
#endif

	if(error < *error_best)
	{
		memcpy(params_best, params, sizeof(*params) * _FIT_COUNT_BASE);
		*error_best = error;
	}
}

double _fit1(
	size_t n,
	const struct point *s,
	double lC,
	bool lock_orientation,
	bool no_delay,
	double y_max,
	double *params)
{
	// NLopt (sometimes?) screws up bad on exact matches.

	if(n <= 1)
	{
		params[_FIT_DELAY_Y] = 0;
		params[_FIT_DELAY_DX] = 0;
		params[_FIT_ATTACK_DX] = 0;
		params[_FIT_HOLD_Y] = s[0].y;
		params[_FIT_HOLD_DX] = 0;
		params[_FIT_DECAY_DX] = 0;
		params[_FIT_SUSTAIN_Y] = s[0].y;
		params[_FIT_SUSTAIN_DX] = 0;
		return 0;
	}

	if(n == 2 && (!lock_orientation || s[0].y >= s[1].y))
	{
		params[_FIT_DELAY_Y] = s[0].y >= s[1].y ? 0 : s[1].y;
		params[_FIT_DELAY_DX] = 0;
		params[_FIT_ATTACK_DX] = 0;
		params[_FIT_HOLD_Y] = s[0].y;
		params[_FIT_HOLD_DX] = 0;
		params[_FIT_DECAY_DX] = box_warp(s[1].x);
		params[_FIT_SUSTAIN_Y] = s[1].y;
		params[_FIT_SUSTAIN_DX] = 0;
		return 0;
	}

	double error = INFINITY;

	size_t first_max = 0;
	while(first_max != n)
	{
		if(
			first_max == 0 ||
			first_max == n - 1 ||
			(
				(s[first_max - 1].y < s[first_max].y && s[first_max].y >= s[first_max + 1].y) ||
				(s[first_max - 1].y > s[first_max].y && s[first_max].y <= s[first_max + 1].y)
			))
		{
			size_t last_max = first_max + 1;
			while(last_max != n && s[first_max].y == s[last_max].y)
			{
				assert(last_max < n);
				++last_max;
			}
			--last_max;

			if(
				(
					(first_max == 0 || s[first_max - 1].y < s[first_max].y) &&
					(last_max == n - 1 || s[last_max].y > s[last_max + 1].y)
				) ||
				(
					!lock_orientation &&
					(first_max == 0 || s[first_max - 1].y > s[first_max].y) &&
					(last_max == n - 1 || s[last_max].y < s[last_max + 1].y)
				))
			{
				// No lock_orientation in _peak(), so fix bad s[0].y > s[n - 1].y no later than here.
				_peak(
					s,
					n,
					no_delay,
					y_max,
					lC,
					params,
					&error,
					lock_orientation && s[0].y > s[n - 1].y ? 0 : first_max,
					last_max);
			}

			first_max = last_max;
		}

		++first_max;
	}

	// https://modularwalls.com.au/wp-content/uploads/Human-perception-of-sound_April2016-WEB.pdf
	// ± 1dB Not perceptible
	// ± 3dB Threshold of perception
	// ± 5dB Clearly noticeable

	// https://en.wikipedia.org/wiki/Just-noticeable_difference
	// "For amplitude, the JND for humans is around 1 dB (Middlebrooks & Green, 1991; Mills, 1960)."
	// https://doi.org/10.1146%2Fannurev.ps.42.020191.001031

	assert(isfinite(error));
	return error;
}

struct _split_fit
{
	struct box_xmb src_begin[envelope_fit_max_src_n];
	const struct box_xmb *src_end;

	double lC;
};

enum _fit_segment _split_find(const double *params, double x, double *x0)
{
	static const unsigned char dx[] = {_FIT_DELAY_DX, _FIT_ATTACK_DX, _FIT_HOLD_DX, _FIT_DECAY_DX};
	*x0 = 0;
	for(unsigned i = 0; i != arraysize(dx); ++i)
	{
		unsigned e = dx[i];
		double x1 = *x0 + params[e];
		if(x < x1)
			return e;
		*x0 = x1;
	}

	return _FIT_SUSTAIN_DX;
}

void _split_zero(unsigned begin, unsigned end, double *params)
{
	switch(begin)
	{
	case _FIT_DELAY_DX:
		if(end == _FIT_DELAY_DX)
			break;
		params[_FIT_DELAY_DX] = 0;
		// Fallthrough.
	case _FIT_ATTACK_DX:
		if(end == _FIT_ATTACK_DX)
			break;
		params[_FIT_ATTACK_DX] = 0;
		// Fallthrough.
	case _FIT_HOLD_DX:
		if(end == _FIT_HOLD_DX)
			break;
		params[_FIT_HOLD_DX] = 0;
		// Fallthrough.
	case _FIT_DECAY_DX:
		if(end == _FIT_DECAY_DX)
			break;
		params[_FIT_DECAY_DX] = 0;
		// Fallthrough.
	}
}

static double _split_decay_y(const double *params, double decay_x0, double x)
{
	double decay_x = params[_FIT_DELAY_DX] + params[_FIT_ATTACK_DX] + params[_FIT_HOLD_DX];
	return _param_y(
		box_warp_inv_sub(x, decay_x0),
		box_warp_inv_sub(decay_x + params[_FIT_DECAY_DX], decay_x),
		params[_FIT_HOLD_Y],
		params[_FIT_SUSTAIN_Y]);
}

static void _split_params(double split, double lC, double params[2][_FIT_COUNT_FULL], double y_max)
{
	memcpy(&params[1], &params[0], sizeof(params[0]));

	double x0;
	enum _fit_segment seg = _split_find(params[0], split, &x0);

	//double x1 = x0 + params[0][seg];
	double dx = split - x0;

	switch(seg)
	{
	case _FIT_DELAY_DX:
		break;

	case _FIT_ATTACK_DX:
		params[0][_FIT_HOLD_Y] = _split_attack_y0(
			lC,
			box_warp_inv_sub(split, params[0][_FIT_DELAY_DX]),
			box_warp_inv_sub(params[0][_FIT_DELAY_DX] + params[0][_FIT_ATTACK_DX], params[0][_FIT_DELAY_DX]),
			params[0][_FIT_DELAY_Y],
			params[0][_FIT_HOLD_Y]);
		params[0][_FIT_SUSTAIN_Y] = params[0][_FIT_HOLD_Y];
		break;

	case _FIT_HOLD_DX:
		params[0][_FIT_SUSTAIN_Y] = params[0][_FIT_HOLD_Y];

		params[1][_FIT_ATTACK_DX] += dx;
		params[1][_FIT_HOLD_DX] -= dx;

		break;

	case _FIT_DECAY_DX:
		{
			double y = _split_decay_y(params[0], x0, split);
			params[0][_FIT_SUSTAIN_Y] = y;
			//if(params[0][_FIT_ATTACK_DX] == 0 && params[0][_FIT_ATTACK_DX] == 0)
			//	params[0][_FIT_SUSTAIN_Y] = y;

			params[1][_FIT_DELAY_Y] = params[1][_FIT_SUSTAIN_Y];
			params[1][_FIT_HOLD_Y] = y;
			params[1][_FIT_ATTACK_DX] += params[1][_FIT_HOLD_DX] + dx;
			params[1][_FIT_HOLD_DX] = 0;
			params[1][_FIT_DECAY_DX] -= dx;
		}
		break;

	case _FIT_SUSTAIN_DX:
		assert(seg == _FIT_SUSTAIN_DX);

		//params[1][_FIT_DELAY_Y] = params[1][_FIT_SUSTAIN_Y];
		//params[1][_FIT_HOLD_Y] = params[1][_FIT_SUSTAIN_Y];
		params[1][_FIT_ATTACK_DX] += params[1][_FIT_HOLD_DX] + params[1][_FIT_DECAY_DX] + dx;
		params[1][_FIT_HOLD_DX] = 0;
		params[1][_FIT_DECAY_DX] = 0;
		break;

	default:
		break;
	}

	_split_zero(seg, -1, params[0]);
	params[0][seg] = dx;

	params[1][_FIT_SUSTAIN_DX] = 0;
}

static double _split_f(unsigned n, const double *p, double *grad, void *self_raw)
{
	const struct _split_fit *self = (const struct _split_fit *)self_raw;

	struct point dst_begin[3];
	dst_begin[0].x = self->src_begin->x;
	dst_begin[0].y = self->src_begin->mb.b;
	dst_begin[1].x = *p;

	const struct box_xmb *p_src = self->src_begin;
	while(p_src != self->src_end && p_src[1].x < *p)
		++p_src;

	const struct box_mb *mb_src_p = &p_src->mb;
	dst_begin[1].y = mb_src_p->m * box_warp_inv(*p) + mb_src_p->b;

	dst_begin[2].x = self->src_end->x;
	dst_begin[2].y = self->src_end->mb.b;

	const struct point *dst_end = arrayend(dst_begin) - 1;

	struct box_line_range pos;
	line_range_new(&pos, self->src_begin, dst_begin);

	double result = 0;
	double dst_diff[2] = {};

	while(line_range_more(&pos, self->src_end, dst_end))
	{
		double g[4];
		result += box_sqr(box_line, self->lC, &pos, grad ? g : NULL);

		if(grad)
		{
			double *g0 = (pos.seg_dst - dst_begin) ? g : (g + 2);
			dst_diff[0] += g0[0];
			dst_diff[1] += g0[1];
		}

		line_range_next(&pos);
	}

	if(grad)
		*grad = dst_diff[0] + dst_diff[1] * mb_src_p->m * box_warp_inv_diff(*p);

/*
	printf("%.17g\t%.17g", *p, result);
	if(grad)
		printf("\t%.17g", *grad);
	putchar('\n');
*/

	return result;
}

double _split_map(double split, double t) // Warped -> not warped
{
	//double s0 = 0, s1 = split * 2;
	//return _b2(t, s0, s1);
	return t * box_warp_inv(split) * 2;
}

void _split_adjust_hold(double vol_lC, enum _fit_segment seg, double x, double *params)
{
	// x is not warped.
	if(seg == _FIT_DELAY_DX)
	{
		params[_FIT_HOLD_Y] = 0;
	}
	else if(seg == _FIT_ATTACK_DX)
	{
		params[_FIT_HOLD_Y] = _split_attack_y0(
			vol_lC,
			x - box_warp_inv(params[_FIT_DELAY_DX]),
			box_warp_inv_sub(params[_FIT_DELAY_DX] + params[_FIT_ATTACK_DX], params[_FIT_DELAY_DX]),
			params[_FIT_DELAY_Y],
			params[_FIT_HOLD_Y]);
	}
}

size_t envelope_fit(
	size_t mod_n,
	const struct point *mod_s,
	double mod_lC,
	size_t vol_n,
	const struct point *vol_s,
	double vol_lC,
	bool vol_no_delay,
	double y_max,
#if DEBUG_ENVELOPES
	double *mod_split_x,
	double *mod_error,
	double *vol_error,
#endif
	struct envelope_fit (*envelopes)[2])
{
	double params_mod[envelope_fit_max_envelopes][_FIT_COUNT_FULL];

	double mod_error0 = _fit1(
		mod_n,
		mod_s,
		mod_lC,
		false,
		false,
		y_max,
		params_mod[0]);

	if(mod_n > 1 && mod_error0 >= 4.3) // i.e. $08A
	{
		double split = mod_s[mod_n - 1].x;

		{
			nlopt_opt opt = nlopt_create(NLOPT_LD_SLSQP, 1);
			if(!opt)
				abort();

			// TODO xtol_abs?
			nlopt_check(opt, nlopt_set_xtol_rel(opt, 1e-3));

			nlopt_check(opt, nlopt_set_lower_bounds1(opt, box_warp(mod_s[1].x)));
			nlopt_check(opt, nlopt_set_upper_bounds1(opt, box_warp(mod_s[mod_n - 2].x)));

			struct _split_fit split_fit = {{}, NULL, mod_lC};
			split_fit.src_end = _mb(mod_s, split_fit.src_begin, mod_n - 1);

			nlopt_check(opt, nlopt_set_min_objective(opt, _split_f, &split_fit));

			split = box_warp((mod_s[1].x + mod_s[mod_n - 2].x) * 0.5);
			double result;

	/*
			{
				const double d = 1e-8;
				double grad;
				double i0 = _split_f(1, &x_max, &grad, &split_fit);
				printf("%.17g\t", i0);
				printf("%.17g\n", grad);

				x_max += d;
				double i1 = _split_f(1, &x_max, &grad, &split_fit);
				printf("%.17g\t", i1);
				printf("%.17g\n", (i1 - i0) / d);
			}
	*/

			int nlopt_result = nlopt_optimize(opt, &split, &result);
			if(nlopt_result != NLOPT_FAILURE && nlopt_result != NLOPT_ROUNDOFF_LIMITED)
				nlopt_check(opt, nlopt_result);

#if DEBUG_ENVELOPES
			*mod_split_x = box_warp_inv(split);
#endif

			assert(isfinite(split) && split >= 0);
			nlopt_destroy(opt);
		}

		_split_params(split, mod_lC, params_mod, y_max);
		assert(isfinite(params_mod[1][_FIT_ATTACK_DX]));

		{
			struct _flex_fit self =
			{
				{},
				NULL,
				2,
				{
					(1 << _FIT_HOLD_DX) |
						(1 << _FIT_DECAY_DX) |
						(1 << _FIT_SUSTAIN_DX) |
						(1 << _FIT_SUSTAIN_DY_DELAY),
					(1 << _FIT_DELAY_Y) |
						(1 << _FIT_DELAY_X_T) |
						(1 << _FIT_ATTACK_DX_X0) |
						(1 << _FIT_HOLD_Y) |
						(1 << _FIT_HOLD_DX) |
						(1 << _FIT_DECAY_DX),
				},

				params_mod,

				y_max,
				mod_lC,

				true,
			};

			self.src_end = _mb(mod_s, self.src_begin, mod_n - 1);

			size_t mandatory_enum;
			if(params_mod[0][_FIT_DELAY_DX] != 0 || params_mod[0][_FIT_ATTACK_DX] != 0)
			{
				self.enums[0] |= (1 << _FIT_DELAY_DX) | (1 << _FIT_ATTACK_DX) | (1 << _FIT_HOLD_DY_SUSTAIN);
				mandatory_enum = _FIT_ATTACK_DX;
			}
			else
			{
				mandatory_enum = _FIT_DECAY_DX;
			}

			double p[14];
			double *p0 = p;

			{
				double x0 = 0;

				for(size_t e = 0; e != self.envelope_count; ++e)
				{
					_read_x_funcs(x0, self.params[e]);
					p0 = _read_x0(self.enums[e], self.params[e], p0, _FIT_COUNT_FULL);
					x0 = _params_dx(self.params[e]);
				}
			}

			// TODO: SLSQP seems to be pretty slow.
			nlopt_opt opt = nlopt_create(NLOPT_LD_SLSQP, p0 - p);
			if(!opt)
				abort();

			nlopt_check(opt, nlopt_set_min_objective(opt, _lines_f, &self));

			nlopt_check(opt, nlopt_set_ftol_rel(opt, 1e-15)); // TODO: Way too tight!

			double min0[_FIT_COUNT_FULL] = {};
			double max0[_FIT_COUNT_FULL] =
			{
				y_max,
				INFINITY,
				INFINITY,
				y_max,
				INFINITY,
				INFINITY,
				y_max,
				INFINITY,

				INFINITY,
				INFINITY,
				INFINITY,
				1
			};

			min0[mandatory_enum] = DBL_EPSILON;

			{
				// Only matters for params_mod[1]. params_mod[0] doesn't use DELAY_Y or HOLD_Y.
				double sustain_y = params_mod[1][_FIT_SUSTAIN_Y];
				if(
					params_mod[1][_FIT_DELAY_Y] <= params_mod[1][_FIT_SUSTAIN_Y] &&
					params_mod[1][_FIT_HOLD_Y] >= params_mod[1][_FIT_SUSTAIN_Y])
				{
					min0[_FIT_DELAY_Y] = 0;
					max0[_FIT_DELAY_Y] = sustain_y;
					min0[_FIT_HOLD_Y] = sustain_y;
					max0[_FIT_HOLD_Y] = y_max;

					min0[_FIT_SUSTAIN_DY_DELAY] = 0;
					max0[_FIT_SUSTAIN_DY_DELAY] = y_max - params_mod[0][_FIT_DELAY_Y];
					min0[_FIT_HOLD_DY_SUSTAIN] = 0;
					max0[_FIT_HOLD_DY_SUSTAIN] = y_max;
				}
				else
				{
					min0[_FIT_DELAY_Y] = sustain_y;
					max0[_FIT_DELAY_Y] = y_max;
					min0[_FIT_HOLD_Y] = 0;
					max0[_FIT_HOLD_Y] = sustain_y;

					min0[_FIT_SUSTAIN_DY_DELAY] = -params_mod[0][_FIT_DELAY_Y];
					max0[_FIT_SUSTAIN_DY_DELAY] = 0;
					min0[_FIT_HOLD_DY_SUSTAIN] = -y_max;
					max0[_FIT_HOLD_DY_SUSTAIN] = 0;
				}
			}

			double m[14];

			{
				double *m0 = m;
				for(size_t e = 0; e != self.envelope_count; ++e)
				{
					m0 = _read_x0(self.enums[e], min0, m0, _FIT_COUNT_FULL);
					min0[mandatory_enum] = 0; // Only the first envelope needs this.
				}

				nlopt_check(opt, nlopt_set_lower_bounds(opt, m));
			}

			{
				double *m0 = m;
				for(size_t e = 0; e != self.envelope_count; ++e)
					m0 = _read_x0(self.enums[e], max0, m0, _FIT_COUNT_FULL);
				nlopt_check(opt, nlopt_set_upper_bounds(opt, m));
			}

			{
				double grad[14];
				assert(isfinite(_lines_f(p0 - p, p, grad, &self)));
			}

			nlopt_result nlopt_result = nlopt_optimize(opt, p, &mod_error0);
			if(nlopt_result != NLOPT_FAILURE && nlopt_result != NLOPT_ROUNDOFF_LIMITED) // SLSQP a buttface.
				nlopt_check(opt, nlopt_result);
			mod_error0 /= _error_scale;

			_write_x(&self, p);

			nlopt_destroy(opt);
		}

		split = _params_dx(params_mod[0]);
#if DEBUG_ENVELOPES
		*mod_split_x = box_warp_inv(split);
#endif

		_write_envelope(params_mod[0], &envelopes[0][envelope_fit_mod]);
		_write_envelope(params_mod[1], &envelopes[1][envelope_fit_mod]);

		double params_vol[envelope_fit_max_envelopes][_FIT_COUNT_BASE];
#if DEBUG_ENVELOPES
		*mod_error = mod_error0;

		*vol_error =
#endif
		_fit1(
			vol_n,
			vol_s,
			vol_lC,
			true,
			vol_no_delay,
			y_max,
			params_vol[0]);

		memcpy(&params_vol[1], &params_vol[0], sizeof(params_vol[0]));

		/*
		Attack: 0.0-1.0
		Decay: 0.2201143956168158-2.673244772839988
		*/

		{
			double max_sustain_0 = _split_map(split, 2.673244772839988);
			double max_sustain = box_warp(max_sustain_0);

			double dy = params_vol[0][_FIT_HOLD_Y] - params_vol[0][_FIT_SUSTAIN_Y];
			if(dy != 0)
			{
				params_vol[0][_FIT_DECAY_DX] =
					box_warp(box_warp_inv(params_vol[0][_FIT_DECAY_DX]) * params_vol[0][_FIT_HOLD_Y] / dy);
			}

			double sustain_x =
				params_vol[0][_FIT_DELAY_DX] +
				params_vol[0][_FIT_ATTACK_DX] +
				params_vol[0][_FIT_HOLD_DX] +
				params_vol[0][_FIT_DECAY_DX];

			if(sustain_x >= max_sustain || dy == 0)
			{
				double x0;
				enum _fit_segment seg = _split_find(params_vol[0], max_sustain, &x0);
				if(dy == 0 && seg == _FIT_SUSTAIN_DX)
				{
					x0 = sustain_x - params_vol[0][_FIT_DECAY_DX];
					seg = _FIT_DECAY_DX;
				}

				_split_adjust_hold(vol_lC, seg, max_sustain_0, params_vol[0]);

				_split_zero(seg, _FIT_SUSTAIN_DX, params_vol[0]);
				params_vol[0][seg] = max_sustain - x0;
				params_vol[0][_FIT_SUSTAIN_DX] = 0;
			}
		}

		params_vol[0][_FIT_SUSTAIN_Y] = 0;

		{
			double decay_x = params_vol[0][_FIT_DELAY_DX] + params_vol[0][_FIT_ATTACK_DX] + params_vol[0][_FIT_HOLD_DX];
			double max_decay_0 = _split_map(split, 0.2201143956168158);
			double max_decay = box_warp(max_decay_0);

			if(decay_x > max_decay)
			{
				double x0;
				enum _fit_segment seg = _split_find(params_vol[0], max_decay, &x0);
				assert(seg != _FIT_DECAY_DX);

				_split_adjust_hold(vol_lC, seg, max_decay_0, params_vol[0]);

				_split_zero(seg, _FIT_DECAY_DX, params_vol[0]);
				params_vol[0][seg] = max_decay - x0;
				params_vol[0][_FIT_DECAY_DX] += decay_x - max_decay;
			}
		}

		{
			double hold_x = params_vol[1][_FIT_DELAY_DX] + params_vol[1][_FIT_ATTACK_DX];
			double min_hold = box_warp(_split_map(split, 1));

			if(hold_x < min_hold)
			{
				double x0;
				enum _fit_segment seg = _split_find(params_vol[1], min_hold, &x0);

				if(seg == _FIT_DECAY_DX)
					params_vol[1][_FIT_HOLD_Y] = _split_decay_y(params_vol[1], x0, min_hold);
				else if(seg == _FIT_SUSTAIN_DX)
					params_vol[1][_FIT_HOLD_Y] = params_vol[1][_FIT_SUSTAIN_Y];

				params_vol[1][_FIT_ATTACK_DX] += min_hold - hold_x;
				params_vol[1][seg] -= min_hold - x0;
				assert(seg == _FIT_HOLD_DX || seg == _FIT_DECAY_DX || seg == _FIT_SUSTAIN_DX);
				_split_zero(_FIT_HOLD_DX, seg, params_vol[1]);
			}
		}

		//if(params_vol[1][_FIT_DELAY_DX]
		//params_vol[1][_FIT_DELAY_DX] += split;

		_write_envelope(params_vol[0], &envelopes[0][envelope_fit_vol]);
		_write_envelope(params_vol[1], &envelopes[1][envelope_fit_vol]);

		return 2;
	}

	_write_envelope(params_mod[0], &envelopes[0][envelope_fit_mod]);

	double params_vol[_FIT_COUNT_BASE];

#if DEBUG_ENVELOPES
	*mod_error = mod_error0;
	*mod_split_x = -INFINITY;

	*vol_error =
#endif
	_fit1(
		vol_n,
		vol_s,
		vol_lC,
		true,
		vol_no_delay,
		y_max,
		params_vol);
	_write_envelope(params_vol, &envelopes[0][envelope_fit_vol]);

	return 1;
}

#if 0

static void _get_dst(
	double lC,
	size_t envelope_count,
	const double (*params_src)[_FIT_COUNT_FULL],
	double (*params_dst)[_FIT_XY_COUNT])
{
	for(size_t e = 0; e != envelope_count; ++e)
	{
		for(size_t i = 0; i != _FIT_COUNT_BASE; ++i)
		{
			assert((i == _FIT_DELAY_Y && params_src[e][i] == -INFINITY) || isfinite(params_src[e][i]));
		}
	}

	_segments_xy(envelope_count, params_src, params_dst);
	_segments_xy_clip_attack(lC, envelope_count, params_dst);
}

#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

double frand()
{
	return (double)rand() / RAND_MAX;
}

double double_time()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + tv.tv_usec * 1e-6;
}

int main()
{
/*
	srand(time(NULL));
	double x[2] = {3 * frand(), 0.5 * frand()};
	double s[2 * (2 + 4)];
	bfgs(s, 2, x, 0.0001, 4, f, df);
*/

	double then = double_time();
	envelope_fit_fixed_hold(
	double now = double_time();
	printf("T+%g\n", now - then);

	{
		double s[5][2];
		_get_dst(x, s);
		for(size_t i = 0; i != 5; ++i)
			printf("%g\t%g\n", s[i][0], s[i][1]);
	}
}

#endif

#if 0

int main()
{
	const double lCvol = log(2) / 12;

#if 0
	struct point src[7];
	for(size_t i = 0; i != arraysize(src); ++i)
	{
		double x = i / (arraysize(src) - 1.0);
		src[i].x = x * x;
		src[i].y = 127 * x * (1 - x);
	}

	double params0[_FIT_COUNT] =
	{
		0,
		0.2,
		0.401,
		127,
		0.4011,
		0.8
	};

	const double lC = lCvol;
#endif

#if 0
	struct point src[] = {{0, 127}, {2.6103001647498481, 63}, {7.1550967223397421, 37}, {11.699893279929636, 0}};
	double params0[] = {0, 0, 0, 127, 0, 11.699893279929636};
	const double lC = 0.024819723262237539;
#endif

#if 1
	struct point src[] =
	{
		{0, -INFINITY},
		{0.068593501602322762, -INFINITY},
		{0.068593501602322762, 127},
		{7.2001437161241757, 39},
		{7.7121437161241762, 0}
	};

	static const double params0[] =
	{
		-INFINITY,
		0.068593501602322762,
		109.68765950933252,
		127,
		127.00000000000006,
		71.143550214521909
	};

	const double lC = lCvol;
#endif

#if 0
	struct point src[] = {{0, 127}, {1, 0}};
	static const double params0[] = {0, 0.00, 0.301, 127, 0.203, 0.096};
	const double lC = lCvol;
#endif

#if 0
	const double zero = 0;
	struct point src[] = {{0, zero}, {0.2, zero}, {0.2, 127}, {1.0, 127}};
	static const double params0[] = {zero, 0.2, 0.2, 120, 0.0, 0.6};
	const double lC = lCvol;
#endif

	double self_params[_FIT_COUNT];
	memcpy(self_params, params0, sizeof(self_params));

	struct _flex_fit self =
	{
		{src, arrayend(src) - 1},
		{},

		0,
		{},

		self_params,

		127,
		lC,

		true,
	};

	self.src_end = _mb(self.src, self.src_mb);

	const uint8_t *enums_src;
	if(src[0].y == -INFINITY)
	{
		static const uint8_t enums_data[] = {_FIT_ATTACK_DX, _FIT_HOLD_Y, _FIT_HOLD_DX, _FIT_DECAY_DX};
		enums_src = enums_data;
		self.enums_n = arraysize(enums_data);
	}
	else
	{
		static const uint8_t enums_data[] = {_FIT_ATTACK_Y, _FIT_DELAY_DX, _FIT_ATTACK_DX, _FIT_HOLD_Y, _FIT_HOLD_DX, _FIT_DECAY_DX};
		enums_src = enums_data;
		self.enums_n = arraysize(enums_data);
	}

	memcpy(self.enums, enums_src, self.enums_n);

	double params[_FIT_COUNT];
	_read_x(self.enums_n, self.enums, self.params, params);

	double grad[_FIT_COUNT];
	double x = _lines_f(self.enums_n, params, grad, &self);
	printf("%.17g", x);
	for(size_t i = 0; i != self.enums_n; ++i)
		printf("\t%.17f", grad[i]);
	putchar('\n');

	printf("%.17g", x);
	for(size_t i = 0; i != self.enums_n; ++i)
	{
		static const double _d = 1e-4;
		memcpy(self.params, params0, sizeof(params0));
		_read_x(self.enums_n, self.enums, self.params, params);
		params[i] += _d;
		double grad0[_FIT_COUNT];
		double x0 = _lines_f(self.enums_n, params, grad0, &self);
		printf("\t%.17f", (x0 - x) / _d);
	}

	putchar('\n');
}

#endif

#if 0

int main()
{
	const double lC = log(2) / 12;

/*
	struct point src[] =
	{
		{0, 0},
		{0.75, 0},
		{1.75, 127},
		{2.25, 127},
		{2.5, 64},
		{2.5, 64}
	};
*/

/*
	struct point src[] =
	{
		{0, 0},
		{2, 127},
		{2.5, 64},
		{2.5, 64}
	};
*/

	/*
	struct point src[] =
	{
		{0, 0},
		{2.5, 125},
		{5.5, 24},
		{7.5, 108},
		{8.5, 64},
		{8.5, 64}
	};
	*/

	struct point src[] = {{0, -INFINITY}, {0.016, -INFINITY}, {0.016, 0}, {0.075714111458355673, 127}, {0.075714111458355673, 127}};

	assert(src[arraysize(src) - 1].x == src[arraysize(src) - 2].x);

#if 0
	double params_xy[][_FIT_XY_COUNT] =
	{
		{0, 0.9, 1.7, 127, 2.9, 3.6, 32, 5},
		{16, 4.5, 5.5, 96, 6.4, 7.5, 64, 7.5}
	};

	double params[arraysize(params_xy)][_FIT_COUNT_FULL];

	for(size_t e = 0; e != arraysize(params_xy); ++e)
	{
		params[e][_FIT_DELAY_Y] = params_xy[e][_FIT_XY_ATTACK_Y];
		params[e][_FIT_DELAY_DX] = box_warp(params_xy[e][_FIT_XY_ATTACK_X]);
		params[e][_FIT_ATTACK_DX] = box_warp(params_xy[e][_FIT_XY_HOLD_X]) - box_warp(params_xy[e][_FIT_XY_ATTACK_X]);
		params[e][_FIT_HOLD_Y] = params_xy[e][_FIT_XY_HOLD_Y];
		params[e][_FIT_HOLD_DX] = box_warp(params_xy[e][_FIT_XY_DECAY_X]) - box_warp(params_xy[e][_FIT_XY_HOLD_X]);
		params[e][_FIT_DECAY_DX] = box_warp(params_xy[e][_FIT_XY_SUSTAIN_X]) - box_warp(params_xy[e][_FIT_XY_DECAY_X]);
		params[e][_FIT_SUSTAIN_Y] = params_xy[e][_FIT_XY_SUSTAIN_Y];
		params[e][_FIT_SUSTAIN_DX] = box_warp(params_xy[e][_FIT_XY_END_X]) - box_warp(params_xy[e][_FIT_XY_SUSTAIN_X]);
	}
#endif

	double params[][_FIT_COUNT_FULL] =
	{
		{-INFINITY, 0.12649110640673517, 0.14867086672610103, 127, 0, 0, 127, 0, 22.166351562207794, 22.166351562207794, 0.29372000694274958, -INFINITY}
	};

	/*
	double params[2][_FIT_COUNT_FULL] =
	{
		{0, 1, 1, 127, 1, 1, 32, 1},
		{16, 5, 0, 96, 1, 1, 64, 0}
	};
	*/

/*
	double params0[2][_FIT_COUNT_BASE];
	for(size_t i = 0; i != arraysize(params); ++i)
		memcpy(params0, params[i], sizeof(double) * _FIT_COUNT_BASE);
*/

	struct _flex_fit self =
	{
		{},
		NULL,

		arraysize(params),
		{
			/*
			(1 << _FIT_DELAY_DX) |
				(1 << _FIT_ATTACK_DX) |
				(1 << _FIT_HOLD_DX) |
				(1 << _FIT_DECAY_DX) |
				(1 << _FIT_SUSTAIN_DX) |
				(1 << _FIT_HOLD_DY_SUSTAIN) |
				(1 << _FIT_SUSTAIN_DY_DELAY),
				//(1 << _FIT_HOLD_Y) |
				//(1 << _FIT_SUSTAIN_Y),
			(1 << _FIT_DELAY_Y) |
				(1 << _FIT_DELAY_X_T) |
				(1 << _FIT_ATTACK_DX_X0) |
				//(1 << _FIT_DELAY_DX) |
				//(1 << _FIT_ATTACK_DX) |
				(1 << _FIT_HOLD_Y) |
				(1 << _FIT_HOLD_DX) |
				(1 << _FIT_DECAY_DX),
			*/

			0x3c
		},

		params,

		127,
		0.057762265046662105, // lC,

		true,
	};

	self.src_end = _mb(src, self.src_begin, arraysize(src) - 2);

	double p[14];
	double *p0 = p;

	{
		double x0 = 0;
		for(size_t i = 0; i != self.envelope_count; ++i)
		{
			double params_full[_FIT_COUNT_FULL];
			memcpy(params_full, self.params[i], sizeof(double) * _FIT_COUNT_BASE);
			_read_x_funcs(x0, params_full);
			x0 = _params_dx(params[i]);
			p0 = _read_x0(self.enums[i], params_full, p0, _FIT_COUNT_FULL);
		}
	}

	assert(p <= arrayend(p));
	size_t enums_n = p0 - p;

#if 1
	double grad_data[14];
	double *grad = grad_data;
	//double *grad = NULL;

	double y0 = _lines_f(enums_n, p, grad, &self);
	printf("%.17g\n", y0 / _error_scale);

	const char *vars[] =
	{
		"DELAY_Y",
		"DELAY_DX",
		"ATTACK_DX",
		"HOLD_Y",
		"HOLD_DX",
		"DECAY_DX",
		"SUSTAIN_Y",
		"SUSTAIN_DX",
		"SUSTAIN_DY_DELAY",
		"HOLD_DY_SUSTAIN",
		"ATTACK_DX_X0",
		"DELAY_X_T",
	};

	const double d = 1.0 / (1 << 22);
	size_t var_i = 1, var_e = 0;
	const char **var_s = vars;
	for(size_t i = 0; i != enums_n; ++i)
	{
		while(!(self.enums[var_e] & var_i))
		{
			var_i <<= 1;
			++var_s;

			if(var_i == 1 << _FIT_COUNT_FULL)
			{
				var_i = 1;
				++var_e;
				var_s = vars;
			}
		}

		double x0 = p[i];
		p[i] += d;
		double y1 = _lines_f(enums_n, p, NULL, &self);
		double dy = y1 - y0;
		printf("%-16s %.17g", *var_s, dy / (_error_scale * d));
		p[i] = x0;

		if(grad)
		{
			double d_grad_i = d * grad[i];
			printf("\t%.17g\t%.17g\t%g", grad[i] / _error_scale, log2(fabs((dy - d_grad_i) / (dy + d_grad_i))), (dy / d - grad[i]) / _error_scale);
		}

		putchar('\n');

		var_i <<= 1;
		++var_s;

		if(var_i == 1 << _FIT_COUNT_FULL)
		{
			var_i = 1;
			++var_e;
			var_s = vars;
		}
	}
#endif

#if 0
	++self.src.end;

	static const double x1[] = {2.5, 2.75, 3, 3.5, 4, 4.5};
	for(size_t j = 0; j != arraysize(x1); ++j)
	{
		arrayend(src)[-1].x = x1[j];
		_mb(self.src, self.src_mb);

		printf("%.17g", _lines_f(enums_n, p, grad, &self) / _error_scale);
		if(grad)
		{
			for(size_t i = 0; i != enums_n; ++i)
				printf("\t%.17g", grad[i] / _error_scale);
		}
		putchar('\n');
	}
#endif

#if 0
	//double params_xy[2][_FIT_XY_COUNT];
	_segments_xy(arraysize(params), params, params_xy);
	double prev_attack_x = params_xy[1][_FIT_XY_ATTACK_X], prev_attack_y = params_xy[1][_FIT_XY_ATTACK_Y];
	_segments_xy_clip_attack(lC, arraysize(params), params_xy);

	double grad[2][_FIT_XY_COUNT];
	double r = _lines_f0(lC, arraysize(params), self.src_begin, self.src_end, params_xy, grad);
	_segments_xy_clip_attack_grad(lC, arraysize(params), prev_attack_x, prev_attack_y, params_xy, grad);
	printf("%.17g\n", r);

	for(size_t e = 0; e != arraysize(params); ++e)
	{
		for(size_t i = 0; i != _FIT_XY_COUNT; ++i)
		{
			static const char *const vars[] =
			{
				"ATTACK_Y",
				"ATTACK_X",
				"HOLD_X",
				"HOLD_Y",
				"DECAY_X",
				"SUSTAIN_X",
				"SUSTAIN_Y",
				"END_X ",
			};

			_segments_xy(arraysize(params), params, params_xy);
			//_segments_xy_clip_attack(lC, arraysize(params), params_xy);

			const double d0 = -1.0 / (1 << 16);
			static const double d[_FIT_XY_COUNT] = {d0, -d0, d0, d0, d0, d0, d0, d0};
			params_xy[e][i] += d[i];

			_segments_xy_clip_attack(lC, arraysize(params), params_xy);
			double diff = (_lines_f0(lC, arraysize(params), self.src_begin, self.src_end, params_xy, NULL) - r) / d[i];

			printf(
				"%lu.%s\t%.17g\t%.17g\t%.17g\n",
				e,
				vars[i],
				grad[e][i],
				diff,
				log2(fabs((diff - grad[e][i]) / (diff + grad[e][i]))));
		}
	}
#endif

	return 0;
}

#endif
