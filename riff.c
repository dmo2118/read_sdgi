/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "riff.h"

io_result riff_begin(struct riff *riff, struct io_file *io, uint32_t tag)
{
	uint32_t chunk[2];
	chunk[0] = tag;
	chunk[1] = 0;
	riff->offset = io_file_tell(io);
	return io_file_write(io, &chunk, sizeof(chunk));
}

io_result riff_commit(struct riff *riff, struct io_file *io)
{
	io_file_size end = io_file_seek(io, riff->offset + 4);

	uint32_t size = (end - (riff->offset + 8) + 1) & ~1;
	uint32_t size0 = little_endian_32(size);

	io_result result = io_file_write(io, &size0, sizeof(size0));
	io_file_seek(io, end);
	if(result)
		return result;

	if(end & 1)
		return io_file_write(io, "", 1);
	return 0;
}

io_result riff_write(struct io_file *io, uint32_t tag, const void *data, size_t size)
{
	size_t chunk_size = (size + 1) & ~1;
	uint32_t chunk[2];
	chunk[0] = tag;
	chunk[1] = little_endian_32(chunk_size);
	io_result result = io_file_write(io, chunk, sizeof(chunk));
	if(result)
		return result;
	result = io_file_write(io, data, size);
	if(result)
		return result;
	if(size & 1)
	{
		char zero = 0;
		return io_file_write(io, &zero, sizeof(zero));
	}
	return 0;
}
