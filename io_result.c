/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#if _WIN32

#include "io_result.h"

static const _TCHAR _message_not_enough_memory[] = _TEXT("Not enough storage is available to process this command. ");

const _TCHAR *io_result_new_str(int error)
{
	TCHAR *message;

	DWORD size = FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_IGNORE_INSERTS |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL,
		error,
		0,
		(TCHAR *)(&message),
		0,
		NULL);

	if(!size)
	{
		message = LocalAlloc(LMEM_FIXED, 10 * sizeof(TCHAR));
		if(!message)
			return _message_not_enough_memory;
		else
			wsprintf(message, (SHORT)error == (INT)error ? TEXT("%d ") : TEXT("%.8X "), error);
	}

	return message;
}

void io_result_delete(const _TCHAR *str)
{
	if(str != _message_not_enough_memory)
		LocalFree((_TCHAR *)str);
}

#endif
