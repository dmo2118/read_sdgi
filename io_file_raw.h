/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_FILE_RAW_H
#define IO_FILE_RAW_H

#include "io_result.h"

typedef unsigned long long io_file_raw_size;

#if _WIN32

typedef HANDLE io_file_raw;

static inline io_result io_file_raw_new(io_file_raw *file, const char *path)
{
	HANDLE r = CreateFile(path, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
	if(r == INVALID_HANDLE_VALUE)
		return GetLastError();
	*file = r;
	return ERROR_SUCCESS;
}

static inline void io_file_raw_delete(io_file_raw file)
{
	CloseHandle(file);
}

static inline io_result io_file_raw_write(io_file_raw file, const void *data, size_t size)
{
	DWORD bytes_written;
	if(!WriteFile(file, data, size, &bytes_written, NULL))
		return GetLastError();
	if(bytes_written != size)
		return ERROR_DISK_FULL;
	return ERROR_SUCCESS;
}

static inline io_result io_file_raw_seek(io_file_raw file, io_file_raw_size size)
{
	LARGE_INTEGER size64;
	size64.QuadPart = size;
	if(SetFilePointer(file, size64.LowPart, &size64.HighPart, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
		return GetLastError();
	return ERROR_SUCCESS;
}

#else

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

typedef int io_file_raw;

static inline io_result io_file_raw_new(io_file_raw *fd, const char *path)
{
	int r = open(
		path,
		O_RDWR | O_CREAT | O_TRUNC,
#if DJGPP
		// DJGPP supports long file names for open and _open, but not _dos_open.
		S_IRUSR | S_IWUSR
#else
		S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH
#endif
		);
	if(r == -1)
		return errno;
	*fd = r;
	return 0;
}

static inline void io_file_raw_delete(io_file_raw fd)
{
	close(fd);
}

static inline io_result io_file_raw_write(io_file_raw fd, const void *data, size_t size)
{
	ssize_t result = write(fd, data, size);
	if(result < 0)
		return errno;
	if(result != size)
		return ENOSPC; // Lies!
	return 0;
}

static inline io_result io_file_raw_seek(io_file_raw fd, io_file_raw_size size)
{
	if(lseek(fd, size, SEEK_SET) < 0)
		return errno;
	return 0;
}

#endif

#endif
