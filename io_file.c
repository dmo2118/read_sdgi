/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "io_file.h"

#include <stdlib.h>

const unsigned buffer_capacity = 32768;

io_result io_file_new(struct io_file *self, const char *path)
{
	io_result result = io_file_raw_new(&self->_f, path);
	if(result)
	{
		free(self->_buffer);
		return result;
	}

	self->_pos = 0;

	self->_buffer = NULL;
	self->_size = 0;
	self->_offset = 0;

	return 0;
}

static io_result _flush(struct io_file *self, size_t new_size)
{
	if(new_size)
	{
		io_result result = io_file_raw_seek(self->_f, self->_offset);
		if(result)
			return result;

		result = io_file_raw_write(self->_f, self->_buffer, new_size);
		if(result)
			return result;
	}

	return 0;
}

io_result io_file_delete(struct io_file *self)
{
	io_result result = _flush(self, self->_size);
	io_file_raw_delete(self->_f);
	free(self->_buffer);
	return result;
}

io_result io_file_write(struct io_file *self, const void *data, size_t size)
{
	void *dest = io_file_write_begin(self, size);
	if(!dest)
		return io_result_no_memory;
	memcpy(dest, data, size);
	io_result result = io_file_write_commit(self, size);
	if(result)
		io_file_write_rollback(self);
	return result;
}

void *io_file_write_begin(struct io_file *self, size_t size)
{
	if(self->_buffer && self->_size + size <= buffer_capacity)
	{
		if(!self->_size)
		{
			self->_offset = self->_pos;
			return self->_buffer;
		}

		if(self->_pos == self->_offset + self->_size)
			return (char *)self->_buffer + self->_size;
	}

	io_result result = _flush(self, self->_size);
	if(result)
		return NULL;

	self->_size = 0;
	self->_offset = self->_pos;

	if(size > buffer_capacity)
	{
		free(self->_buffer);
		self->_buffer = malloc(size);
	}
	else
	{
		if(!self->_buffer)
			self->_buffer = malloc(buffer_capacity);
	}

	return self->_buffer;
}

io_result io_file_write_commit(struct io_file *self, size_t size)
{
	size_t new_size = self->_size + size;
	if(new_size > buffer_capacity)
	{
		io_result result = _flush(self, new_size);
		if(result)
			return result;

		free(self->_buffer);
		self->_buffer = NULL;
		self->_size = 0;
	}
	else
	{
		self->_size = new_size;
	}

	self->_pos += size;

	return 0;
}

