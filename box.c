/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "box.h"

#include "point.h"

#include <assert.h>
#include <string.h>

enum
{
	_box_v_count = 2
};

static inline void _zero_n(double *x, size_t n)
{
	for(size_t i = 0; i != n; ++i)
		x[i] = 0;
}

static void _box2_edge(double *g_u)
{
	_zero_n(g_u, 2 + _box_v_count);
}

double box_sqr(box_v box_v, double lC, const struct box_line_range *pos, double *grad)
{
	// integrate(abs(line(mb_s0,x)-f(lC,x))/x,x,pos.x0,pos.x1)
	// grad: d/d pos->seg_dst[0].x, [0].y, [1].x, [1].y

	// Protip: Don't try to integrate m*x+b if m is infinite.

	struct box_mb mb_s0 = pos->seg_src->mb;
	mb_s0.b -= mb_s0.m * box_mean;

	struct box_mb mb_px = {(pos->x1 - pos->x0) * (pos->x1 + pos->x0), pos->x0 * pos->x0};

	const struct point *d = pos->seg_dst;

	double r;

	// SLSQP really likes to try numbers near epsilon, which breaks the gradient for c0_i.
	//if(box_v != line_v)
	//	fprintf(stderr, "mb_px.m = %.17g\n", mb_px.m);
	if(fabs(mb_px.m) <= 1.0 / (1ull << 40))
	{
		// 1. t.
		assert(d[0].x != d[1].x || (pos->has_x1_0 && pos->has_x1_1));
		double ddx = box_warp_inv_sub(d[1].x, d[0].x);

		//(pos->x0 + pos->x1) * 0.5; // pos->x0, pos->x1, or (pos->x0 + pos->x1) * 0.5.
		// Matters when testing the derivative.
		double wpx = pos->x0;

		double t0 = 0, t1 = 1;
		double t = box_warp_inv_sub(wpx, d[0].x) / ddx;
		if(!pos->has_x1_0 || !pos->has_x1_1)
		{
			if(!pos->has_x1_0)
				t0 = t;
			if(!pos->has_x1_1)
				t1 = t;
		}

		// 2. v.
		struct box_mb mb_t = {t1 - t0, t0};
		double v[_box_v_count];
		double diff_v[_box_v_count][4];
		const struct box_vtbl *vtbl = box_v(lC, d[0].y, d[1].y, mb_t, v, grad ? diff_v : NULL);

		double g_box2[2 + _box_v_count];

		if(mb_px.b == -box_mean)
		{
			// Apparently src term only, dst cancels out.
			g_box2[0] = v[0] != mb_s0.b ? HUGE_VAL : fabs(mb_s0.m);
			g_box2[1] = 0;
			g_box2[2] = 0;
			g_box2[3] = 0;
			r = 0;
		}
		else
		{
			r = 0;
			mb_px.b += box_mean;
			box0(vtbl, lC, mb_s0, mb_px, v, g_box2);
			if(mb_px.m < 0)
				g_box2[0] = -g_box2[0];
		}

		if(grad)
		{
			double diff_v_xy[_box_v_count][4];
			double ddx2 = 1 / (ddx * ddx);
			for(size_t i = 0; i != _box_v_count; ++i)
			{
				// Is warp with !pos->has_x1_* fixed? Signs point to yes.
				//assert(pos->has_x1_0 || (fabs(d1x - px0) < 1e-12 && fabs(px0 - d0x) < 1e-12));
				//assert(pos->has_x1_1 || (fabs(px1 - d1x) < 1e-12 && fabs(d0x - px1) < 1e-12));

				diff_v_xy[i][1] = diff_v[i][0];
				diff_v_xy[i][3] = diff_v[i][1];

				if(!pos->has_x1_0 || !pos->has_x1_1)
				{
					double cvd;
					if(!pos->has_x1_0 && !pos->has_x1_1) // Don't subtract-then-immediately-add by diff_v[i][2].
						cvd = diff_v[i][3];
					else if(!pos->has_x1_0)
						cvd = diff_v[i][3] - diff_v[i][2];
					else // if(!pos->has_x1_1)
						cvd = diff_v[i][2];
					cvd *= ddx2;

					diff_v_xy[i][0] = box_warp_inv_diff(d[0].x) * box_warp_inv_sub(wpx, d[1].x) * cvd;
					diff_v_xy[i][2] = box_warp_inv_sub(d[0].x, wpx) * box_warp_inv_diff(d[1].x) * cvd;
				}
				else
				{
					diff_v_xy[i][0] = 0;
					diff_v_xy[i][2] = 0;
				}
			}

#if 0
			const size_t i = 0;
			memcpy(grad, diff_v_xy[i], sizeof(double) * 4);
			return v[i];
#endif

			for(size_t i = 0; i != 2 + _box_v_count; ++i)
				grad[i] = diff_v_xy[0][i] * g_box2[2] + diff_v_xy[1][i] * g_box2[3];

			grad[0] += -pos->has_x1_0 * g_box2[0] + pos->has_x1_0 * g_box2[1];
			grad[2] += pos->has_x1_1 * g_box2[0];

			grad[0] *= box_warp_inv_diff(d[0].x);
			grad[2] *= box_warp_inv_diff(d[1].x);
		}
	}
	else
	{
		double dx = box_warp_inv_sub(d[1].x, d[0].x);
		struct box_mb mb_t = {1 / dx, -(box_warp_inv(d[0].x) + box_mean) / dx};
		//mb_t.b -= mb_t.m * box_mean;

		double v[_box_v_count];
		double diff_v[_box_v_count][4];
		const struct box_vtbl *vtbl = box_v(lC, d[0].y, d[1].y, mb_t, v, grad ? diff_v : NULL);

		assert(isfinite(v[0]));
		assert(isfinite(v[1]));

		if(mb_px.m < 0)
		{
			double g_box2[2 + _box_v_count];
			r = -box2(
				vtbl,
				lC,
				mb_s0,
				box_warp_inv(pos->x1) + box_mean,
				box_warp_inv(pos->x0) + box_mean,
				v,
				grad ? g_box2 : NULL);
			// TODO: Gradient.
		}
		else
		{
			double g_box2[2 + _box_v_count];
			r = box2(
				vtbl,
				lC,
				mb_s0,
				box_warp_inv(pos->x0) + box_mean,
				box_warp_inv(pos->x1) + box_mean,
				v,
				grad ? g_box2 : NULL);

			if(grad)
			{
				double diff_v_xy[_box_v_count][4] = {};
				double f = 2 / (dx * dx);
				for(size_t i = 0; i != _box_v_count; ++i)
				{
					//diff_v[i][2] -= diff_v[i][3] * box_mean;

					diff_v_xy[i][0] = (diff_v[i][2] - (box_mean + box_warp_inv(d[1].x)) * diff_v[i][3]) * d[0].x * f;
					diff_v_xy[i][1] = diff_v[i][0];
					diff_v_xy[i][2] = ((box_warp_inv(d[0].x) + box_mean) * diff_v[i][3] - diff_v[i][2]) * d[1].x * f;
					diff_v_xy[i][3] = diff_v[i][1];
				}

				//const size_t i = 0;
				//memcpy(grad, diff_v_xy[i], sizeof(double) * 4);
				//return v[i];

				grad[0] = pos->has_x1_0 ? box_warp_inv_diff(d[0].x) * g_box2[0] : 0;
				grad[1] = 0;
				grad[2] = pos->has_x1_1 ? box_warp_inv_diff(d[1].x) * g_box2[1] : 0;
				grad[3] = 0;

				//if(d[0].x == 0)
				if(pos->x0 == -box_mean)
				{
					assert(!isfinite(g_box2[2]));
					for(size_t i = 0; i != 4; ++i)
					{
						if(diff_v_xy[0][i] != 0)
							grad[i] = HUGE_VAL;
					}
				}
				else
				{
					for(size_t i = 0; i != 4; ++i)
						grad[i] += g_box2[2] * diff_v_xy[0][i];
				}

				if(!isfinite(g_box2[3]))
				{
					for(size_t i = 0; i != 4; ++i)
					{
						if(fabs(diff_v_xy[1][i]) > 1.0 / (1ull << 50))
							grad[i] = NAN;
					}
				}
				else
				{
					for(size_t i = 0; i != 4; ++i)
						grad[i] += g_box2[3] * diff_v_xy[1][i];
				}
			}
		}
	}

	return r;
}

void box0(const struct box_vtbl *vtbl, double lC, struct box_mb mb_s0, struct box_mb mb_px, const double *v, double *grad)
{
	//printf("*\t%.17g\t%.17g\t%.17g\t%.17g\n", mb_px.m, mb_px.b, v[0], v[1]);
	_box2_edge(grad);

	// TODO: Simplify, starting here

	// 3. mb_ts.
	struct box_mb mb_ts;
	mb_ts.m = mb_s0.m * mb_px.m;
	mb_ts.b = mb_s0.m * mb_px.b + mb_s0.b - v[0];

	// 4. u.
	const size_t max_n = 2;
	double u[max_n + 2];
	u[0] = 0;

	double diff_u[1 + max_n + 1][2 + _box_v_count];
	_box2_edge(diff_u[0]);

	size_t xn0 = vtbl->u(lC, mb_ts, v, u + 1, diff_u + 1);

	size_t xn1 = 1;
	double (*g_u)[4] = diff_u + 1;

	for(size_t i = 1; i != xn0 + 1; ++i)
	{
		double ui = u[i];
		if(0 < ui && ui < 1)
		{
			u[xn1] = ui;

			if(grad)
			{
				double dv0 = diff_u[i][2] - diff_u[i][1];
				(*g_u)[0] = diff_u[i][0] * mb_s0.m;
				(*g_u)[1] = diff_u[i][1] * mb_s0.m;
				(*g_u)[2] = dv0;
				(*g_u)[3] = diff_u[i][3];

				++g_u;
			}

			++xn1;
		}
	}

	assert(xn1 <= 3); // Needs real sorting if xn1 > 3.
	if(xn1 == 3 && u[1] > u[2])
	{
		double x = u[1];
		u[1] = u[2];
		u[2] = x;

		for(size_t i = 0; i != 4; ++i)
		{
			x = diff_u[1][i];
			diff_u[1][i] = diff_u[2][i];
			diff_u[2][i] = x;
		}
	}

	u[xn1] = 1;
	_box2_edge(diff_u[xn1]);
	++xn1;

#if 0
	size_t u_i = 1;
	assert(u_i < xn1);
	memcpy(grad, diff_u[u_i], sizeof(double) * (2 + _box_v_count));
	return u[u_i];
#endif

	// (...Ending here.)

	double if0 = vtbl->i(lC, u[0], v) + v[0] * u[0];

	for(size_t i = 1; i != xn1; ++i)
	{
		assert(mb_px.b != 0.0);
		double du = u[i] - u[i - 1];

		// diff(integrate(f(u)/(u-a),u),a) == integrate(f(u)/(u-a)^2,u)
		// limit(a^2/(u-a)^2,a,inf) == 1
		//assert(mb_px.m == 0.0);
		double if1 = vtbl->i(lC, u[i], v) + v[0] * u[i];
		double g_i_0 = (if1 - if0 - du * mb_s0.b) / mb_px.b - du * mb_s0.m;
		if0 = if1;

		if(grad)
			grad[0] += fabs(g_i_0); // TODO: Is this OK?
	}
}

double box2(const struct box_vtbl *vtbl, double lC, struct box_mb mb_s0, double x0, double x1, const double *v, double *grad)
{
	// TODO: grad == NULL
	assert(x0 < x1);

	double db = v[0] - mb_s0.b;
	if(x0 == 0 && db != 0)
	{
		if(grad)
		{
			for(size_t i = 0; i != 2 + _box_v_count; ++i)
				grad[i] = NAN;
		}

		return HUGE_VAL;
	}

	// 4. u.
	const size_t max_n = 2;
	double x[max_n + 2];
	x[0] = x0;

	double diff_x[1 + max_n + 1][2 + _box_v_count];
	diff_x[0][0] = 1;
	diff_x[0][1] = 0;
	_zero_n(diff_x[0] + 2, _box_v_count);

	struct box_mb mb_s = {mb_s0.m, mb_s0.b - v[0]};
	size_t xn0 = vtbl->u(lC, mb_s, v, x + 1, grad ? diff_x + 1 : NULL);

	size_t xn1 = 1;
	double (*g_x)[4] = diff_x + 1;

	for(size_t i = 1; i != xn0 + 1; ++i)
	{
		double xi = x[i];
		if(x0 < xi && xi < x1)
		{
			x[xn1] = xi;

			if(grad)
			{
				// Order matters.
				double diff_x_1 = diff_x[i][1];
				(*g_x)[0] = 0;
				(*g_x)[1] = 0;
				memcpy(*g_x + 2, diff_x[i] + 2, sizeof(double) * _box_v_count);
				(*g_x)[2] -= diff_x_1;

				++g_x;
			}

			++xn1;
		}
	}

	assert(xn1 <= 3); // Needs real sorting if xn1 > 3.
	if(xn1 == 3 && x[1] > x[2])
	{
		double t = x[1];
		x[1] = x[2];
		x[2] = t;

		for(size_t i = 0; i != 2 + _box_v_count; ++i)
		{
			t = diff_x[1][i];
			diff_x[1][i] = diff_x[2][i];
			diff_x[2][i] = t;
		}
	}

	x[xn1] = x1;
	diff_x[xn1][0] = 0;
	diff_x[xn1][1] = 1;
	_zero_n(diff_x[xn1] + 2, _box_v_count);

	++xn1;

#if 0
	size_t x_i = 1;
	assert(x_i < xn1);
	memcpy(grad, diff_x[x_i], sizeof(double) * (2 + _box_v_count));
	return x[x_i];
#endif

	double g_i0[1 + _box_v_count];
	double r0 = vtbl->idiv(lC, x[0], v, grad ? g_i0 : NULL);

	//double f0 = vtbl->f(lC, x[0], v) / x[0];

	// 5. Tape out.
	double result = 0;
	if(grad)
		_box2_edge(grad);

	//bool has_width = x0 != x1;

	for(size_t i = 1; i != xn1; ++i)
	{
		double dx = x[i] - x[i - 1], xi0 = x[i - 1], xi1 = x[i];
		double log_t = log(xi1 / xi0);

		double term = 0;
		double g_i[2 + _box_v_count] = {};

#if 1
		if(db != 0)
		{
			term += db * log_t;
			for(size_t j = 0; j != 2 + _box_v_count; ++j)
				g_i[j] += db * (diff_x[i][j] / xi1 - diff_x[i - 1][j] / xi0);
		}
		g_i[2] += log_t;
#endif

#if 1
		term -= mb_s0.m * dx;
		for(size_t j = 0; j != 2 + _box_v_count; ++j)
			g_i[j] -= mb_s0.m * (diff_x[i][j] - diff_x[i - 1][j]);
#endif

#if 1
		double g_i1[1 + _box_v_count];
		double r1 = vtbl->idiv(lC, x[i], v, grad ? g_i1 : NULL);

		term += r1 - r0;

		if(i == 1)
		{
			// Volume delay_dx > 0 followed by attack_dx > 0 has NANs in the gradient. Don't use them if we don't have to.
			// (Doesn't work if delay is real and hold is -INFINITY.)
			for(size_t j = 0; j != 2 + _box_v_count; ++j)
				g_i[j] += diff_x[i][j] * g_i1[0];
			g_i[0] -= g_i0[0];
		}
		else
		{
			for(size_t j = 0; j != 2 + _box_v_count; ++j)
				g_i[j] += diff_x[i][j] * g_i1[0] - diff_x[i - 1][j] * g_i0[0];
		}

		for(size_t j = 0; j != _box_v_count; ++j)
			g_i[j + 2] += g_i1[j + 1] - g_i0[j + 1];

/*
		if(grad)
		{
			// g_i1[0] == (f_v(v,u) - intg.y0) / (u[i] - a)
			// diff(integrate((f(u)-y0(a))/(u-a)),u) == (f(u)-y0(a))/(u-a)
			double f1 = vtbl->f(lC, x[i], v) / x[i];

			f0 = f1;
		}
*/

		r0 = r1;
		memcpy(g_i0, g_i1, sizeof(g_i0)); // TODO: Swap pointers?
#endif

#if 0
		memcpy(grad, g_i, sizeof(*grad) * (2 + _box_v_count));
		return term;
#endif

#if 1
		if(term < 0)
		{
			term = -term;

			if(grad)
			{
				for(size_t j = 0; j != 4; ++j)
					g_i[j] = -g_i[j];
			}
		}
#endif

		result += term;

		if(grad)
		{
			for(size_t j = 0; j != 4; ++j)
				grad[j] += g_i[j];
		}
	}

	return result;
}

#if 0

#include <gsl/gsl_deriv.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>

#include <math.h>

#include "box_intg.h"

static struct box_mb _mb0(const struct point *seg)
{
	struct box_mb mb;

	if(seg[0].y == -INFINITY && seg[1].y == -INFINITY)
	{
		mb.m = 0;
		mb.b = -INFINITY;
	}
	else
	{
		double dx = seg[1].x - seg[0].x;
		mb.m = (seg[1].y - seg[0].y) / dx;
		mb.b = (seg[1].x * seg[0].y - seg[0].x * seg[1].y) / dx;
	}

	return mb;
}

static const double _d = 1e-5;
static const double _lC = log(2) / 12;
static const box_v _v = box_line;
const size_t _integration_limit = 200; /* default in Maxima */

#include <stdio.h>

const double _x0 = 0.3, _x1 = 0.7;

struct _intg_raw
{
	struct box_line_range pos;
	box_v box_v;
	const struct box_vtbl *vtbl;
	struct box_mb mb_px;

	double v[_box_v_count];

	double *x;
};

static const struct box_vtbl *_test_box_v(box_v box_v, double lC, const struct box_line_range *pos, double *v)
{
	const struct point *d = pos->seg_dst;

	/*
	// 1. t.
	assert(d[0].x != d[1].x || (pos->has_x1_0 && pos->has_x1_1));
	double ddx = box_warp_inv_sub(d[1].x, d[0].x);
	double pdx1 = 1.0 / ddx;
	double
		t0 = pos->has_x1_0 ? 0 : box_warp_inv_sub(pos->x0, d[0].x) * pdx1,
		t1 = pos->has_x1_1 ? 1 : box_warp_inv_sub(pos->x1, d[0].x) * pdx1;

	// 2. v.
	struct box_mb mb_t = {t1 - t0, t0};
	return box_v(lC, d[0].y, d[1].y, mb_t, v, NULL);
	*/

	double dx = box_warp_inv_sub(d[1].x, d[0].x);
	struct box_mb mb_t = {1 / dx, -box_warp_inv(d[0].x) / dx};
	return box_v(lC, d[0].y, d[1].y, mb_t, v, NULL);
}

static void _set_has(struct box_line_range *pos)
{
	pos->x0 = pos->has_x1_0 ? pos->seg_dst[0].x : box_warp(_x0);
	pos->x1 = pos->has_x1_1 ? pos->seg_dst[1].x : box_warp(_x1);
}

/*
static void _g_out(double r, double *x, struct box_line_range *pos)
{
	double old_x = *x;
	*x += _d;

	_set_has(pos);
	printf("\t%.17g", (box_sqr(_v, _lC, pos, NULL) - r) / _d);

	*x = old_x;
}
*/

/*
[SX0,SY0,SX1,SY1]: [0.15,110.0,0.92,34.0];
[DX0,DY0,DX1,DY1]: [0.25,12.5,0.8,97.0];
lC: log(2)/12;
line(t,y0,y1) := (y1-y0)*t+y0;
f(t) := log(line(t,exp(lC*DY0),exp(lC*DY1)))/lC;
t_from_x(x,x0,x1) := (x-x0)/(x1-x0);
[X0,X1]: [DX0,DX1];
Mean: exp(-20*lC);
plot2d(
    [
        line(t_from_x(x,SX0,SX1),SY0,SY1),
        f(t_from_x(x,DX0,DX1))
    ],[x,0,1],[y,0,127]);
quad_qag(abs(f(t_from_x(x,DX0,DX1))-line(t_from_x(x,SX0,SX1),SY0,SY1))/(x+Mean),x,X0,X1,1);
*/

/*
plot2d(
    [
        (Mean/2)*(1/x),
        1/(x/Mean+1),
        1/((x/Mean)^(4/3)+1)
    ],[x,0,8],[y,0,2]);
*/

static double _f_box(double x, void *params)
{
	struct _intg_raw *self = (struct _intg_raw *)params;
	double old_x = *self->x;
	*self->x = x;
	_set_has(&self->pos);
	//double r = box2(self->vtbl, _lC, self->pos.x0, self->pos.x1, self->v, NULL);
	double r = box_sqr(self->box_v, _lC, &self->pos, NULL);
	//double r = 0;
	//box0(self->vtbl, _lC, self->mb_px, self->v, NULL);
	*self->x = old_x;
	return r;
}

static double _line(double t, struct box_mb mb)
{
	return mb.m * t + mb.b;
}

static double _f_delta(double x, void *params)
{
	const struct _intg_raw *self = (const struct _intg_raw *)params;
	double grad[1 + _box_v_count];
	self->vtbl->idiv(_lC, x, self->v, grad);
	double fx = grad[0] * x;
	return fabs(((fx + self->v[0]) - _line(x, self->pos.seg_src->mb)) / (x + box_mean));
}

const size_t _double_max_size = 32;

static size_t _test_cmp(const char *src, double dst)
{
	char dst_str[_double_max_size];
	sprintf(dst_str, "%.17g", dst);
	fputs(dst_str, stdout);
	size_t result = 0;
	while(src[result] && src[result] == dst_str[result])
		++result;
	return result;
}

static size_t _g_out2(const char *r_str, struct _intg_raw *self, double *x)
{
	gsl_function f = {_f_box, self};
	self->x = x;
	double result, abserr;
	gsl_deriv_forward(&f, *x, 1.0 / (1 << 9), &result, &abserr);
	putchar('\t');
	return _test_cmp(r_str, result);
}

static void _gsl_handler(const char *reason, const char *file, int line, int gsl_errno)
{
	printf("[%s:%d: %s]\n", file, line, reason);
}

static void _test(
	gsl_integration_workspace *workspace,
	box_v box_v,
	const struct point *src0,
	const struct point *dst0,
	bool has_x0,
	bool has_x1)
{
	struct box_xmb src[2] = {{box_warp(src0[0].x), _mb0(src0)}, {box_warp(src0[1].x), {0, src0[1].y}}};

	struct point dst[2] = {dst0[0], dst0[1]};
	dst[0].x = box_warp(dst[0].x);
	dst[1].x = box_warp(dst[1].x);

	struct _intg_raw self = {{src, dst, has_x0, has_x1}, box_v};
	_set_has(&self.pos);

	self.mb_px.m = NAN; // _box_mb_px(&self.pos);
	self.mb_px.b = NAN;
	self.vtbl = _test_box_v(box_v, _lC, &self.pos, self.v);

	char r_strs[5][_double_max_size];

	double grad[4] = {};
	//double r = box2(self.vtbl, _lC, self.pos.x0, self.pos.x1, self.v, grad);
	double r = box_sqr(self.box_v, _lC, &self.pos, grad);
	//double r = 0;
	//box0(self.vtbl, _lC, self.mb_px, self.v, grad);
	sprintf(r_strs[0], "%.17g", r);
	fputs(r_strs[0], stdout);

	for(size_t i = 0; i != 4; ++i)
	{
		sprintf(r_strs[i + 1], "%.17g", grad[i]);
		putchar('\t');
		fputs(r_strs[i + 1], stdout);
	}

	putchar('\n');

#if 1
	gsl_function f = {_f_delta, &self};
	double r0, abserr;
	if(self.pos.x0 == self.pos.x1)
	{
		r0 = 0;
	}
	else if(self.pos.x0 == -box_mean && self.pos.seg_src->mb.b != self.pos.seg_dst[0].y)
	{
		r0 = HUGE_VAL;
	}
	else
	{
		gsl_error_handler_t *old_handler = gsl_set_error_handler(_gsl_handler);
		gsl_integration_qag(
			&f,
			box_warp_inv(self.pos.x0),
			box_warp_inv(self.pos.x1),
			0,
			1e-8,
			_integration_limit,
			1,
			workspace,
			&r0,
			&abserr);
		gsl_set_error_handler(old_handler);
	}

	size_t r_str_sizes[5];
	r_str_sizes[0] = _test_cmp(r_strs[0], r0);
#if 0
	//r_str_sizes[1] = _g_out2(r_strs[1], &self, &self.mb_px.m);
	//r_str_sizes[2] = _g_out2(r_strs[2], &self, &self.mb_px.b);
	assert(self.pos.has_x1_0);
	assert(self.pos.has_x1_1);
	r_str_sizes[1] = _g_out2(r_strs[1], &self, &self.pos.seg_dst[0].x);
	r_str_sizes[2] = _g_out2(r_strs[2], &self, &self.pos.seg_dst[1].x);
	r_str_sizes[3] = _g_out2(r_strs[3], &self, &self.v[0]);
	r_str_sizes[4] = _g_out2(r_strs[4], &self, &self.v[1]);
#else
	r_str_sizes[1] = _g_out2(r_strs[1], &self, &dst[0].x);
	r_str_sizes[2] = _g_out2(r_strs[2], &self, &dst[0].y);
	r_str_sizes[3] = _g_out2(r_strs[3], &self, &dst[1].x);
	r_str_sizes[4] = _g_out2(r_strs[4], &self, &dst[1].y);
#endif
	putchar('\n');

	for(unsigned i = 0; i != 5; ++i)
	{
		for(unsigned j = r_str_sizes[i]; j; --j)
			putchar('.');
		for(const char *s = r_strs[i] + r_str_sizes[i]; *s; ++s)
			putchar('#');
		putchar('\t');
	}
	putchar('\n');
#endif
}

int main()
{
	static const struct point src0[] = {{0.0, 67.0}, {0.92, 34.0}};
	static const struct point src1[] = {{0.15, 110.0}, {0.92, 34.0}};
	//static const struct point src[] = {{0, 0}, {1, 0}};

	static const struct point dst0[] = {{0.25, 12.5}, {0.8, 97.0}};
	static const struct point dst1[] = {{0.25, 12.5}, {0.8, 17.0}};
	static const struct point dst2[] = {{0.25, 97.0}, {0.8, 12.5}};
	static const struct point dst3[] = {{0.25, 78.0}, {0.8, 97.0}};

	static const struct point dst_tall1[] = {{0.55, 12.5}, {0.55 + 1e-6, 97.0}};
	static const struct point dst_tall0[] = {{0.55, 12.5}, {0.55, 97.0}}; // !has_width
	static const struct point dst_tall0_x0[] = {{_x0 * 0.5, 12.5}, {_x0, 97.0}};
	//static const struct point dst[] = {{0.4, -12}, {1.0, 0.0}};

	static const struct point dst_org0[] = {{0, 67.0}, {0.8, 110.0}};
	static const struct point dst_org1[] = {{1e-6, 67.0}, {0.8, 110.0}}; // TODO: 1e0 is broken for _box()! Why?

	static const struct point dst_flat[] = {{0.25, 62}, {0.8, 62}};
	static const struct point dst_zero[] = {{0.0, 0.0}, {1.0, 0.0}};

	static const struct point dst_org00[] = {{0, 67.0}, {0, 110.0}};
	static const struct point dst_org01[] = {{0, 67.0}, {1e-1, 110.0}};

	// _box, _boxc: 76.342163997332818
	// struct point dst[] = {{0.25, 12.5}, {0.8, 12.5}};

	// 0
	// struct point dst[] = {{0.7, 12.5}, {0.7, 22.0}};

	// 0
	// struct point dst[] = {{0.7, 12.5}, {0.7, 122.0}};

	// 0
	// struct point dst[] = {{0.7, 12.5}, {0.7, 12.5}};

	gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(_integration_limit);
	assert(workspace);

#if 1
	_test(workspace, box_line, src1, dst0, true, true);
	_test(workspace, box_line, src1, dst0, false, false);
	_test(workspace, box_c, src1, dst0, true, true); // _c0_i
	_test(workspace, box_c, src1, dst0, true, false);
	_test(workspace, box_c, src1, dst0, false, true);

	_test(workspace, box_line, src1, dst1, true, true);
	_test(workspace, box_c, src1, dst1, true, true); // _c_i

	_test(workspace, box_line, src1, dst2, true, true);
	_test(workspace, box_c, src1, dst2, true, true); // _c_i

	_test(workspace, box_c, src1, dst3, true, true); // _c_i

	puts("# Flat");

	_test(workspace, box_line, src1, dst_flat, true, true);
	_test(workspace, box_c, src1, dst_flat, true, true); // _c_i
	_test(workspace, box_line, src1, dst_flat, false, false);

	_test(workspace, box_line, src1, dst_zero, false, false);
	_test(workspace, box_c, src1, dst_zero, false, false);

	puts("# Tall");

	_test(workspace, box_line, src1, dst_tall1, true, true);
	_test(workspace, box_c, src1, dst_tall1, true, true);
#endif

	_test(workspace, box_line, src1, dst_tall0, true, true); // 0
	_test(workspace, box_c, src1, dst_tall0, true, true); // 0

	_test(workspace, box_line, src1, dst_tall0_x0, false, true); // (probably OK now)
	_test(workspace, box_c, src1, dst_tall0_x0, false, true); // (probably OK now)

	puts("# Near origin");

	_test(workspace, box_line, src0, dst_org1, true, true);
	_test(workspace, box_c, src0, dst_org1, true, true);

	_test(workspace, box_line, src0, dst_org0, true, true);
	_test(workspace, box_c, src0, dst_org0, true, true);

	_test(workspace, box_c, src0, dst_org0, true, false);

#if 1
	puts("# Tall near origin");

	_test(workspace, box_line, src0, dst_org01, true, true);
	_test(workspace, box_c, src0, dst_org01, true, true);

	_test(workspace, box_line, src0, dst_org00, true, true);
	_test(workspace, box_c, src0, dst_org00, true, true);

	// db != 0
	puts("# Mismatch near origin");

	_test(workspace, box_line, src1, dst_org1, true, true);
	_test(workspace, box_c, src1, dst_org1, true, true);

	_test(workspace, box_line, src1, dst_org0, true, true);
	_test(workspace, box_c, src1, dst_org0, true, true);

	_test(workspace, box_line, src1, dst_org00, true, true);
	_test(workspace, box_c, src1, dst_org00, true, true);
#endif

	gsl_integration_workspace_free(workspace);

#if 0
	const double center = box_warp(0.55);
	struct box_xmb src[] = {{box_warp(src1[0].x), _mb0(src1)}, {box_warp(src1[1].x), {0, src1[1].y}}};
	struct point dst[] = {{center, 12.5}, {center, 97.0}}; // !has_width

	struct box_line_range pos = {src, dst, true, true};

	for(int i = -15; i != 15; ++i)
	{
		dst[1].x = center + i * 1e-4;
		_set_has(&pos);

		/*
		double v[2];
		const struct box_vtbl *vtbl = _test_box_v(box_c, _lC, &pos, v);

		double grad[4];
		printf("%.15g\t%.17g\n", dst[1].x, box2(vtbl, _lC, _box_mb_px(&pos), v));
		*/

		double grad[4];
		printf("%.7g\t%.17g\n", dst[1].x, box_sqr(box_c, _lC, &pos, grad));
	}
#endif

#if 0
	double grad[4];
	double r = box_sqr(_v, _lC, &pos, grad);
	printf("%.17g", r);

	for(size_t i = 0; i != 4; ++i)
		printf("\t%.17g", grad[i]);
	putchar('\n');

	printf("%.17g", r);
	_g_out(r, &dst[0].x, &pos);
	_g_out(r, &dst[0].y, &pos);
	_g_out(r, &dst[1].x, &pos);
	_g_out(r, &dst[1].y, &pos);
	putchar('\n');
#endif
}

#endif
