/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "plot_bezier.h"

#include "point.h"

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>

#include <gsl/gsl_poly.h>

static double _bezier_t(double x, double a, double b, double c, double d)
{
	double a1 = 1 / (3 * (b - c) + d - a);
	double xn[3];

	int roots = gsl_poly_solve_cubic(a1 * 3 * (c - 2 * b + a), a1 * 3 * (b - a), a1 * (a - x), &xn[0], &xn[1], &xn[2]);
	double tmin = INFINITY;

	for(size_t i = 0; i != roots; ++i)
	{
		if(fabs(xn[i] - 0.5) < fabs(tmin - 0.5))
			tmin = xn[i];
	}

	return tmin;
}

static void _trunc(double z, struct point *pts)
{
	double z2 = z * z;
	double z1 = z - 1;
	double z12 = z1 * z1;

	pts[3].x = z2 * z * pts[3].x - 3 * z2 * z1 * pts[2].x + 3 * z * z12 * pts[1].x - z12 * z1 * pts[0].x;
	pts[3].y = z2 * z * pts[3].y - 3 * z2 * z1 * pts[2].y + 3 * z * z12 * pts[1].y - z12 * z1 * pts[0].y;
	pts[2].x = z2 * pts[2].x - 2 * z * z1 * pts[1].x + z12 * pts[0].x;
	pts[2].y = z2 * pts[2].y - 2 * z * z1 * pts[1].y + z12 * pts[0].y;
	pts[1].x = z * pts[1].x - z1 * pts[0].x;
	pts[1].y = z * pts[1].y - z1 * pts[0].y;
	pts[0].x = pts[0].x;
	pts[0].y = pts[0].y;
}

static void _point_rev(struct point *pts)
{
	struct point temp = pts[0];
	pts[0] = pts[3];
	pts[3] = temp;

	temp = pts[1];
	pts[1] = pts[2];
	pts[2] = temp;
}

static struct point _point_center(struct point p0, double dy0, struct point p1, double dy1)
{
	double d = 1/(dy1-dy0);
	struct point c =
	{
		(p0.y-p1.y+dy1*p1.x-dy0*p0.x)*d,
		(dy1*p0.y-dy0*p1.y+dy0*dy1*(p1.x-p0.x))*d
	};
	return c;
}

static double _point_dy(const struct point *p0, const struct point *p1)
{
	return (p1->y - p0->y) / (p1->x - p0->x);
}

static double _scale(double p0, double p1, double p2_0, double p2_1)
{
	return p0 + (p1 - p0) * (p2_1 - p0) / (p2_0 - p0);
}

static struct point _point_scale(
	const struct point *p0,
	const struct point *p1,
	const struct point *p2_0,
	const struct point *p2_1)
{
	const struct point r = {_scale(p0->x, p1->x, p2_0->x, p2_1->x), _scale(p0->y, p1->y, p2_0->y, p2_1->y)};
	return r;
}

static void _trunc_at(struct point *pts, struct point p1, double dy1)
{
	_trunc(
		(_bezier_t(p1.x, pts[0].x, pts[1].x, pts[2].x, pts[3].x) + _bezier_t(p1.y, pts[0].y, pts[1].y, pts[2].y, pts[3].y))
			* 0.5,
		pts);

	double dy0 = _point_dy(&pts[0], &pts[1]);

	struct point
		c_true = _point_center(pts[0], dy0, p1, dy1),
		c_fake = _point_center(pts[0], dy0, pts[3], _point_dy(&pts[2], &pts[3]));

	pts[1] = _point_scale(&pts[0], &pts[1], &c_fake, &c_true);
	pts[2] = _point_scale(&pts[3], &pts[2], &c_fake, &c_true);
	pts[3] = p1;
}

static void _trunc_at_rev(struct point *pts, struct point p1, double dy1)
{
	_point_rev(pts);
	_trunc_at(pts, p1, dy1);
	_point_rev(pts);
}

static inline double _log_d_dx(double a, double m, double b, double x)
{
	return (a * m) / (m * x + b);
}

static double _log_y(double a, double m, double b, double x)
{
	return a * log(m * x + b);
}

void plot_bezier_log(double a, double m, double b, double x0, double x1)
{
	/*
	// Cvol: 2^(1/12);
	// plot2d(a*log(m*(x-dx)),[x,0,1],[y,-127*log(Cvol),0]);
	static const struct point pts0[] =
	{
		{0.025529928956892114, -3.6679038304630467},
		{0.15109412646997228, 1.2504097976358901},
		{-1.5405948647652465, 2.6285725530142381},
		{39.169713385749077, 3.6679038304630467}
	};
	*/

	// plot2d(a*log(m*(x-dx)),[x,exp(0),exp(1)],[y,0,1])
	static const struct point pts0[] =
	{
		{0.1353352832366127, -2},
		{0.4165309810930653, 0.077770786239281353},
		{1.3609228714725345, 1.1841808822739197},
		{7.3890560989306504, 2}
	};

/*
	static const struct point pts0[] =
	{
		{0.22313016014842982, -1.5},
		{0.53808278371266349, -0.088480269297875003},
		{1.3899171022499264, 0.81013242561806764},
		{4.4816890703380645, 1.5},
	};
*/

/*
	static const struct point pts0[] =
	{
		{0.36787944117144233, -1},
		{0.67857017572690503, -0.15545502198729311},
		{1.3044246568636579, 0.47987101381725283},
		{2.7182818284590451, 1}
	};
*/

/*
	static const struct point pts0[] =
	{
		{0.60653065971263342, -0.5},
		{0.83468653034002549, -0.12383456306151214},
		{1.1631369559001743, 0.20547822519827696},
		{1.6487212707001282, 0.5}
	};
*/

	if(a == 0 || m == 0)
	{
		struct point pt;
		pt.y = a * log(b);
		pt.x = (x0 * 2 + x1) * (1.0 / 3.0);
		point_graph(pt);
		pt.x = (x0 + x1 * 2) * (1.0 / 3.0);
		point_graph(pt);
		pt.x = x1;
		point_graph(pt);
		return;
	}

	assert(-b < x0 * m);

	double dy_part = pts0[3].y - pts0[0].y;
	if(m < 0)
		dy_part = -dy_part;

	double y0 = log(m * x0 + b), y1 = log(m * x1 + b);

	double dy_full = y1 - y0;
	size_t parts = ceil(dy_full / dy_part);

	double cy = 0.5 * (y0 + y1 - dy_part * (parts - 1));

	for(size_t y = 0; y != parts; ++y)
	{
		struct point pts[4];
		double dy = dy_part * y + cy;
		for(size_t i = 0; i != 4; ++i)
		{
			pts[i].x = (exp(dy) * pts0[i].x - b) / m;
			pts[i].y = a * (pts0[i].y + dy);
		}

		if(m < 0)
			_point_rev(pts);

		if(pts[0].x < x0)
		{
			struct point p = {x0, _log_y(a, m, b, x0)};
			_trunc_at_rev(pts, p, _log_d_dx(a, m, b, x0));
		}

		if(pts[3].x > x1)
		{
			struct point p = {x1, _log_y(a, m, b, x1)};
			_trunc_at(pts, p, _log_d_dx(a, m, b, x1));
		}

		for(size_t i = 1; i != 4; ++i)
			point_graph(pts[i]);
	}
}

#if 0

#include "plot2d.h"

#include <stdio.h>

struct _log_f
{
	double a;
	double m;
	double b;
};

double _log_f_plot(void *self_raw, double x)
{
	const struct _log_f *self = (const struct _log_f *)self_raw;
	return _log_y(self->a, self->m, self->b, x);
}

void _test(double a, double m, double b, double x0, double x1)
{
	{
		fputs("\t<path class=\"src\" d=\"M", stdout);
		struct _log_f self = {a, m, b};
		plot2d(&self, _log_f_plot, x0, x1, 160);
		fputs("\t\" />\n", stdout);
	}

	{
		fputs("\t<path class=\"params\" d=\"M", stdout);
		struct point p = {x0, _log_y(a, m, b, x0)};
		point_graph(p);
		fputs(" C", stdout);
		plot_bezier_log(a, m, b, x0, x1);
		fputs("\" />\n", stdout);
	}
}

int main()
{
	const double w = 640, h = 480;

	const double xmin = -2, xmax = 2, ymin = -2, ymax = 2;

	printf(
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
		"\n"
		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"%g\" height=\"%g\">\n"
		"<defs>\n"
		"	<style type=\"text/css\">\n"
		"		path {\n"
		"			fill: none;\n"
		"			vector-effect: non-scaling-stroke;\n"
		"		}\n"
		"\n"
		"		.src {\n"
		"			stroke: #004586;\n"
		"			stroke-width: .03in;\n"
		"		}\n"
		"\n"
		"		.params {\n"
		"			stroke: #ff420e;\n"
		"			stroke-width: 1pt;\n"
		"		}\n"
		"	</style>\n"
		"</defs>\n"
		"<g transform=\"translate(0,%g) scale(%g,%g) translate(%g,%g)\">\n",
		w, h,
		h, w / (xmax - xmin), -h / (ymax - ymin), -xmin, -ymin);

		_test(1.0 / 5.0, 5.0, 1, -(1.0 - (1.0 / 512.0)) / 5.0, 2);
		_test(0.9, -2.1, 2.73, -2, 1.24);
		_test(-1.5, 1, 1, -0.625, 2);
		_test(-1, -1, 0, -2, -0.25);
		_test(0, 1, 3, -2, 2);
		_test(1, 0, 3, -2, 2);

		fputs(
			"</g>\n"
			"</svg>\n",
			stdout);

}

#endif
