workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
- build
- upload
- release

variables:
  PROJECT_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
  MACOS_BINARY: sdgi2sf2-${CI_COMMIT_SHORT_SHA}-macOS-amd64
  MACOS_URL: ${PROJECT_URL}/packages/generic/sdgi2sf2/${CI_COMMIT_SHORT_SHA}/${MACOS_BINARY}.zip
  WIN64_BINARY: sdgi2sf2-${CI_COMMIT_SHORT_SHA}-Win64
  WIN64_URL: ${PROJECT_URL}/packages/generic/sdgi2sf2/${CI_COMMIT_SHORT_SHA}/${WIN64_BINARY}.zip
  DOS_BINARY: sdgi2sf2-${CI_COMMIT_SHORT_SHA}-DOS-i386
  DOS_URL: ${PROJECT_URL}/packages/generic/sdgi2sf2/${CI_COMMIT_SHORT_SHA}/${DOS_BINARY}.zip

build-macos:
  stage: build
  tags:
  - macos
  artifacts:
    name: ${MACOS_BINARY}
    paths:
    - sdgi2sf2
    - libnlopt.*.dylib
    - libgsl.*.dylib
    - libgslcblas.*.dylib
    - README.html
    - COPYING.html
    - COPYING/lgpl-2.1.html
    - COPYING/nlopt.txt
  script: |
    fetch ()
    {
        oci_platform="$1"
        homebrew_platform="$2"
        formula="$3"

        json="$(curl -fsS "https://formulae.brew.sh/api/formula/$formula.json" | jq -c)"
        version="$(echo "$json" | jq -r .versions.stable)"

        # Also possible: oras blob fetch -o - "ghcr.io/homebrew/core/$formula:$version@sha256:$sha256"
        oras pull --platform "$oci_platform" "ghcr.io/homebrew/core/$formula:$version"

        tar="$formula--$version.$homebrew_platform.bottle.tar.gz"

        echo "$(echo "$json" | jq -r ".bottle.stable.files.$homebrew_platform.sha256")" "$tar" | gsha256sum -c

        tar xzf "$tar"
        cp -Rl "$formula/$version/include" "$formula/$version/lib" .
    }

    build ()
    {
        oci_platform="$1"
        homebrew_platform="$2"

        mkdir -p homebrew

        (
            cd homebrew

            fetch "$oci_platform" "$homebrew_platform" nlopt
            fetch "$oci_platform" "$homebrew_platform" gsl

            sed -I '' 's/@@HOMEBREW_CELLAR@@/homebrew/g' lib/pkgconfig/*.pc
        )

        make clean
        make -j `sysctl -n hw.ncpu` CPATH='homebrew/include' PKG_CONFIG_PATH='homebrew/lib/pkgconfig' LDFLAGS='-rpath @executable_path'

        libnlopt="$(readlink homebrew/lib/libnlopt.dylib)"
        cp "homebrew/lib/$libnlopt" .

        libgsl="$(readlink homebrew/lib/libgsl.dylib)"
        install_name_tool sdgi2sf2 -change "@@HOMEBREW_PREFIX@@/opt/gsl/lib/$libgsl" "@executable_path/$libgsl"
        mv "homebrew/lib/$libgsl" .

        libgslcblas="$(readlink homebrew/lib/libgslcblas.dylib)"
        install_name_tool sdgi2sf2 -change "@@HOMEBREW_PREFIX@@/opt/gsl/lib/$libgslcblas" "@executable_path/$libgslcblas"
        mv "homebrew/lib/$libgslcblas" .
    }

    build 'darwin/amd64:macOS 10.15.7' catalina

    chmod u+w *.dylib

    for md in README COPYING
    do
        cmark-gfm -e table < $md.md > $md.html
    done

    mkdir COPYING
    cp homebrew/nlopt/*/COPYING COPYING/nlopt.txt
    curl -fsS https://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html > COPYING/lgpl-2.1.html

build-win64:
  stage: build
  artifacts:
    name: ${WIN64_BINARY}
    paths:
    - sdgi2sf2.exe
    - libgcc_s_seh-1.dll
    - libgsl-*.dll
    - libgslcblas-0.dll
    - libnlopt.dll
    - libstdc++-6.dll
    - libwinpthread-1.dll
    - README.html
    - COPYING.html
    - COPYING/nlopt.html
    - COPYING/lgpl-2.1.html
    - COPYING/gcc-exception-3.1.txt
    - COPYING/libwinpthread.txt
  image: debian:stable-slim # Alpine doesn't have cmark-gfm.
  # Debian MinGW-w64 doesn't seem to do UCRT for the time being.
  script: |
    apt-get update
    apt-get install -y cmark-gfm curl dos2unix gcc-mingw-w64-x86-64-win32 git gpg jq make pkg-config unzip zstd
    gpg --keyserver hkps://keyserver.ubuntu.com --recv-keys 5F944B027F7FE2091985AA2EFA11531AA0AA7F57

    fetch ()
    {
        base="$1"
        name="${2:-$1}"

        version="$(curl -fsS "https://packages.msys2.org/api/search?query=mingw-w64-$base" | jq -r '.results.exact.version')"
        pkg="https://mirror.msys2.org/mingw/mingw64/mingw-w64-x86_64-$name-$version-any.pkg.tar.zst"
        curl -LfsSO "$pkg"
        curl -LfsSO "$pkg.sig"
        gpg --verify mingw-w64-x86_64-$name-$version-any.pkg.tar.zst.sig
        tar --zstd -xf mingw-w64-x86_64-$name-$version-any.pkg.tar.zst
    }

    fetch gcc gcc-libs
    fetch gsl
    fetch nlopt
    fetch winpthreads-git libwinpthread-git

    make \
        CC=x86_64-w64-mingw32-gcc \
        CFLAGS='-O3 -Wno-unused-function -Wall -Imingw64/include' \
        LDFLAGS=-Lmingw64/lib \
        PKG_CONFIG_PATH=mingw64/lib/pkgconfig \
        PKG_CONFIG_SYSROOT_DIR=.

    mv mingw64/bin/*.dll .

    for md in README COPYING
    do
        cmark-gfm -e table < $md.md > $md.html
    done

    mkdir COPYING
    curl -fsS https://raw.githubusercontent.com/stevengj/nlopt/master/doc/docs/NLopt_License_and_Copyright.md |
        cmark-gfm > COPYING/nlopt.html
    curl -fsS https://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html > COPYING/lgpl-2.1.html
    # unix2dos < mingw64/share/licenses/gsl/COPYING > COPYING/gsl.txt
    # unix2dos < mingw64/share/licenses/gcc-libs/COPYING3 > COPYING/gcc-libs.txt
    unix2dos < mingw64/share/licenses/gcc-libs/COPYING.RUNTIME > COPYING/gcc-exception-3.1.txt
    unix2dos < mingw64/share/licenses/libwinpthread/COPYING > COPYING/libwinpthread.txt

build-dos:
  stage: build
  artifacts:
    name: ${DOS_BINARY}
    paths:
    - sdgi2sf2.exe
    - README.md
    - COPYING.md
    - COPYING/nlopt.txt
    - COPYING/lgpl-2.1.txt
    - COPYING/cwsdpmi.doc
    - COPYING/gpl-2.0.txt
  image: debian:stable-slim
  script: |
    apt-get update
    # libfl2 is for i586-pc-msdosdjgpp-ar.
    apt-get install -y bzip2 dos2unix dosbox cmark-gfm curl git jq libfl2 make unzip
    apt-get install -y --no-install-recommends cmake # Don't install GCC.

    (
        cd /usr/local
        curl -fsSL "$(curl -fsS https://api.github.com/repos/andrewwutw/build-djgpp/releases | \
            jq -r '.[0].assets[] | select(.name | test("djgpp-linux64-.*\\.tar\\.bz2")).browser_download_url')" | tar xj
    )

    git clone --depth 1 -b "$(git ls-remote --tags https://github.com/stevengj/nlopt/ | \
        cut -b52- | \
        egrep 'v[0-9]+(\.[0-9]+){2}$' | \
        tail -n1)" https://github.com/stevengj/nlopt/
    curl -fsSL 'https://ftpmirror.gnu.org/gsl/gsl-latest.tar.gz' | tar xz
    curl -fsSO 'https://sandmann.dotster.com/cwsdpmi/csdpmi7b.zip'

    (
        mkdir nlopt/build
        cd nlopt/build

        cmake \
            --toolchain=../../toolchain-djgpp.cmake \
            -DHAS_FPIC=FALSE \
            -DBUILD_SHARED_LIBS=OFF \
            -DCMAKE_INSTALL_PREFIX=../../i586-pc-msdosdjgpp \
            -DNLOPT_CXX=OFF \
            ..

        make -j `nproc`
        make install
    )

    (
        prefix="$(pwd)/i586-pc-msdosdjgpp"
        ln -s gsl-* gsl
        cd gsl
        PATH="$PATH:/usr/local/djgpp/bin"
        ./configure --host=i586-pc-msdosdjgpp --prefix="$prefix"
        make -j `nproc`
        make install
    )

    (
        mkdir cwsdpmi
        cd cwsdpmi
        unzip ../csdpmi7b.zip
        SDL_VIDEODRIVER=dummy dosbox -c 'mount a bin' -c 'a:' -c 'cwsparam CWSDSTUB.EXE swapfile=""' -c 'exit'
    )

    make \
        -j `nproc` \
        CC=/usr/local/djgpp/bin/i586-pc-msdosdjgpp-gcc \
        CFLAGS='-O3 -Wall -march=i386 -I i586-pc-msdosdjgpp/include' \
        LDFLAGS='-s -L i586-pc-msdosdjgpp/lib' \
        LDLIBS='-lnlopt -lgsl -lgslcblas -lemu' # No -lm, that's the "alternate" (i.e. a bit slower) math library.

    /usr/local/djgpp/i586-pc-msdosdjgpp/bin/exe2coff sdgi2sf2.exe
    cat cwsdpmi/bin/CWSDSTUB.EXE sdgi2sf2 > sdgi2sf2.exe

    for md in README COPYING
    do
        unix2dos "$md.md"
    done

    mkdir COPYING
    unix2dos < nlopt/COPYING > COPYING/nlopt.txt
    curl -fsS https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt | unix2dos > COPYING/lgpl-2.1.txt
    mv cwsdpmi/bin/cwsdpmi.doc COPYING
    curl -fsS https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt | unix2dos > COPYING/gpl-2.0.txt

upload:
  stage: upload
  image: alpine
  # The files for an artifact are added to the image, but the .zip archive file is not.
  script: |
    apk add curl jq

    jobs="$(curl -fsS "$PROJECT_URL/pipelines/$CI_PIPELINE_ID/jobs" | jq -c)"

    put_artifact ()
    {
        job_name="$1"
        url="$2"

        curl -fsSL "$PROJECT_URL/jobs/$(
               echo "$jobs" | jq -r ".[] | select(.name == \"$job_name\").id"
            )/artifacts" |
            curl -fsS -X PUT --data-binary @- -H "JOB-TOKEN: $CI_JOB_TOKEN" "$url"
    }

    put_artifact build-macos "$MACOS_URL"
    put_artifact build-win64 "$WIN64_URL"
    put_artifact build-dos "$DOS_URL"

# release-cli allows for a null description. The release: YAML does not.
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest # Does not contain curl.
  script: |
    release-cli create \
      --name "sdgi2sf2/$CI_COMMIT_SHORT_SHA" \
      --tag-name "release_$CI_COMMIT_SHORT_SHA" \
      --assets-link '{"name":"'$MACOS_BINARY'.zip","url":"'$MACOS_URL'"}' \
      --assets-link '{"name":"'$WIN64_BINARY'.zip","url":"'$WIN64_URL'"}' \
      --assets-link '{"name":"'$DOS_BINARY'.zip","url":"'$DOS_URL'"}'
