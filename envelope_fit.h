/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ENVELOPE_FIT_H
#define ENVELOPE_FIT_H

#include "point.h"

#include <stdbool.h>
#include <stddef.h>

//#define DEBUG_ENVELOPES 1

enum
{
	envelope_fit_max_envelopes = 2,
	envelope_fit_max_src_n = 7
};

struct envelope_fit
{
	double delay_y;
	double hold_y;
	double sustain_y;

	double delay_dx;
	double attack_dx;
	double hold_dx;
	double decay_dx;
};

enum envelope_fit_type
{
	envelope_fit_mod,
	envelope_fit_vol
};

void envelope_fit_segments(const struct envelope_fit *envelope, struct point *dst);

size_t envelope_fit(
	size_t mod_n,
	const struct point *mod_s,
	double mod_lC,
	size_t vol_n,
	const struct point *vol_s,
	double vol_lC,
	bool vol_no_delay,
	double y_max,
#if DEBUG_ENVELOPES
	double *mod_split_x,
	double *mod_error,
	double *vol_error,
#endif
	struct envelope_fit (*envelopes_out)[2]);

#endif
