/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SF_H
#define SF_H

#include <inttypes.h>

#ifdef _WIN32
#	include <windows.h>
#else
typedef uint8_t BYTE;
typedef int8_t CHAR;
typedef uint16_t WORD;
typedef int16_t SHORT;
typedef uint32_t DWORD;
#endif

struct __attribute__((packed)) sfVersionTag
{
	WORD wMajor;
	WORD wMinor;
};

struct __attribute__((packed)) sfPresetHeader
{
	CHAR achPresetName[20];
	WORD wPreset;
	WORD wBank;
	WORD wPresetBagNdx;
	DWORD dwLibrary;
	DWORD dwGenre;
	DWORD dwMorphology;
};

struct __attribute__((packed)) sfPresetBag
{
	WORD wGenNdx;
	WORD wModNdx;
};

typedef uint16_t SFModulator;
typedef uint16_t SFGenerator;
typedef uint16_t SFTransform;

struct __attribute__((packed)) sfModList
{
	SFModulator sfModSrcOper;
	SFGenerator sfModDestOper;
	SHORT modAmount;
	SFModulator sfModAmtSrcOper;
	SFTransform sfModTransOper;
};

typedef uint16_t SFGenerator;

enum
{
	startAddrsOffset = 0,
	endAddrsOffset = 1,
	startloopAddrsOffset = 2,
	endloopAddrsOffset = 3,
	startAddrsCoarseOffset = 4,
	modLfoToPitch = 5,
	vibLfoToPitch = 6,
	modEnvToPitch = 7,
	initialFilterFc = 8,
	initialFilterQ = 9,
	modLfoToFilterFc = 10,
	modEnvToFilterFc = 11,
	endAddrsCoarseOffset = 12,
	modLfoToVolume = 13,
	unused1 = 14,
	chorusEffectsSend = 15,
	reverbEffectsSend = 16,
	pan = 17,
	unused2 = 18,
	unused3 = 19,
	unused4 = 20,
	delayModLFO = 21,
	freqModLFO = 22,
	delayVibLFO = 23,
	freqVibLFO = 24,
	delayModEnv = 25,
	attackModEnv = 26,
	holdModEnv = 27,
	decayModEnv = 28,
	sustainModEnv = 29,
	releaseModEnv = 30,
	keynumToModEnvHold = 31,
	keynumToModEnvDecay = 32,
	delayVolEnv = 33,
	attackVolEnv = 34,
	holdVolEnv = 35,
	decayVolEnv = 36,
	sustainVolEnv = 37,
	releaseVolEnv = 38,
	keynumToVolEnvHold = 39,
	keynumToVolEnvDecay = 40,
	instrument = 41,
	reserved1 = 42,
	keyRange = 43,
	velRange = 44,
	startloopAddrsCoarseOffset = 45,
	keynum = 46,
	velocity = 47,
	initialAttenuation = 48,
	reserved2 = 49,
	endloopAddrsCoarseOffset = 50,
	coarseTune = 51,
	fineTune = 52,
	sampleID = 53,
	sampleModes = 54,
	reserved3 = 55,
	scaleTuning = 56,
	exclusiveClass = 57,
	overridingRootKey = 58,
	unused5 = 59,
	endOper = 60,
};

typedef struct
{
	BYTE byLo;
	BYTE byHi;
} rangesType;

typedef union
{
	rangesType ranges;
	SHORT shAmount;
	WORD wAmount;
} genAmountType;

struct __attribute__((packed)) sfGenList
{
	SFGenerator sfGenOper;
	genAmountType genAmount;
};

struct __attribute__((packed)) sfInst
{
	CHAR achInstName[20];
	WORD wInstBagNdx;
};

struct sfInstBag
{
	WORD wInstGenNdx;
	WORD wInstModNdx;
};

enum
{
	monoSample = 1,
	rightSample = 2,
	leftSample = 4,
	linkedSample = 8,
	RomMonoSample = 32769,
	RomRightSample = 32770,
	RomLeftSample = 32772,
	RomLinkedSample = 32776
};

typedef uint16_t SFSampleLink;

struct __attribute__((packed)) sfSample
{
	CHAR achSampleName[20];
	DWORD dwStart;
	DWORD dwEnd;
	DWORD dwStartloop;
	DWORD dwEndloop;
	DWORD dwSampleRate;
	BYTE byOriginalPitch;
	CHAR chPitchCorrection;
	WORD wSampleLink;
	SFSampleLink sfSampleType;
};

#endif
